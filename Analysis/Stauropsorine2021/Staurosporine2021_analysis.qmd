---
title: "Results analysis for the Stauropsorine 2021 dataset"
date: "2024-07-02"
author: "Cecile Le Sueur"
format:
    html:   
        code-fold: false
        toc: true
        number-sections: true 
execute:
  freeze: auto  # re-render only when source changes
bibliography: ../../Analysis/references.bib

---

```{r}
knitr::opts_chunk$set(echo = TRUE)
knitr::opts_chunk$set(cache = TRUE)

library(miceadds)
library(dplyr)
library(tidyr)
# plotting
library(ggplot2)
library(colorBlindness)
library(latex2exp) # for latex expression
#read excel files
library("readxl")
# str_remove
library("stringr")
#rocit
library(ROCit)
source("../FunctionsR/CommonFunctions.R")
```

# Introduction

In this document, we analyse the results of running GPMelt on the Staurosporine 2021 dataset [@zinn2021improved].

* Pre-processing: we only kept proteins with 2 replicates per conditions, and  with replicates having at least a correlation of 0.4.

We first gather the results from GPMelt, and compute the p-values. We then compare the results with the ones obtained with NPARC[@childs2019nonparametric].

# Data loading

## GPMelt results

```{r}

Ratios_RealDataset <- read.csv(file = "GPMelt_results/Ratios_RealDataset.csv") %>% dplyr::select(-X)

Ratios_NullDataset <- read.csv(file = "GPMelt_results/Ratios_NullDataset.csv") %>% dplyr::select(-X)

RealDataset <- read.csv(file = "GPMelt_results/GPMelt_inputDataset.csv") %>% dplyr::select(-X)

Fits_RealDataset <- read.csv(file = "GPMelt_results/Predictions_Levels1to2_RealDataset.csv") %>% dplyr::select(-X)

FitsParameters <- read.csv(file = "GPMelt_results/Parameters_from_GPMelt_fits.csv") %>% dplyr::select(-X)

```


# Data cleaning 

## clean the dataset containing the ratios obtained for the null distribution approximation

```{r}
Ratios_NullDataset <- Ratios_NullDataset %>% 
  mutate(Level_1_bis = gsub("Task_[0-9]+_", "", Folder)) %>%
  tidyr::separate(col = Level_1, into=c("Rm", "sample_id"), sep="Sample", remove=FALSE)%>%
  mutate(sample_id = as.numeric(sample_id)) %>%
  filter(Version == "AllPoints") %>%
  dplyr::select(-Version, -Folder, -Rm) %>%
  dplyr::rename("Level_1_sample"= "Level_1","Level_1" ="Level_1_bis") %>%
  dplyr::relocate("Level_1", .before= "Level_1_sample")

```

## clean the dataset containing the ratios obtained for the real dataset

```{r}
Ratios_RealDataset <- Ratios_RealDataset %>%
  filter(Version == "AllPoints") %>%
  dplyr::select(-Version,-Folder)

```

# Results analysis

## Check the number of IDs 

### dataset containing the ratios obtained for the real dataset

```{r}
Ratios_RealDataset %>% summarise(n_IDs = n_distinct(Level_1))
```

## Check the number of samples per ID used to build the null distribution approximation.

```{r}
Ratios_NullDataset %>% group_by(Level_1) %>% summarise(n_samples = n_distinct(Level_1_sample))%>% filter(n_samples !=10)
```
 10 samples have been obtained for each protein.
 

## p-values computation

```{r}
if(! file.exists("prerun/GPMelt_p_values.csv")){
  
  null_distribution_approximation_of_the_statistic =  Ratios_NullDataset %>% 
    dplyr::select(LikelihoodRatio) %>%
    pull()
  
  p_values_df <- Ratios_RealDataset %>% 
    mutate(size_null_distribution_approximation = length(null_distribution_approximation_of_the_statistic)) %>%
    rowwise() %>%
    mutate(n_values_as_extreme_in_null_distribution_approximation = sum(null_distribution_approximation_of_the_statistic >= LikelihoodRatio))%>%
    rowwise() %>%
    mutate(pValue = (n_values_as_extreme_in_null_distribution_approximation + 1) /(size_null_distribution_approximation+1)) %>%
    ungroup()
 
    write.csv(p_values_df,
            file = "prerun/GPMelt_p_values.csv")
    
}else{
  
 p_values_df <- read.csv("prerun/GPMelt_p_values.csv")
 
}


```

### p-values histogram

```{r}
#| fig-format:
#|   pdf


BaseSize=15

n_pval <- Ratios_RealDataset %>% summarise(n_IDs = n_distinct(Level_1))

dat_title <- "Staurosporine 2021"
  
pval_histo <- p_values_df %>%
  ggplot() +
  geom_histogram(aes(x = pValue, y = after_stat(density)), fill = "steelblue", alpha = 0.5, boundary = 0, bins = 60) +
  geom_line(aes(x = pValue, y = dunif(pValue)), color = "darkred", size = 1.5) +
  ggtitle(dat_title)+
  geom_text(data = n_pval, aes(x = Inf, y = Inf, label = paste0("# p-values = ", n_IDs)), 
        hjust = 1.1, vjust = 1.1, inherit.aes = FALSE, color = "black") +
  scale_x_continuous(name ="p-value", labels=c("0", "0.25", "0.5","0.75",  "1"), breaks = c(0, 0.25, 0.5,0.75, 1))+
  theme_Publication(base_size = BaseSize)

print(pval_histo)
```
# comparison with NPARC results

## load NPARC results

The following results have been obtained by running NPARC on the dataset that can be found there: "data/Dataset_TMT11_2Rep2Cond_PearsonCorrelationAbove04.csv". The code is available in the file "Staurosporine2021_NPARC.qmd" of the current folder.

```{r}
load.Rdata(filename="prerun/NPARC_modelMetrics.RData",
           objname="NPARC_modelMetrics" )

load.Rdata(filename="prerun/NPARC_fStats.RData",
           objname="NPARC_fStats" )

load.Rdata(filename="prerun/NPARC_results.RData",
           objname="NPARC_fits" )



```


## Detect the non sigmoidal curves as protein IDs with outlier RSS1 

RSS1 computes the residual sum of score of the alternative model. Wihle this model is supposed to be more flexible (two sigmoids are fitted to the data, one per condition ), a large RSS1 suggests that at least one of the two sigmoid didn't fit the data well.

```{r}

GoodnessOfFit <- full_join(
  NPARC_modelMetrics,
  NPARC_fStats %>% dplyr::select(id, pVal))

RssOutliers <- GoodnessOfFit %>% 
  group_by(modelType) %>%
  summarise(iqr = IQR(rss, na.rm = TRUE),
            q1 = quantile(rss, probs=c(.25), na.rm = TRUE),
            q3 = quantile(rss, probs=c(.75), na.rm = TRUE),
            q90 = quantile(rss, probs=c(.90), na.rm = TRUE),
            q95 = quantile(rss, probs=c(.95), na.rm = TRUE),
            q97 = quantile(rss, probs=c(.97), na.rm = TRUE),
            q99 = quantile(rss, probs=c(.99), na.rm = TRUE)) %>%
  mutate(upOutlier = q3+ 1.5 * iqr )
```


### outlier rss 1 (90th-quantil)

```{r}

UpOutliers90 <-GoodnessOfFit %>% 
  full_join(RssOutliers %>% dplyr::select(modelType, q90)) %>% 
  group_by(modelType) %>% 
  filter((rss >= q90) ) %>%
  filter(modelType != "null") %>%
  ungroup()
  
length(unique(UpOutliers90$id))
```

```{r}
GoodnessOfFit_no_UpOutliers90 <- anti_join(
   GoodnessOfFit %>% dplyr::select(id,pVal) %>% distinct(),
   UpOutliers90 %>% dplyr::select(id,pVal) %>% distinct()
) %>%
  mutate(method="NPARC_removing_outliers") 

length(unique(GoodnessOfFit_no_UpOutliers90$id))
```

### p-values histogram (Supp Fig.7, panel E)

#### considering the number of IDs

```{r}
data_to_plot <- full_join(
  GoodnessOfFit %>% dplyr::select(id,pVal) %>% distinct() %>% mutate(method="NPARC"),
  GoodnessOfFit_no_UpOutliers90 %>% dplyr::select(id,pVal,method) %>% distinct() 
) %>% full_join(
  p_values_df %>% dplyr::select(Level_1,pValue) %>% distinct() %>% mutate(method="GPMelt") %>% dplyr::rename("id" = "Level_1", "pVal"="pValue")
) 

```

```{r}
#| fig-format:
#|   pdf


BaseSize=11

n_pval <- data_to_plot %>% group_by(method) %>% summarise(n_IDs = n_distinct(id))

dat_title <- "Staurosporine 2021"
  
pval_histo <- data_to_plot %>% 
  filter( ! is.na(pVal))%>%
  ggplot() +
  geom_histogram(aes(x = pVal, y = after_stat(density)), fill = "steelblue", alpha = 0.5, boundary = 0, bins = 20) +
  geom_line(aes(x = pVal, y = dunif(pVal)), color = "darkred", size = 1.5) +
  facet_grid(.~factor(method, 
                      levels = c("NPARC", "NPARC_removing_outliers", "GPMelt" ),
                      labels = c("NPARC\nNo removal", "NPARC\nRemoving 90% RSS-1 outliers", "GPMelt" )
                      ))+
  ggtitle(dat_title)+
  geom_text(data = n_pval, aes(x = Inf, y = Inf, label = paste0("N = ", n_IDs)), 
        hjust = 1.1, vjust = 1.1, inherit.aes = FALSE, color = "black") +
  scale_x_continuous(name ="p-value", labels=c("0", "0.25", "0.5","0.75",  "1"), breaks = c(0, 0.25, 0.5,0.75, 1))+
  theme_Publication(base_size = BaseSize)

print(pval_histo)
```

#### considering the number of p-values entering the histogram

```{r}
data_to_plot <- full_join(
  GoodnessOfFit %>% dplyr::select(id,pVal) %>% distinct() %>% mutate(method="NPARC"),
  GoodnessOfFit_no_UpOutliers90 %>% dplyr::select(id,pVal,method) %>% distinct() 
) %>% full_join(
  p_values_df %>% dplyr::select(Level_1,pValue) %>% distinct() %>% mutate(method="GPMelt") %>% dplyr::rename("id" = "Level_1", "pVal"="pValue")
) %>% filter( ! is.na(pVal))

```

```{r}

BaseSize=11

n_pval <- data_to_plot %>% group_by(method) %>% summarise(n_IDs = n_distinct(id))

dat_title <- "Staurosporine 2021"
  
pval_histo <- data_to_plot %>%
  ggplot() +
  geom_histogram(aes(x = pVal, y = after_stat(density)), fill = "steelblue", alpha = 0.5, boundary = 0, bins = 20) +
  geom_line(aes(x = pVal, y = dunif(pVal)), color = "darkred", size = 1.5) +
  facet_grid(.~factor(method, 
                      levels = c("NPARC", "NPARC_removing_outliers", "GPMelt" ),
                      labels = c("NPARC\nNo removal", "NPARC\nRemoving 90% RSS-1 outliers", "GPMelt" )
                      ))+
  ggtitle(dat_title)+
  geom_text(data = n_pval, aes(x = Inf, y = Inf, label = paste0("# p-values = ", n_IDs)), 
        hjust = 1.1, vjust = 1.1, inherit.aes = FALSE, color = "black") +
  scale_x_continuous(name ="p-value", labels=c("0", "0.25", "0.5","0.75",  "1"), breaks = c(0, 0.25, 0.5,0.75, 1))+
  theme_Publication(base_size = BaseSize)

print(pval_histo)
```

## Example of pathological cases (Fig S5, panel B)

Hereafter, we plot 3 proteins belonging to the $90\%$ RSS-1 or RSS-0 outliers for which the data are far from sigmoidal.

```{r}
GoodnessOfFit %>% 
  full_join(RssOutliers %>% dplyr::select(modelType, q90)) %>% 
  group_by(modelType) %>% 
  filter((rss >= q90) ) %>%
  ungroup() %>% 
  filter(id %in% c("SLC38A2", "RPL12", "LIMA1")) %>%
  dplyr::select(- c("loglik","tm_sd","nCoeffs","nFitted","conv"))
```

```{r}

#| fig-format:
#|   pdf


level1 <- c( "SLC38A2", "RPL12", "LIMA1")

gg_PathologicalCases <- ggplot()+
    # data
    geom_point(data=RealDataset%>%
              filter(Level_1 %in% level1) ,
              mapping=aes(x=x, y=y, color=Level_2, shape=as.factor(rep)), size=5)+
    # NPARC fit 
  geom_line(data=NPARC_fits[["predictions"]] %>%
              filter(id %in% level1) %>%
              filter(modelType != "null")%>%
              dplyr::rename("Group"="group", "Level_1"="id") %>%
              mutate(Model="NPARC"),
            mapping= aes(x=x, y=.fitted, color=Group), size=1)+
  #fit for GPMelt
  geom_line(data=Fits_RealDataset %>%
              filter(Level_1%in% level1) %>%
              filter(Level_2 %in% c("Control","Treatment" ))%>%
              mutate(Model="GPMelt"),
            mapping= aes(x=x, y=y, color=Level_2), size=1)+
  geom_ribbon(data=Fits_RealDataset %>%
              filter(Level_1%in% level1)%>%
              filter(Level_2 %in% c("Control","Treatment" ))%>%
              mutate(Model="GPMelt") ,
                aes(x=x,
                    ymin = conf_lower,
                    ymax=conf_upper,
                    fill=Level_2 ),
                alpha=0.3)+
    scale_color_manual(name="Condition", 
                     values = c("Control"="aquamarine3", 
                                "Treatment" = "darkorange2",
                                "null"="black"), 
                     labels= c("Control"="Control", 
                                "Treatment" = "Treatment",
                               "null"="null"), 
                     breaks=c("Control", "Treatment", "null"))+
    scale_fill_manual(name="Condition", 
                     values = c("Control"="aquamarine3", 
                                "Treatment" = "darkorange2",
                                "null"="black"), 
                     labels= c("Control"="Control", 
                                "Treatment" = "Treatment",
                               "null"="null"), 
                     breaks=c("Control", "Treatment", "null"))+
       scale_shape_manual(name="Replicate",
                     values=c("1"=16, 
                              "2" = 17,
                              "3"=15))+
    facet_wrap(factor(Model, levels=c("NPARC", "GPMelt"))~Level_1, scales="free_y")+
    xlab("Temperature [°C]")+
    ylab("Fold change")+
    theme_Publication(base_size=BaseSize)+
  theme(strip.text = element_text(size =rel(1)))

print(gg_PathologicalCases)


```

# impact of the size of the null distribution estimation

## compute the p-value for different number of samples $S$, with $S \in [2,4,6,8,10]$

For each possible number of samples, we try all possible combination of samples,a nd compute the p-values from this null distribution approximation.
 
```{r}

if(! file.exists("prerun/GPMelt_p_values_varying_number_samples.csv")){
  
  pValues_VaryingSamples <- vector(mode ="list", length=5)

  for(i in seq(2, 10, by=2) ){
    
    CombiList <- combn(c(1:10), m=i, simplify=FALSE)
    pValues_VaryingSamples[[i]]  <- vector(mode ="list", length=length(CombiList))
  
     for(CL in seq_along(CombiList)){
       
       null_distribution_approximation_of_the_statistic =  Ratios_NullDataset %>% 
       filter(sample_id %in% CombiList[[CL]]) %>%
       dplyr::select(LikelihoodRatio) %>%
       pull()
    
    pValues_VaryingSamples[[i]][[CL]] <- Ratios_RealDataset %>% 
      mutate(size_null_distribution_approximation = length(null_distribution_approximation_of_the_statistic)) %>%
      rowwise() %>%
      mutate(n_values_as_extreme_in_null_distribution_approximation = sum(null_distribution_approximation_of_the_statistic >= LikelihoodRatio))%>%
      rowwise() %>%
      mutate(pValue = (n_values_as_extreme_in_null_distribution_approximation + 1) /(size_null_distribution_approximation+1)) %>%
      ungroup()
    
     }
    
    pValues_VaryingSamples[[i]] <- data.table::rbindlist(pValues_VaryingSamples[[i]], idcol = "combi")
     
  
  }

  pValues_VaryingSamples_df <- data.table::rbindlist(pValues_VaryingSamples, idcol ="N_samples")
  
  write.csv(
    pValues_VaryingSamples_df,
    file = "prerun/GPMelt_p_values_varying_number_samples.csv"
  )

}else{
  
  pValues_VaryingSamples_df <- read.csv(file = "prerun/GPMelt_p_values_varying_number_samples.csv")
  
}



```


## load the GO terms annotation

This data frame contains information obtained from the GO terms (downloaded in march 2023) for our IDs. The columns indicate if the protein has a "protein-kinase activity".

```{r}
 UniprotInfo_and_GOterms <-read.csv(file="data/KinaseAndHitsInfoAboutIDs.csv") %>% dplyr::select(-X, -PublishedHits)

```


## Compute the ROC curves obtained with S samples per ID, for $S \in [2,4,6,8,10]$

```{r}
if(! file.exists("prerun/GPMelt_p_values_varying_number_samples_list.RData")){
  
  pValues_VaryingSamples_df_split <- split(pValues_VaryingSamples_df %>% ungroup(), 
                interaction(pValues_VaryingSamples_df$N_samples, pValues_VaryingSamples_df$combi),
                drop=TRUE)
}

```

```{r}

if(! file.exists("prerun/GPMelt_p_values_varying_number_samples_list.RData")){
  
  ROC_Curve_results_List <- vector(mode="list", length = length(pValues_VaryingSamples_df_split))
  names(ROC_Curve_results_List) <- names(pValues_VaryingSamples_df_split)
  
  ROC_Curve_data_List <- vector(mode="list", length = length(pValues_VaryingSamples_df_split))
  names(ROC_Curve_data_List) <- names(pValues_VaryingSamples_df_split) 
    
  for(inter in names(pValues_VaryingSamples_df_split)){
    
    pValues_VaryingSamples_df_split[[inter]] <- pValues_VaryingSamples_df_split[[inter]] %>% 
      mutate(p_adj = p.adjust(pValue, method = "BH")) %>% 
      mutate(score =  1-p_adj) %>%
      left_join(
        UniprotInfo_and_GOterms,
        by=c("Level_1"="gene_name")
      )
    
     ROC_Curve_results_List[[inter]] <- rocit(
        score = pValues_VaryingSamples_df_split[[inter]] %>% dplyr::select(score) %>% pull(), 
        class = pValues_VaryingSamples_df_split[[inter]] %>% dplyr::select(ProteinKinaseActivity_27032023) %>% pull()) # note that some IDs miss the annotations
      
      ROC_Curve_data_List[[inter]] <- data.frame(
        statistic = "p_adj", 
        scaling ="FC",
        FPR = ROC_Curve_results_List[[inter]]$FPR,
        TPR = ROC_Curve_results_List[[inter]]$TPR,
        cutoff = ROC_Curve_results_List[[inter]]$Cutoff)
    
  
  }
  
    DF_ROCData <- data.table::rbindlist(ROC_Curve_data_List, idcol = "N_samples.combi_id") %>%
      tidyr::separate(col="N_samples.combi_id", into=c("N_samples","combi_id"), sep="\\.", remove=FALSE)

    save(
      pValues_VaryingSamples_df_split,
      file ="prerun/GPMelt_p_values_varying_number_samples_list.RData"
    )
    
    write.csv(
      DF_ROCData,
      file ="prerun/GPMelt_ROCcurves_varying_number_samples_df.csv"
    )
  
}else{
  
  DF_ROCData <- read.csv(file ="prerun/GPMelt_ROCcurves_varying_number_samples_df.csv") %>%
    mutate(method="GPMelt") %>%
    dplyr::select(-X)
  
}
```

## compute ROC curve for NPARC

We have seen previously that the p-value histogram of NPARC presents a peak on the right. Additionaly, we have seen that, by removing the $90\%$ RSS-1 outliers, once can recover a flat right tail of the p-value distribution. We also saw that these $90\%$ RSS-1 outliers corespond to non-sigmoidal curves which are incorrectly fitted by NPARC. Hence, in the following ROC curve analysis, we propose to
 
* remove the $90\%$ RSS-1 outliers prioir to the BH correction on the p-values.
* set to 1 the adjusted p-value of the $90\%$ RSS-1 outliers and of all IDs with p-value = 1.

```{r}
NPARC_results_forROC <- GoodnessOfFit_no_UpOutliers90 %>%
  filter(! is.na(pVal)) %>%
   mutate(p_adj = p.adjust(pVal, method = "BH")) 

outliers_NPARC <- anti_join(
  GoodnessOfFit %>% 
    dplyr::select(id,pVal) %>% 
    distinct(),
  NPARC_results_forROC
)

length(unique(NPARC_results_forROC$id)) # expect 3720 IDs

length(unique(outliers_NPARC$id)) # expect 683 IDs

NPARC_results_forROC <- full_join(
  NPARC_results_forROC %>%
    mutate(isOutlier = FALSE), 
  outliers_NPARC %>%
    mutate(isOutlier = TRUE) %>%
    mutate(p_adj = 1)
)

length(unique(NPARC_results_forROC$id)) # expect 4403 IDs
```


```{r}

NPARC_results_forROC <- NPARC_results_forROC %>% 
  left_join(
    UniprotInfo_and_GOterms,
    by=c("id"="gene_name")
  ) %>% 
  mutate(score =  ifelse(is.na(p_adj), 0, 1-p_adj)) # set p_adj =1 for if pVal=NA. 95 IDs have no p-values computed by NPARC.

ROC_Curve_results_NPARC <- rocit(
      score = NPARC_results_forROC %>% dplyr::select(score) %>% pull(), 
      class = NPARC_results_forROC %>% dplyr::select(ProteinKinaseActivity_27032023) %>% pull()) # note that some IDs miss the annotations
      
DF_ROCData_withNPARC <- rbind(
  DF_ROCData,
  data.frame(
    N_samples.combi_id=NA,
    N_samples = NA,
    combi_id = NA,
    statistic = "p_adj", 
    scaling ="FC",
    method ="NPARC",
    FPR = ROC_Curve_results_NPARC$FPR,
    TPR = ROC_Curve_results_NPARC$TPR,
    cutoff = ROC_Curve_results_NPARC$Cutoff
    )
)

      
```

## compute the thresholds

```{r}
DF_ROCData_Threshold <- c()

for (Thres  in c(0.005, 0.01, 0.015)){
  
    Th=1-Thres
    DF_ROCData_BasicWithTh <- DF_ROCData_withNPARC %>%
      mutate(DistToTh = abs(Th-cutoff) ) %>% 
      group_by(N_samples.combi_id, method)   %>% 
      mutate(MinDistToTh = (DistToTh == min(abs(Th-cutoff)) )) 
      
    
    DF_ROCData_Threshold <- rbind(
      DF_ROCData_Threshold, 
      DF_ROCData_BasicWithTh %>% filter(MinDistToTh==TRUE) %>% mutate(Threshold = Thres) %>% mutate(Threshold2 = 1-min(cutoff, Th)))
    
  
}

```


## Fig S2

```{r}

#| fig-format:
#|   pdf

GPMelt_representative <- DF_ROCData_Threshold %>% 
               filter(method !="NPARC") %>%
               group_by(N_samples,Threshold) %>%
               filter(DistToTh == min(DistToTh)) %>%
  slice_sample(n=1)
  
NCombi <- DF_ROCData_withNPARC %>%
  filter(method !="NPARC")  %>%
  group_by(N_samples) %>%
  summarise(NCombination = n_distinct(combi_id))


  

ROC_Staurosporine2021 <- ggplot()+
  # shows all possible ROC curves in grey
  geom_line(data=DF_ROCData_withNPARC %>% filter(method !="NPARC"), 
            mapping=aes(x= FPR,
                        y= TPR, 
                        group = N_samples.combi_id),
            size=1, color="grey") +
  # shows one specific ROC cruve from GPMelt 
   geom_line(data = DF_ROCData_withNPARC %>% 
               inner_join(GPMelt_representative%>%
             ungroup() %>% dplyr::select(N_samples.combi_id,N_samples,combi_id )), 
          mapping=aes(x= FPR,
                      y= TPR, 
                      color = "GPMelt"),
          size=1) +
  # shows NPARC ROC curve
 geom_line(data = DF_ROCData_withNPARC %>% 
             filter(method =="NPARC") %>% 
             dplyr::select(-N_samples) %>%
             ungroup(), 
          mapping=aes(x= FPR,
                      y= TPR, 
                      color = "NPARC"),
          size=1) +
  # shows threshold for GPMelt representative 
  geom_point(data = GPMelt_representative %>%
              ungroup() ,
             mapping=aes(x= FPR, y= TPR, fill="GPMelt",shape = as.factor(Threshold)),
             size=3)+
  # shows threshold for NPARC 
  geom_point(data = DF_ROCData_Threshold %>% 
               filter(method =="NPARC")  %>%
               dplyr::select(-N_samples) %>%
              ungroup() ,
             mapping=aes(x= FPR, y= TPR, fill = "NPARC", shape = as.factor(Threshold)),
             size=3)+
  # text
  geom_text(data= NCombi, 
            aes(x = 0.025, y=0, label = paste0("# Combinations = ",NCombination )), hjust = 0, size =5, color="grey")+
  # additional plots specifications
  facet_grid(.~factor(N_samples, levels=c("2","4","6", "8", "10")))+
  #  guides(shape = guide_legend(title = TeX("$\\alpha$-Threshold")))+
  scale_color_manual(name ="Models",
                    labels=c("NPARC"= "NPARC" ,
                             "BayesianSig"=  "Bayesian Sigmoid",
                             "semi_param_Bayesian" ="Bayesian Semi-parametric",
                             "GPMelt"="GPMelt"),
                    values = c("NPARC"= "#422CB2" ,
                             "BayesianSig"= "#85B22C",
                             "semi_param_Bayesian" ="#2C85B2",
                             "GPMelt"="#B22C2C"))+
   scale_fill_manual(name ="Models",
                    labels=c("NPARC"= "NPARC" ,
                             "BayesianSig"=  "Bayesian Sigmoid",
                             "semi_param_Bayesian" ="Bayesian Semi-parametric",
                             "GPMelt"="GPMelt"),
                    values = c("NPARC"= "#422CB2" ,
                             "BayesianSig"= "#85B22C",
                             "semi_param_Bayesian" ="#2C85B2",
                             "GPMelt"="#B22C2C"))+
  scale_shape_manual(name = TeX("$\\alpha$-Threshold"),
                     values =c("0.003"=24,
                               "0.005"=22,
                               "0.01"=21,
                               "0.015"= 23))+
  guides(fill ="none",
         color = guide_legend(override.aes = list(linewidth = 3, size=3)))+
  ylim(c(0,0.50))+
  scale_x_continuous(name = "1 -  Specificity (FPR)", breaks =c(0, 0.025, 0.05, 0.075, 0.1),labels =c(0, 0.025, 0.05, 0.075, 0.1), limits =c(0,0.1) )+
  ylab("Sensitivity (TPR)")+
  ggtitle("Performance of GPMelt in function of the number of samples per ID (Staurosporine 2021 dataset)",
    subtitle =paste0("Approximation of ground truth using GO term Protein kinase activity"))+
  theme_Publication(base_size=BaseSize)+
  theme(legend.position="bottom", legend.box="vertical", legend.margin=margin())

print(ROC_Staurosporine2021)

```


# Session info

```{r}
sessionInfo()
```








```{r}

```

