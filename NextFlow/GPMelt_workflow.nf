include { INPUT_FORMATTING   } from './modules_input_formating.nf'
include { INPUT_FORMATTING_WITHSUBSET } from './modules_input_formating.nf'
include { RUN_GPMELT_IN_BATCH } from './modules_run_gpmelt.nf'
include { COMPUTE_GPMELT_STATISTICS } from './modules_run_gpmelt.nf'
include { GATHER_AND_SAVE_RESULTS } from './module_results_gathering.nf'

log.info """\
    GPMelt   P I P E L I N E
    ===================================
    dataset: ${params.dataset}
    nb sample for null distribution approximation: ${params.nb_sample}
    parameters file: ${params.parameter_file}
    """
    .stripIndent(true)


workflow {
    
    // define the first chanels
    Channel
        .fromPath(params.dataset)
        .set { dataset_ch }

    Channel
        .fromPath(params.nb_sample)
        .set { nb_sample_ch }
    
    Channel
        .fromPath(params.parameter_file)
        .set { parameter_ch }
    
    // should be a value channel that can be consumed any number of times by a process or operator.
    // typically, RUN_GPMELT_IN_BATCH will need to consum it as many times as values in formatted_inputs.input_data_pkl.flatten()
    Channel
        .value(params.permanent_results_directory)
        .set { permanent_results_directory_ch }
    
    Channel
        .value(params.number_IDs_per_jobs)
        .set { number_id_per_batch_ch }
    
    Channel
        .value(params.minimise_required_memory_space)
        .set { minimise_required_memory_space_ch }

    // prepare the input data for GPMelt
    if (params.dataset_subsetting) {

        Channel
        .fromPath(params.ids4subset_file)
        .set { ids4_dataset_subset_ch }

        formatted_inputs = INPUT_FORMATTING_WITHSUBSET(dataset_ch, nb_sample_ch, parameter_ch, permanent_results_directory_ch, number_id_per_batch_ch, minimise_required_memory_space_ch, ids4_dataset_subset_ch)

    } else {

        formatted_inputs = INPUT_FORMATTING(dataset_ch, nb_sample_ch, parameter_ch, permanent_results_directory_ch,number_id_per_batch_ch,minimise_required_memory_space_ch)

    }
    //formatted_inputs = workflow_without_subsetting(dataset_ch, nb_sample_ch, parameter_ch,permanent_results_directory_ch)
    
    // check what has been emitted
    formatted_inputs.input_data_pkl.subscribe { println "Got: $it as output of INPUT_FORMATTING" } 
    
    // run GPMelt on batches of IDS
    gpmelt_results = RUN_GPMELT_IN_BATCH(formatted_inputs.input_data_pkl.flatten(),permanent_results_directory_ch)
    
    // check what has been emitted
    gpmelt_results.results_pkl.subscribe { println "Got: $it as output of RUN_GPMELT_IN_BATCH." } 

    // with gpmelt_results.results_pkl.collect() we ensure that GATHER_AND_SAVE_RESULTS only starts once all results_pkl have been gathered
    gather_results = GATHER_AND_SAVE_RESULTS(gpmelt_results.results_pkl.collect(),permanent_results_directory_ch)

    // check the output csv from GATHER_AND_SAVE_RESULTS
    gather_results.likelihood_ratios_sampled_dataset.subscribe { println "Got: $it as output of GATHER_AND_SAVE_RESULTS." } 
    gather_results.likelihood_ratios_real_dataset.subscribe { println "Got: $it as output of GATHER_AND_SAVE_RESULTS." } 
    
    // compute the p-values
    COMPUTE_GPMELT_STATISTICS(gather_results.likelihood_ratios_real_dataset, gather_results.likelihood_ratios_sampled_dataset, formatted_inputs.parameter_dict_pkl,permanent_results_directory_ch)
    
}

