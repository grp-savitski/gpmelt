#! /usr/bin/env python3

import pandas as pd
import sys
import os
import pickle 
import ast 
import logging
import gpytorch
from pathlib import Path

from utils.ComputeScaling import compute_ScalingFactorPerReplicate
from utils.Check_modelTypeValidity import Check_modelTypeValidity
from utils.create_df_to_run_gpmelt import create_id_dataframe
from utils.read_csv_with_index_detection import read_csv_with_index_detection

########################################
# in this python script, we
# 1) read the parameter file
# 2) read the datafile
# 3) check the validity of the model & data columns
# 4) scale the data if required & save in a final directory
# 5) subset the columns, sort the dataset & subset the IDs (if required)
# 6) read the number of samples per IDs
# 7) For each IDs, subset the data, the number of samples and combine with a copy of parameter dictionary
# 8) combine the IDs in group (size of the group is defined in the parameters) and save one pkl file for each group
# 9) output the list of pkl file on which GPMelt should be run, which is saved on the temporary working directory of the Nextflow workflow
########################################s

def main(dataset_csv, nb_sample_csv, parameter_file_txt, number_IDs_per_jobs, minimise_required_memory_space,  Ids4subset_csv = None, debug=True):

    if debug:
        logging.basicConfig(filename='debug.log', level=logging.DEBUG)
        logging.debug("##############################")
        logging.debug("Entering the script to format the inputs for GPMelt")
        
    r"""
    Args:
        dataset_csv (string):
            path to the input dataset saved as a csv file.
            Expected is a dataset with variables "Level_1 to Level_L", indicating the meaning of the different levels of the hierarchy
            "rep" for replicates
            "condition" (optinal) for condition (Control vs Treatment)
            "y" for observed intensities (raw or scaled)
        
        nb_sample_csv (string):
            path to the dataset containing the number of samples to draw per ID, saved as a csv file.

        Ids4subset_csv (string, optinal): 
            path to a csv file with entry "Level_1", containing IDs that can be found in the "Level_1" column of the input dataset
            For dataset subsetting (to test different model specification)
        
            
    """

    number_IDs_per_jobs = int(number_IDs_per_jobs)
    minimise_required_memory_space = (minimise_required_memory_space=="True") | (minimise_required_memory_space=="true")
    
    #######################################
    ### Parameters loading
    #######################################

    # reading the data from the file 
    if os.path.isfile(parameter_file_txt):
        with open(parameter_file_txt) as f:
            parameter_dict_txt = f.read()
            parameter_dict = ast.literal_eval(parameter_dict_txt) 

            if debug:
                logging.debug('This is how the parameter dict looks like: \n')
                logging.debug(parameter_dict)
    else:
        raise ValueError(f"The parameter dictionary does not exists.")

    #add the parameter 
    parameter_dict['Minimise_required_memory_space'] = minimise_required_memory_space
    #######################################
    ### Check required keys in parameters 
    #######################################
    required_keys = ['Scaling', 'modelType', 'control_condition', 'training_iterations', 'LearningRate', 'Amsgrad', 'mean', 'n_PredictionsPoints', 'PlotSave', 'lengthscale_prior', 'lengthscale_MinConstraint', 'prediction_type', 'Minimise_required_memory_space', 'GPMelt_statistic']

    for key in required_keys:
        if not key in parameter_dict.keys():
            raise ValueError(f"Missing key {key} in the parameter dictionary!")

    #######################################
    ### Parameters formatting
    #######################################
    # redefine the mean from a string to a gpytorch object
    # note that 'None' and 'True/False' will be transformed to the correct type using 'ast.literal_eval', but not gpytorch.means objects.
    # question: can we do it better than this?
    if parameter_dict['mean'] == "gpytorch.means.ZeroMean()":
        parameter_dict['mean'] = gpytorch.means.ZeroMean()
    elif parameter_dict['mean'] == "gpytorch.means.ConstantMean()":
        parameter_dict['mean'] = gpytorch.means.ConstantMean()
    else:
        raise NotImplementedError(f"The selected mean ( " + parameter_dict['mean'] + " ) is not yet implemented as part of GPMelt. Please choose between gpytorch.means.ZeroMean() and gpytorch.means.ConstantMean()." )

    #######################################
    ### Dataset loading, scaling & formatting (re-ordering of the rows and subsetting of the columns )
    #######################################

    if os.path.isfile(dataset_csv):

        #index_col=0 assumes that the dataset_csv has been saved with an index, i.e NOT like df.to_csv("*.csv", index=False)
        #data = pd.read_csv(dataset_csv,index_col=0)
        data = read_csv_with_index_detection(dataset_csv, debug=debug)
        
        ## Check that the dataset has the required variable given the model type ##
        NLevels, dfColnames_Levels = Check_modelTypeValidity(data, parameter_dict['modelType'])

        if not set(['rep','x', 'y']).issubset(data.columns):
            raise ValueError(f"The provided dataset should contain the variable rep - for replicates - and x,y - for training inputs.")
        
        ## data scaling ##
        if not parameter_dict['Scaling'] is None:

            if not parameter_dict['Scaling'] in ["log", "mean", "median", "geometricMean", "FC"]:
                raise ValueError(f"Only log, mean, median, geometricMean and FC scaling are possible.")
    
            scaledData = compute_ScalingFactorPerReplicate(data, NLevels)
            scaledData.to_csv(index=True, path_or_buf = "data_with_scalingFactors.csv")
            
            data = scaledData.rename(columns={'y':'y_raw'}).rename(columns={"y_" + parameter_dict['Scaling'] if parameter_dict['Scaling'] =="log" else "y_" + parameter_dict['Scaling'] + '_Scaling':'y'})
        
        # columns to use for dataframe ordering
        column_order = dfColnames_Levels[:1] + [ 'condition' if  'condition' in data.columns else ''] + dfColnames_Levels[1:] + ['rep','x']
        column_order = [x for x in column_order if x]
        
        # first subset the columns as data might have a lot of new column after applying compute_ScalingFactorPerReplicate
        data = data[column_order + ['y']]

        # order the data frame according to some columns
        data = data.sort_values(column_order)

        # remove rows for which the input "y" is missing
        data.dropna(subset=['y'], inplace=True)

        # remove duplicated rows
        data = data.drop_duplicates()

        if debug:
            logging.basicConfig(filename='debug.log', level=logging.DEBUG)
            logging.debug('This is how the input data look like: \n')
            logging.debug(data.head())
    
    else:
        raise ValueError(f"The provided csv path for the dataset is invalid")

    #######################################
    ### Specify the number of samples per ID
    #######################################

    if os.path.isfile(nb_sample_csv):
        NumberSamples_perID = pd.read_csv(nb_sample_csv,index_col=0)
    else:
        raise ValueError(f"The provided csv path for the number of samples per ID is invalid")

    #######################################
    ### Dataset subsetting (for model specification testing)
    #######################################
    
    if (Ids4subset_csv is not None):

        if os.path.isfile(Ids4subset_csv):

            IDsToKeep_df = pd.read_csv(Ids4subset_csv,index_col=0) 
            IDsToKeep = IDsToKeep_df['Level_1'].values 
            data_subset = data.loc[ data["Level_1"].isin(IDsToKeep)].drop_duplicates() 
            NumberSamples_perID = NumberSamples_perID.loc[ NumberSamples_perID["Level_1"].isin(IDsToKeep)].drop_duplicates() 

            if debug:
                logging.basicConfig(filename='debug.log', level=logging.DEBUG)
                logging.debug('The following list of Ids has been used as subset to test the model specification: \n')
                logging.debug(IDsToKeep)
        
        else:

            raise ValueError(f"The provided csv path for the Ids subsetting is invalid")

    else:

        data_subset = data.drop_duplicates()

    #######################################
    ### Order Ids per number of samples 
    #######################################
    NumberSamples_perID = NumberSamples_perID.sort_values(['NSamples'], ascending=False)
    
    #create ordered unique list of IDs
    UniqueIDs = NumberSamples_perID['Level_1'].drop_duplicates().tolist()

    #######################################
    ### Create a dataframe defining how many IDs should be run simultaneously per job
    #######################################
    # this data frame will be saved at the same place as 'UniqueIDs.csv'
    GPMelt_rules = create_id_dataframe(dir_name = "", N_IDs = len(UniqueIDs), N_subsequent_ids = number_IDs_per_jobs)
      
    #######################################
    ### save a dictionary for each row of GPMelt_rules
    #######################################
    for row in GPMelt_rules.itertuples(index=True, name='Pandas'):
        Global_dict = {}
        for id in UniqueIDs[row.min_id :row.max_id+1]:
            InputDict = {}
            InputDict['data'] = data_subset[data_subset.Level_1 == id]
            InputDict['numberSamples_perID'] = NumberSamples_perID[NumberSamples_perID.Level_1 == id]
            InputDict['parameters'] = parameter_dict
            Global_dict[id] = InputDict

        # account for pkl_files_directory in case it is specified
        with open(os.path.join(row.pkl_files_directory, row.pkl_files_name + '.pkl'), 'wb') as f:
            pickle.dump(Global_dict, f)
            if debug:
                logging.debug("saved a global pickle file")

        if debug:
            logging.basicConfig(filename='debug.log', level=logging.DEBUG)
            logging.debug('saved pickle file for IDs: \n')
            logging.debug(str(row.min_id) + " to " + str(row.max_id))
            logging.debug("pickle file name: " +  row.pkl_files_name)


    #######################################
    ### save GPMelt_rules & UniqueIDs & parameter_dict
    #######################################
    # Note: not sure UniqueIDs.csv is required.
    GPMelt_rules.to_csv('GPMelt_rules.csv', index=False)
    UniqueIDs = pd.DataFrame(UniqueIDs)
    UniqueIDs.to_csv('UniqueIDs.csv', index=False)    
    with open('parameter_dict.pkl', 'wb') as f:
        pickle.dump(parameter_dict, f)

  

if __name__ == '__main__':
    main(*sys.argv[1:])
