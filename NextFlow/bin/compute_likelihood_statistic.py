#! /usr/bin/env python3

import logging
import sys
import pandas as pd
import pickle

from utils.compute_n_larger_or_equal import calculate_sum_extreme


def main(likelihood_ratios_real_dataset_csv, likelihood_ratios_sampled_dataset_csv, parameters_dict_pkl,  debug=True):

    if debug:
        logging.basicConfig(filename='debug.log', level=logging.DEBUG)
        logging.debug('################################################################')
        logging.debug('Entering script computing GPMelt statistics: \n')

    likelihood_ratios_real_dataset = pd.read_csv(likelihood_ratios_real_dataset_csv) 
    likelihood_ratios_sampled_dataset = pd.read_csv(likelihood_ratios_sampled_dataset_csv)

    with open(parameters_dict_pkl, 'rb') as f:
        parameters_dict = pickle.load(f)
    
    if parameters_dict['GPMelt_statistic'] == "dataset-wise":

        if debug:
            logging.debug("the mode 'dataset-wise' has been chosen to compute p-values from GPMelt statistics." )
            logging.debug("This means that the null distribution approximation is obtained by gathering the statistics computed on the samples coming from all IDs of the dataset.")

        # Step 1: Create a new DataFrame from likelihood_ratios_real_dataset
        p_val = likelihood_ratios_real_dataset.copy()

        # Step 2: define the null distribution approximation of the statistic
        null_distribution_approximation_of_the_statistic = likelihood_ratios_sampled_dataset['LikelihoodRatio'].tolist()

        # Step 3: compute the size of the null distribution approximation
        p_val['size_null_distribution_approximation'] = len(null_distribution_approximation_of_the_statistic)

        # Step 4: Apply the function to calculate, for each ID, the number of values as extreme in the null distribution approximation 
        p_val['n_values_as_extreme_in_null_distribution_approximation'] = p_val['LikelihoodRatio'].apply(calculate_sum_extreme, null_distribution_approximation_of_the_statistic=null_distribution_approximation_of_the_statistic)

        # Step 5: Calculate pValue
        p_val['pValue'] = (p_val['n_values_as_extreme_in_null_distribution_approximation'] + 1) / (p_val['size_null_distribution_approximation'] + 1)

    elif parameters_dict['GPMelt_statistic'] == "ID-wise":

        if debug:
            logging.debug("the mode 'ID-wise' has been chosen to compute p-values from GPMelt statistics." )


        # Initialize an empty DataFrame to store the results
        p_val = pd.DataFrame()

        # Process each value of Level_1 independently
        levels = likelihood_ratios_real_dataset['Level_1'].unique()
        for level1 in levels:
            # Subset the sampled dataset
            subset_sampled = likelihood_ratios_sampled_dataset[likelihood_ratios_sampled_dataset['Level_1_short'] == level1]
            null_distribution_approximation_of_the_statistic = subset_sampled['LikelihoodRatio'].tolist()

            # Subset the real dataset
            subset_real = likelihood_ratios_real_dataset[likelihood_ratios_real_dataset['Level_1'] == level1].copy()

            # Compute the size of the null distribution approximation
            subset_real['size_null_distribution_approximation'] = len(null_distribution_approximation_of_the_statistic)

            # Apply the function to calculate, for each ID, the number of values as extreme in the null distribution approximation 
            subset_real['n_values_as_extreme_in_null_distribution_approximation'] = subset_real['LikelihoodRatio'].apply(
                calculate_sum_extreme, null_distribution_approximation_of_the_statistic=null_distribution_approximation_of_the_statistic)

            # Calculate pValue
            subset_real['pValue'] = (subset_real['n_values_as_extreme_in_null_distribution_approximation'] + 1) / (
                        subset_real['size_null_distribution_approximation'] + 1)

            # Append the results to the p_val DataFrame
            p_val = pd.concat([p_val, subset_real], ignore_index=True)
    else:
        raise NotImplementedError(f"Only the 'dataset-wise' and 'ID-wise' versions of GPMelt statistic is implemented.")

    ''' 
    ######################################################################################################################################################################################################################################################
    # The following code could be used for the "group-wise" version, if modification of the code is added such that the "group" variable is kept in likelihood_ratios_sampled_dataset and likelihood_ratios_real_dataset
    ######################################################################################################################################################################################################################################################
    elif parameters_dict['GPMelt_statistic'] == "group-wise" and not parameters_dict['GPMelt_statistic_group'] is None:

        if (not parameters_dict['GPMelt_statistic_group'] in likelihood_ratios_sampled_dataset.column) or (not parameters_dict['GPMelt_statistic_group'] in likelihood_ratios_real_dataset.column):
        
            raise ValueError(f"If the group-wise computation of GPMelt statistic is chosen, then likelihood_ratios_df.csv and likelihood_ratios_sampled_dataset_df.csv dataframes should contain one column designing the group. According to the parameters chosen by the user, the column {parameters_dict['GPMelt_statistic_group']} should be present in these 2 data frames but is not.")
       
        else:
            # Initialize an empty DataFrame to store the results
            p_val = pd.DataFrame()
            group_col = parameters_dict['GPMelt_statistic_group']

            # Process each group independently
            groups = likelihood_ratios_real_dataset[group_col].unique()
            for group in groups:
                # Subset the sampled dataset
                subset_sampled = likelihood_ratios_sampled_dataset[likelihood_ratios_sampled_dataset[group_col] == group]
                null_distribution_approximation_of_the_statistic = subset_sampled['LikelihoodRatio'].tolist()

                # Subset the real dataset
                subset_real = likelihood_ratios_real_dataset[likelihood_ratios_real_dataset[group_col] == group].copy()

                # Compute the size of the null distribution approximation
                subset_real['size_null_distribution_approximation'] = len(null_distribution_approximation_of_the_statistic)

                # Apply the function to calculate SumExtreme for each row
                subset_real['n_values_as_extreme_in_null_distribution_approximation'] = subset_real['LikelihoodRatio'].apply(
                    calculate_sum_extreme, null_distribution_approximation_of_the_statistic=null_distribution_approximation_of_the_statistic)

                # Calculate pValue
                subset_real['pValue'] = (subset_real['n_values_as_extreme_in_null_distribution_approximation'] + 1) / (
                            subset_real['size_null_distribution_approximation'] + 1)

                # Append the results to the p_val DataFrame
                p_val = pd.concat([p_val, subset_real], ignore_index=True)

    else:

        raise NotImplementedError(f"Only the 'dataset-wise', 'ID-wise' or 'group-wise' versions of GPMelt statistic is implemented.")
    '''


    if debug:
        logging.debug('################################################################')
    
    p_val_computation_name = parameters_dict['GPMelt_statistic'].replace("-", "_")
    p_val.to_csv(f"p_values_{p_val_computation_name}.csv", index=False)   

    return p_val
    

if __name__ == '__main__':
    main(*sys.argv[1:])