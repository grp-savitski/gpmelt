#! /usr/bin/env python3

from matplotlib import pyplot as plt
import pandas as pd
import numpy as np
from pathlib import Path
import pickle
import multiprocessing as mp
import pickle

import os
import sys
import logging

from fitting_functions.fit_evaluate_sample import fit_evaluate_predict_and_sample_HGPM, fit_evaluate_HGPM_on_one_sample
from utils.Check_modelTypeValidity import Check_modelTypeValidity

########################################
# in this python script, we
# 1) read the pkl file associated to the ID of interest
# 
# ########################################
def testfunction(df, parameters ={}, debug=False):
    return {'test1': df, 'test2':df}

def main(ID_pkl_file, NUM_CPU=2, debug=True):

    with open(ID_pkl_file, 'rb') as f:
        ID_pkl = pickle.load(f)
    
    data = ID_pkl['data'] 
    numberSamples = ID_pkl['numberSamples_perID'] 
    parameters = ID_pkl['parameters'] 

    ## Check that the dataset has the required variable given the model type ##
    NLevels, dfColnames_Levels = Check_modelTypeValidity(data, parameters['modelType'])

    ##################################################
    ## First step is to fit the full HGPM on the real data, evaluate the fit on the full and joint model and possibly sample fromt he joint model.
    ##################################################
    # we want to save the results for each ID

    parameters['save_individual_results'] = True

    results_fit_evaluate_and_sample_HGPM = fit_evaluate_predict_and_sample_HGPM(
        data, 
        parameters,
        sampling=True,
        numberSamples_df = numberSamples,
        debug=True)
    
    sampled_dataset = results_fit_evaluate_and_sample_HGPM['samples_df']

    if sampled_dataset is not None:
        ##################################################
        ## As a seconf step we preprocess the dataset obtained by generating samples from the joint model.
        ##################################################
        # preprocess the dataset made of samples under the null 
        sampled_dataset['Level_1_short'] = sampled_dataset['Level_1']
        sampled_dataset['Level_1'] =  sampled_dataset['Level_1_short'] + "_Sample" + sampled_dataset['Dataset'].astype(int).astype(str) + "_CtrlCd" + sampled_dataset['control_condition']
        sampled_dataset = sampled_dataset.drop(columns={'Level_2_ForNullHyp'})

        ##################################################
        ## Fitting of the null dataset ##
        # All samples are fitted in parallel via multiprocessing
        ##################################################
        #'''
        Level1_Samples = sampled_dataset.Level_1.unique()

        # Initialize an empty dictionary to collect DataFrames
        combined_results_dict = {}

        for level1 in Level1_Samples:
            
            df = sampled_dataset[sampled_dataset.Level_1 == level1]
            
            df = df.drop(columns=['Level_1_short'])
            df = df.sort_values( dfColnames_Levels + ['rep',  'x'])
        
            print("Enter fitting of sample : " + level1)
            result_dict = fit_evaluate_HGPM_on_one_sample(
                df, 
                parameters,
                debug=debug)
            
            #print(result_dict)
            
            # directly combine the results obtained on each level1
            for key, df in result_dict.items():
                if key not in combined_results_dict:
                    combined_results_dict[key] = []
                combined_results_dict[key].append(df)

        # Concatenate the DataFrames for each key and save as CSV
        for key, df_list in combined_results_dict.items():
            combined_df = pd.concat(df_list, ignore_index=True)
            combined_df.to_csv(os.path.join(f"{key}_null_distribution_approximation.csv"), index=False)

        #'''
        ##################################################
        '''
        pool = mp.Pool(int(NUM_CPU))
        results_objects = [] 

        Level1_Samples = sampled_dataset.Level_1.unique()

        for level1 in Level1_Samples:
            
            df = sampled_dataset[sampled_dataset.Level_1 == level1]
            
            df = df.drop(columns=['Level_1_short'])
            df = df.sort_values( dfColnames_Levels + ['rep',  'x'])
        
            print("Enter fitting of sample : " + level1)
            results_objects.append(pool.apply_async(fit_evaluate_HGPM_on_one_sample, args=(df,parameters, debug)))

        results = [r.get() for r in results_objects] #this will wait all parallels jobs to be done before closing the pool
        
        pool.close()
        pool.join() 

        # Initialize an empty dictionary to collect DataFrames
        combined_results_dict = {}

        # Iterate over each result dictionary
        for res_dict in results:
            # directly combine the results obtained on each level1
            for key, df in res_dict.items():
                if key not in combined_results_dict:
                    combined_results_dict[key] = []
                combined_results_dict[key].append(df)
        
        # Concatenate the DataFrames for each key and save as CSV
        for key, df_list in combined_results_dict.items():
            combined_df = pd.concat(df_list, ignore_index=True)
            combined_df.to_csv(os.path.join(parameters['directory_name_for_null_dataset_sampling'], f"{key}_null_distribution_approximation.csv"), index=False)
        '''

        ##################################################
        '''
        pool = mp.Pool(int(NUM_CPU))
        results_objects = [] 

        Level1_Samples = sampled_dataset.Level_1.unique()

        for level1 in Level1_Samples:
            
            df = sampled_dataset[sampled_dataset.Level_1 == level1]
            
            df = df.drop(columns=['Level_1_short'])
            df = df.sort_values( dfColnames_Levels + ['rep',  'x'])
        
            print("Enter fitting of sample : " + level1)
            #results_objects.append(pool.apply_async(testfunction, args=(df,parameters, debug)))
            results_objects.append(pool.apply_async(fit_evaluate_HGPM_on_one_sample, args=(df,parameters, debug)))

        results = [r.get() for r in results_objects] #this will wait all parallels jobs to be done before closing the pool
        
        pool.close()
        pool.join() 

        print(results)
        '''

if __name__ == '__main__':
    main(*sys.argv[1:])