#! /usr/bin/env python3

import gpytorch
import torch
import numpy as np
import pandas as pd
import logging

from utils.Check_modelTypeValidity import Check_modelTypeValidity

from models.HadamardMultitask_Models import (
    HadamardMultitaskGPModel_2Levels_OneLengthscale_FixedLevels1and2,
    ######### 3 levels HGPM #########
    HadamardMultitaskGPModel_3Levels_OneLengthscale_FixedLevels1and2and3,
    HadamardMultitaskGPModel_3Levels_OneLengthscale_FixedLevels1and2_FreeLevels3,
    HadamardMultitaskGPModel_3Levels_TwoLengthscales_FixedLevels1and2_FreeLevel3,
    #HadamardMultitaskGPModel_3Levels_OneLengthscale_FixedLevels1and2_FreeLevel3PerGroup,
    ######### 4 levels HGPM #########
    HadamardMultitaskGPModel_4Levels_OneLengthscale_FixedLevels1and2and3and4,
    HadamardMultitaskGPModel_4Levels_OneLengthscale_FixedLevels1and2and3_FreeLevels4,
    HadamardMultitaskGPModel_4Levels_OneLengthscale_FixedLevels1and2_FreeLevels3and4,
    HadamardMultitaskGPModel_4Levels_OneLengthscale_FixedLevels1and2_FreeLevels3FixedLevels4,
    HadamardMultitaskGPModel_4Levels_TwoLengthscales_FixedLevels1and2and3_FreeLevel4,
    HadamardMultitaskGPModel_4Levels_TwoLengthscales_FixedLevels1and2_FreeLevels3and4
)

####################################################################
#    Define_fullHGPM
####################################################################

def Define_fullHGPM(
    data, 
    modelType,  
    mean = gpytorch.means.ZeroMean(), 
    lengthscale_prior=None, 
    lengthscale_MinConstraint=['min', 'mean', 'median', 'max', None],
    debug=False):
    
    r"""
    Args:
        data (data frame):
            Expected is a pandas data frame with variables "Level_1 to Level_L", indicating the meaning of the different levels of the hierarchy
            "rep" for replicates
            "condition" (optional) for condition (Control vs Treatment)
            "x" for temperatures
            "y" for intensities (raw or scaled)
            Note that the input data should be already filtered for the IDs to test and formatted approprietly

        all other arguments should be contained in the parameter dictionary.

    """
    
    if debug:
        logging.basicConfig(filename='debug.log', level=logging.DEBUG, format='%(message)s')
        logging.debug("##############################")
        logging.debug("Entering Define_fullHGPM")

    # define the number of layers in the hierarchical model and the presence of the appropriate columns
    NLevels, dfColnames_Levels = Check_modelTypeValidity(data, modelType)

    ID2test = data['Level_1'].unique()
    
    # initialize likelihood and model for each protein 
    # Model is the GP multitask model defined with inputs x_train (temperatures) and y_train (intensities), the likelihood is a multivariate Gaussian:
    
    full_Lik_list = []
    full_Model_list = []
    Index_df_list = []
    NModel =0

    for id in ID2test:
        
        if debug:
            logging.debug(f"Define full hgpm model for {id}")

        NModel_id = 0

        # filter data to id of interest
        df = data[(data['Level_1'] == id)].drop_duplicates()

        ########## Alternative model ##########
        # we first define the model with all observations of all conditions and the full hierarchical structure
        Levels_items = {}
        for l in range(2, NLevels+1):
            Levels_items["Level_{0}".format(l)] = df[dfColnames_Levels[l-1]].unique()

        # define alternative model:
        temp = torch.as_tensor(np.asarray(df['x'])).float()
        intens = torch.as_tensor(np.asarray(df['y'])).float()
        
        #defines the index for each group
        Index_df = df.filter(items = dfColnames_Levels).drop_duplicates()
        for l in range(2, NLevels+1):
            Index_df['Index_Level_'+str(l)] =  (Index_df['Level_'+str(l)].astype('category').cat.reorder_categories(Levels_items['Level_'+str(l)])).cat.codes
 
        #merge with the original data
        df_index = pd.merge(df, Index_df, on=dfColnames_Levels)
        
        index_level = {}
        index_level["Level_1"]= torch.full((temp.shape[0],1), dtype=torch.long, fill_value=0) # first level is always full correlation
        for l in range(2, NLevels+1):
            index_level["Level_{0}".format(l)] = torch.as_tensor(df_index['Index_Level_'+str(l)].values.reshape((len(df_index['Index_Level_'+str(l)]),1)))

        CheckLength = len(index_level[next(iter(index_level))])
        assert all(len(x) == CheckLength for x in index_level.values()), "Problem with the index definitions for the alternative model"

        ########## Full Layers: for the actual fit ##########
        lik = gpytorch.likelihoods.GaussianLikelihood()

        match modelType:
            #### 4 levels ####
            case "4Levels_OneLengthscale_FixedLevels1and2_FreeLevels3and4" :
                model = HadamardMultitaskGPModel_4Levels_OneLengthscale_FixedLevels1and2_FreeLevels3and4((temp, index_level["Level_1"],index_level["Level_2"], index_level["Level_3"], index_level["Level_4"] ), intens, lik, mean, lengthscale_prior,lengthscale_MinConstraint)
            case "4Levels_OneLengthscale_FixedLevels1and2_FreeLevels3FixedLevels4":
                model = HadamardMultitaskGPModel_4Levels_OneLengthscale_FixedLevels1and2_FreeLevels3FixedLevels4((temp, index_level["Level_1"],index_level["Level_2"], index_level["Level_3"], index_level["Level_4"] ), intens, lik, mean, lengthscale_prior,lengthscale_MinConstraint)
            case "4Levels_OneLengthscale_FixedLevels1and2and3_FreeLevels4":
                model = HadamardMultitaskGPModel_4Levels_OneLengthscale_FixedLevels1and2and3_FreeLevels4((temp, index_level["Level_1"],index_level["Level_2"], index_level["Level_3"], index_level["Level_4"] ), intens, lik, mean, lengthscale_prior,lengthscale_MinConstraint)
            case "4Levels_OneLengthscale_FixedLevels1and2and3and4":
                model = HadamardMultitaskGPModel_4Levels_OneLengthscale_FixedLevels1and2and3and4((temp, index_level["Level_1"],index_level["Level_2"], index_level["Level_3"], index_level["Level_4"] ), intens, lik, mean, lengthscale_prior,lengthscale_MinConstraint)
            case "4Levels_TwoLengthscales_FixedLevels1and2and3_FreeLevel4":
                model = HadamardMultitaskGPModel_4Levels_TwoLengthscales_FixedLevels1and2and3_FreeLevel4((temp, index_level["Level_1"],index_level["Level_2"], index_level["Level_3"], index_level["Level_4"] ), intens, lik, mean, lengthscale_prior,lengthscale_MinConstraint)
            case "4Levels_TwoLengthscales_FixedLevels1and2_FreeLevels3and4":
                model = HadamardMultitaskGPModel_4Levels_TwoLengthscales_FixedLevels1and2_FreeLevels3and4((temp, index_level["Level_1"],index_level["Level_2"], index_level["Level_3"], index_level["Level_4"] ), intens, lik, mean, lengthscale_prior,lengthscale_MinConstraint)
            #### 3 levels ####
            case "3Levels_OneLengthscale_FixedLevels1and2_FreeLevels3" :
                model = HadamardMultitaskGPModel_3Levels_OneLengthscale_FixedLevels1and2_FreeLevels3((temp, index_level["Level_1"],index_level["Level_2"], index_level["Level_3"]), intens, lik, mean, lengthscale_prior,lengthscale_MinConstraint)
            case "3Levels_OneLengthscale_FixedLevels1and2and3" :
                model = HadamardMultitaskGPModel_3Levels_OneLengthscale_FixedLevels1and2and3((temp, index_level["Level_1"],index_level["Level_2"], index_level["Level_3"]), intens, lik, mean, lengthscale_prior,lengthscale_MinConstraint)
            case "3Levels_TwoLengthscales_FixedLevels1and2_FreeLevel3":
                model = HadamardMultitaskGPModel_3Levels_TwoLengthscales_FixedLevels1and2_FreeLevel3((temp, index_level["Level_1"],index_level["Level_2"], index_level["Level_3"]), intens, lik, mean, lengthscale_prior,lengthscale_MinConstraint)
            #case "3Levels_OneLengthscale_FixedLevels1and2_FreeLevel3PerGroup":
            #    model = HadamardMultitaskGPModel_3Levels_OneLengthscale_FixedLevels1and2_FreeLevel3PerGroup((temp, index_level["Level_1"],index_level["Level_2"], index_level["Level_3"]), intens, lik, mean, lengthscale_prior,lengthscale_MinConstraint)


        full_Lik_list.append(lik)
        full_Model_list.append(model)
        Index_df_list.append(Index_df)
        NModel = NModel+1
        NModel_id = NModel_id+1

    #import pdb; pdb.set_trace() 
    # dimensions
    n_models = len(full_Model_list)
    n_pep = len(ID2test)
    
    if debug:
        logging.debug("Number of constructed models: "+ str(n_models))
        logging.debug("Number of comparisons: " +str(n_pep))

    # assert the model list is not empty
    assert n_models!=0, "Error, the model list is empty!"
    
    ######################
    ### Model training ###
    ######################
    
    # Combine different models in an IndependentModelList for simultaneous fitting
    full_model = gpytorch.models.IndependentModelList(*full_Model_list)
    full_likelihood = gpytorch.likelihoods.LikelihoodList(*full_Lik_list)
 
    return full_model, full_likelihood, pd.concat(Index_df_list)
