#! /usr/bin/env python3

import sys
import pickle
import logging
import torch
import gpytorch
import os
import copy

from fitting_functions.fit_model import fit_fullHGPM
from prediction_functions.predict_HGPM import predict_full_HGPM, predict_joint_HGPM, evaluate_joint_HGPM
from plotting_functions.plot_models_comparison import plot_fullvsjoint_comparison
from utils.compute_LR import ComputeRatios
from utils.clean_directories import clean_directory
from utils.combine_results_dict import combine_dictionaries
from plotting_functions.combine_all_plots import combine_all_plots

########################################
# in this function, we
# 1) read the pkl file associated to the ID of interest
# 
# ########################################s

def fit_evaluate_predict_and_sample_HGPM(
    data, 
    parameters,
    sampling,
    numberSamples_df,
    debug=True):
    
    if debug:
        logging.basicConfig(filename='debug.log', level=logging.DEBUG, format='%(message)s')
        logging.debug("##############################")
        logging.debug("Entering fit_evaluate_predict_and_sample_HGPM ")

    ###################################################
    # fit and evaluate the full HGPM 
    ###################################################

    results_fit_full_HGPM = fit_fullHGPM(
        data, 
        parameters, 
        debug=debug)
    
    if debug:
        logging.debug('Fitting and evaluation of the full model has been done. \n')
        logging.debug('MLL values look like: \n')
        logging.debug(results_fit_full_HGPM['full_hgpm_mll_values'])
        logging.debug('Index_df look like: \n')
        logging.debug(results_fit_full_HGPM['full_hgpm_index_df'])

    results_for_plotting = {'full_hgpm_fit_loss_values_df' : results_fit_full_HGPM['full_hgpm_fit_loss_values_df']}

    ###################################################
    # x-points for predictions
    ###################################################
    min_x = data['x'].min()
    max_x = data['x'].max()
    with torch.no_grad(), gpytorch.settings.fast_pred_var():
        # specify test point locations
        test_x_disordered = torch.cat((torch.linspace(min_x, max_x, parameters['n_PredictionsPoints']).float(), torch.tensor(data['x'].unique()).float()))
        test_x, _ = test_x_disordered.sort()  

    ###################################################
    # predict the full HGPM 
    ###################################################
    results_predict_full_HGPM = predict_full_HGPM(
        data, 
        parameters, 
        results_fit_full_HGPM['full_hgpm_state_dict_list'],
        test_x,
        debug=debug)

    if debug:
        logging.debug("Prediction of the full hgpm has been done. \n")

    ## split the results for the plots and the other results
    keys_for_plotting = {'full_hgpm_covariance_matrix_pdf_filename', 'full_hgpm_covariance_matrix_introduction_text', 'full_hgpm_covariance_matrix_pdf_content'}  

    if parameters["PlotSave"]:
        for key in keys_for_plotting:
            results_for_plotting[key] = results_predict_full_HGPM[key] 

    # Remove the keys for plotting from the original dictionary
    # even if parameters["PlotSave"] = False, we want to remove these entries from results_predict_joint_HGPM
    for key in keys_for_plotting:
        results_predict_full_HGPM.pop(key, None) 
    
    if debug:
        logging.debug(f"After splitting the results_predict_full_HGPM, the following keys are in results_for_plotting:{results_for_plotting.keys()}")
        logging.debug(f"After splitting the results_predict_full_HGPM, the following keys are in results_predict_full_HGPM:{results_predict_full_HGPM.keys()}")
    
    ################################################################
    # predict the joint HGP models and compute the associated mlls
    # sample if required
    ################################################################
    results_predict_joint_HGPM = predict_joint_HGPM(
        data, 
        parameters, 
        results_fit_full_HGPM['full_hgpm_state_dict_list'],
        test_x, 
        sampling = sampling,
        nb_samples_df = numberSamples_df,
        debug=debug)

    if debug:
        logging.debug("Prediction and evaluation of the joint hgpm has been done. \n")

        
    ## split the results for the plots and the other results
    keys_for_plotting = {'joint_hgpm_covariance_matrix_pdf_filename', 'joint_hgpm_covariance_matrix_introduction_text', 'joint_hgpm_covariance_matrix_pdf_content'}  

    if parameters["PlotSave"]:
        for key in keys_for_plotting:
            results_for_plotting[key] = results_predict_joint_HGPM[key] 
        
    # Remove the keys for plotting from the original dictionary
    # even if parameters["PlotSave"] = False, we want to remove these entries from results_predict_joint_HGPM
    for key in keys_for_plotting:
        results_predict_joint_HGPM.pop(key, None) 
    
    if debug:
        logging.debug(f"After splitting the results_predict_joint_HGPM, the following keys are in results_for_plotting:{results_for_plotting.keys()}")
        logging.debug(f"After splitting the results_predict_joint_HGPM, the following keys are in results_predict_joint_HGPM:{results_predict_joint_HGPM.keys()}")
    
  
    ###################################################
    # plot the obtained fit from the full & joint HGPM 
    ###################################################
    if parameters["PlotSave"]:
        results_plot = plot_fullvsjoint_comparison(
            data, 
            parameters,
            results_predict_full_HGPM['prediction_full_hgpm_dict'],
            results_predict_joint_HGPM['prediction_joint_hgpm_dict'],
            ItemToPlot=None, 
            MaxPlotPerRow=8,
            SavingPath='', 
            debug=debug)

        if debug:
            logging.debug("Plotting of the full and joint hgpm predictions has been done. \n")

    
    ###################################################
    # compute the ratios of MLL
    ###################################################
    likelihood_ratios_df = ComputeRatios(results_fit_full_HGPM['full_hgpm_mll_values'], results_predict_joint_HGPM['joint_hgpm_mll_values'])
    
    if debug:
        logging.debug("Computing the values of the statistic...")
        logging.debug("MLL values for full model")
        logging.debug(results_fit_full_HGPM['full_hgpm_mll_values'])
        logging.debug("MLL data frame for joint model")
        logging.debug(results_predict_joint_HGPM['joint_hgpm_mll_values'])

    ###################################################
    # Combine and save results
    ###################################################
    # only save the individual predictions if explicitly required
    if parameters['save_individual_results']:
        likelihood_ratios_df.to_csv(index = True, path_or_buf = os.path.join( "likelihood_ratios.csv" ))

    # Note that if sampling=False, then sampled_dataset=None
    combined_results_dict = combine_dictionaries(results_fit_full_HGPM,results_predict_full_HGPM, results_predict_joint_HGPM)
    combined_results_dict['likelihood_ratios_df'] = likelihood_ratios_df

    ###################################################
    # Combine all plots for this id and save the pdf
    ###################################################
    if parameters["PlotSave"]:
        if debug:
            logging.debug("Combine all plots for this id.")
        
        combined_results_dict_for_plot = combine_dictionaries(results_for_plotting, results_plot, parameters)
        if debug:
            logging.debug(f"The following keys are in combined_results_dict_for_plot:{combined_results_dict_for_plot.keys()}")
        
        pdf_name = str(data['Level_1'].unique().item()) + "_plots.pdf"
        combine_all_plots(combined_results_dict_for_plot, pdf_name)
    
    return combined_results_dict


def fit_evaluate_HGPM(
    data, 
    parameters,
    debug=True):
    
    if debug:
        logging.basicConfig(filename='debug.log', level=logging.DEBUG, format='%(message)s')
        logging.debug("##############################")
        logging.debug("Entering simplified_fit_evaluate_HGPM")
    
    ###################################################
    # fit and evaluate the full HGPM 
    ###################################################

    results_fit_full_HGPM = fit_fullHGPM(
        data, 
        parameters, 
        debug=debug)
    
    if debug:
        logging.debug("Entering fit_evaluate_predict_and_sample_HGPM\n")
        logging.debug('Fitting and evaluation of the full model has been done. \n')
        logging.debug('MLL values look like: \n')
        logging.debug(results_fit_full_HGPM['full_hgpm_mll_values'])
        logging.debug('Index_df look like: \n')
        logging.debug(results_fit_full_HGPM['full_hgpm_index_df'])

    ################################################################
    # evaluate the joint HGP models 
    ################################################################
    joint_hgpm_mll_values = evaluate_joint_HGPM(
        data, 
        parameters, 
        results_fit_full_HGPM['full_hgpm_state_dict_list'],
        debug=debug)
    
    if debug:
        logging.debug("Evaluation of the joint hgpm has been done. \n")

    ###################################################
    # compute the ratios of MLL
    ###################################################
    likelihood_ratios_df = ComputeRatios(results_fit_full_HGPM['full_hgpm_mll_values'], joint_hgpm_mll_values)
    
    if debug:
        logging.debug("Computing the values of the statistic...")
        logging.debug("MLL values for full model")
        logging.debug(results_fit_full_HGPM['full_hgpm_mll_values'])
        logging.debug("MLL data frame for joint model")
        logging.debug(joint_hgpm_mll_values)

    ###################################################
    # Combine results
    ###################################################
    results_fit_full_HGPM['likelihood_ratios_df'] = likelihood_ratios_df
    
    return results_fit_full_HGPM


def fit_evaluate_HGPM_on_one_sample(
    data, 
    parameters,
    debug=True):

    FreeSpace = parameters['Minimise_required_memory_space']
    if not type(FreeSpace) == type(True):
        raise ValueError(f'variable Minimise_required_memory_space should be a boolean')

    if debug:
        logging.basicConfig(filename='debug.log', level=logging.DEBUG)
        logging.debug("##############################")
        logging.debug(("Entering fitting + evaluation function for one sample at a time"))
        logging.debug(("Minimise_required_memory_space is " + str(FreeSpace)))

    # which results should be saved
    if FreeSpace : 
        required_outputs = ["hgpm_fit_parameters","likelihood_ratios"]
    else:
        required_outputs = ["hgpm_fit_parameters", "likelihood_ratios", "hgpm_fit_loss_values"]
    
    tmp_parameters = copy.deepcopy(parameters)
    tmp_parameters['save_individual_results'] = False

    results_fit_evaluate_HGPM = fit_evaluate_HGPM(
        data, 
        tmp_parameters,
        debug=debug)

    #only keep the required outputs
    results = {}
    if "hgpm_fit_parameters" in required_outputs:
        results['full_hgpm_fit_parameters_sampled_dataset_df'] = results_fit_evaluate_HGPM['full_hgpm_fit_parameters_df']
    if "likelihood_ratios" in required_outputs:
        results['likelihood_ratios_sampled_dataset_df'] = results_fit_evaluate_HGPM['likelihood_ratios_df']
    if "hgpm_fit_loss_values" in required_outputs:
        results['full_hgpm_fit_loss_values_sampled_dataset_df'] = results_fit_evaluate_HGPM['full_hgpm_fit_loss_values_df']

    return results
