#! /usr/bin/env python3
import os
import pandas as pd
import torch
import logging 

from gpytorch.mlls import SumMarginalLogLikelihood
from utils.save_parameters import SaveParameters
from plotting_functions.plot_fitting_convergence import PlotFitConvergence
from fitting_functions.define_fullHGPM import Define_fullHGPM
from utils.find_suitable_separator import combine_unique_values_to_string

####################################################################
#    fit_fullHGPM
####################################################################

def fit_fullHGPM(
    data, 
    parameters,
    debug=False):

    r"""
    Args:
        data (data frame):
            Expected is a pandas data frame with variables "Level_1 to Level_L", indicating the meaning of the different levels of the hierarchy
            "rep" for replicates
            "condition" (optional) for condition (Control vs Treatment)
            "x" for temperatures
            "y" for intensities (raw or scaled)
            Note that the input data should be already filtered for the IDs to test and formatted approprietly
        
        PlotSave (bool):
            should the plot resulting of this function be saved?

        all other arguments should be contained in the parameter dictionary. Note that parameters['directory_for_results'] is specified when fitting the null distribution.

    Values:
        save 
            lossValues_full_model.csv
            convergence fit (pdf)
            hgpm_fit_parameters.pth
            parameters.csv

        return
            full_model
            full_likelihood
            Index_df 
            LossValues
            MLL_values

    """

    if debug:
        logging.basicConfig(filename='debug.log', level=logging.DEBUG, format='%(message)s')
        logging.debug("##############################")
        logging.debug("Entering fit_fullHGPM")

  
    #import pdb; pdb.set_trace() 
    if not 'save_individual_results' in parameters.keys():
        parameters['save_individual_results'] = False

    ##############################################################################################
    ### Define the full HGPM, i.e the one in which each condition is modeled independently  ###
    ##############################################################################################
    full_model, full_likelihood, Index_df = Define_fullHGPM(data, parameters['modelType'],  parameters['mean'], parameters['lengthscale_prior'], parameters['lengthscale_MinConstraint'], debug)

    ######################
    ### Model training ###
    ######################

    # Find model hyperparameters
    full_model.train()
    full_likelihood.train()
    
    # Optimize the models using the Adam optimizer
    optimizer = torch.optim.Adam([
        {'params': full_model.parameters()},  # Includes all submodel and all likelihood parameters
    ], lr=parameters['LearningRate'], amsgrad=parameters['Amsgrad'])  # learning rate
    
    mll = SumMarginalLogLikelihood(full_likelihood, full_model)

    if debug:
        logging.debug("Fitting of the full hgpm...")
    # Train
    LossValues = []
    for i in range(parameters['training_iterations']):
        optimizer.zero_grad()
        output = full_model(*full_model.train_inputs)
        loss = -mll(output, full_model.train_targets)
        loss.backward()

        if debug:
            logging.debug('Iter %d/%d - Loss: %.3f' % (i + 1, parameters['training_iterations'], loss.item()))

        LossValues.append(loss.item())
        optimizer.step()

    #######################################
    ### Loss values to df                ###
    #######################################
    df_LossValues = pd.DataFrame({
        'LossValue' : LossValues,
        'fitted_levels_1' : combine_unique_values_to_string(data["Level_1"].unique(), potential_separators = ["_", ".", '|', '~', '^', ';', ':'] )}) 

    #######################################
    ### Compute the MLL for each model ###
    #######################################
    if debug:
        logging.debug("Compute the MLL for each fit")
    # this should be - mll, as we use the loss and not the mll in our computation of the likelihood ratio.
    # our loss values for the full model should be the same as the loss values in df_LossValues, i.e. they typically tend towards negative values.
    MLL_values = [ - sub_mll(output, target).item() for sub_mll, output, target in zip(mll.mlls, output, full_model.train_targets)] # return the final MLL for each fitted model independently
    
    #######################################
    ### save the parameters of the fit  ###
    #######################################

    if debug:
        logging.debug("Get the parameters")

    List_state_dict=[]
    hgpm_fit_parameters = []

    for submodels, it in zip(full_model.models, Index_df['Level_1'].unique()):
        List_state_dict.append(submodels.state_dict())
        hgpm_fit_parameters.append( SaveParameters(Index_df.loc[Index_df['Level_1']==it], submodels, it, debug))
             
    hgpm_fit_parameters = pd.concat(hgpm_fit_parameters)

    ######################################################
    ### only save the files if explicitely required.  ###
    ######################################################
    if parameters['save_individual_results']:
        if debug:
            logging.debug("Save results as files.")

        # save the models_parameters
        torch.save(List_state_dict, f= os.path.join('hgpm_fit_parameters.pth'))
        hgpm_fit_parameters.to_csv(index=True, path_or_buf= os.path.join("hgpm_fit_parameters.csv"))

        #save the index_df
        Index_df.to_csv(index=True, path_or_buf= os.path.join("index_full_HGPM.csv"))
        
        ## save the LossValues.csv (first add a column to identify the Loss)
        df_LossValues.to_csv(index=True, path_or_buf= os.path.join( "hgpm_fit_loss_values.csv"))

        ## Plot the convergence of the fit 
        if parameters['PlotSave']: 
            _ , fig, fig_name = PlotFitConvergence(LossValues, parameters['training_iterations'], parameters['LearningRate'], parameters['Amsgrad'])
            fig.savefig(os.path.join(fig_name))

    results_dict = {
        "full_hgpm_model_list" : full_model,
        "full_hgpm_likelihood_list" : full_likelihood,
        "full_hgpm_state_dict_list" : List_state_dict,
        "full_hgpm_ID_list" : Index_df['Level_1'].unique(),
        "full_hgpm_index_df" : Index_df,
        "full_hgpm_fit_loss_values_df" : df_LossValues,
        "full_hgpm_mll_values" : MLL_values,
        "full_hgpm_fit_parameters_df" : hgpm_fit_parameters, 
    }

    if debug:
        logging.debug("##############################")
    
    return results_dict