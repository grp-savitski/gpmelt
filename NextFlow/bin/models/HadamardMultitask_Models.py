#! /usr/bin/env python3

import math
import torch
from matplotlib import pyplot as plt
import pandas as pd
import numpy as np
import copy
import gpytorch
from gpytorch.constraints import Interval, Positive
from gpytorch.lazy import DiagLazyTensor, InterpolatedLazyTensor, PsdSumLazyTensor, RootLazyTensor
#from gpytorch.utils.broadcasting import _mul_broadcast_shape
from gpytorch.priors import Prior
from typing import Optional, Sequence
from gpytorch.mlls import SumMarginalLogLikelihood
from pathlib import Path
import random
from matplotlib.colors import LogNorm
from matplotlib.lines import Line2D
import os
from collections import Counter

from os.path import exists
import sys

from linear_operator.operators import (
    DiagLinearOperator,
    InterpolatedLinearOperator,
    PsdSumLinearOperator,
    RootLinearOperator,
)


####################################################################
#    ExactGPModel
####################################################################

class ExactGPModel(gpytorch.models.ExactGP):
    def __init__(self, train_x, train_y, likelihood, mean = gpytorch.means.ZeroMean()):
        super(ExactGPModel, self).__init__(train_x, train_y, likelihood)
        self.mean_module = mean
        self.covar_module = gpytorch.kernels.ScaleKernel(gpytorch.kernels.RBFKernel())

    def forward(self, x):
        mean_x = self.mean_module(x)
        covar_x = self.covar_module(x)
        return gpytorch.distributions.MultivariateNormal(mean_x, covar_x)


####################################################################
#    GPRegressionModel_SKI_Ratio
####################################################################

class GPRegressionModel_SKI_Ratio(gpytorch.models.ExactGP):
    def __init__(self, train_x, train_y, likelihood, Ratio, mean = gpytorch.means.ZeroMean()):
        super(GPRegressionModel_SKI_Ratio, self).__init__(train_x, train_y, likelihood)

        # SKI requires a grid size hyperparameter. This util can help with that. 
        grid_size = gpytorch.utils.grid.choose_grid_size(train_x,Ratio)

        self.mean_module = mean
        self.covar_module = gpytorch.kernels.ScaleKernel(
            gpytorch.kernels.GridInterpolationKernel(
                gpytorch.kernels.RBFKernel(), grid_size=grid_size, num_dims=1
            )
        ) 

    def forward(self, x):
        mean_x = self.mean_module(x)
        covar_x = self.covar_module(x)
        return gpytorch.distributions.MultivariateNormal(mean_x, covar_x)

        

####################################################################
#    DiagonalIndexKernel_Free
####################################################################

class DiagonalIndexKernel_Free(gpytorch.kernels.Kernel):
    r"""
    A kernel for discrete indices. Kernel is defined by a lookup table.

    .. math::

        \begin{equation}
            k(i, j) = \text{diag}(\mathbf v)_{i, j}
        \end{equation}

    where :math:`\mathbf v` is a  non-negative vector.
    v is either constant for all ("constant diagonal") or different for each entry("free diagonal").
    These parameters are learned.

    Args:
        num_tasks (int):
            Total number of indices.
        batch_shape (torch.Size, optional):
            Set if the MultitaskKernel is operating on batches of data (and you want different
            parameters for each batch)
        rank (int):
            Rank of :math:`B` matrix. Controls the degree of
            correlation between the outputs. With a rank of 1 the
            outputs are identical except for a scaling factor.
        prior (:obj:`gpytorch.priors.Prior`):
            Prior for :math:`B` matrix.
        var_constraint (Constraint, optional):
            Constraint for added diagonal component. Default: `Positive`.

    Attributes:
        raw_var:
            The element-wise log of the :math:`\mathbf v` vector.
    """

    def __init__(
        self,
        num_tasks: int,
        rank: Optional[int] = 1,
        prior: Optional[Prior] = None,
        var_constraint: Optional[Interval] = None,
        **kwargs,
    ):

        if rank > num_tasks:
            raise RuntimeError("Cannot create a task covariance matrix larger than the number of tasks")
        super().__init__(**kwargs)

        if var_constraint is None:
            var_constraint = Positive()

        self.register_parameter(name="raw_var", parameter=torch.nn.Parameter(torch.randn(*self.batch_shape, num_tasks)))
        
        if prior is not None:
            if not isinstance(prior, Prior):
                raise TypeError("Expected gpytorch.priors.Prior but got " + type(prior).__name__)
            self.register_prior("IndexKernelPrior", prior, lambda m: m._eval_covar_matrix())

        self.register_constraint("raw_var", var_constraint)
        self.Num_tasks = num_tasks

    @property
    def var(self):
        return self.raw_var_constraint.transform(self.raw_var)

    @var.setter
    def var(self, value):
        self._set_var(value)

    def _set_var(self, value):
        self.initialize(raw_var=self.raw_var_constraint.inverse_transform(value))

    def _eval_covar_matrix(self):
        return torch.diag_embed(self.var)

    @property
    def covar_matrix(self):
        var = self.var
        res = DiagLazyTensor(var)
        return res

    def forward(self, i1, i2, **params):

        i1, i2 = i1.long(), i2.long()
        covar_matrix = self._eval_covar_matrix()
        batch_shape = torch.broadcast_shapes(i1.shape[:-2], i2.shape[:-2], self.batch_shape)

        res = InterpolatedLinearOperator(
            base_linear_op=covar_matrix,
            left_interp_indices=i1.expand(batch_shape + i1.shape[-2:]),
            right_interp_indices=i2.expand(batch_shape + i2.shape[-2:]),
        )
        return res

####################################################################
#    DiagonalIndexKernel_Fixed
####################################################################

class DiagonalIndexKernel_Fixed(gpytorch.kernels.Kernel):
    r"""
    A kernel for discrete indices. Kernel is defined by a lookup table.

    .. math::

        \begin{equation}
            k(i, j) = \text{diag}(\mathbf v)_{i, j}
        \end{equation}

    where :math:`\mathbf v` is a  non-negative vector.
    v is either constant for all ("constant diagonal") or different for each entry("free diagonal").
    These parameters are learned.

    Args:
        num_tasks (int):
            Total number of indices.
        batch_shape (torch.Size, optional):
            Set if the MultitaskKernel is operating on batches of data (and you want different
            parameters for each batch)
        rank (int):
            Rank of :math:`B` matrix. Controls the degree of
            correlation between the outputs. With a rank of 1 the
            outputs are identical except for a scaling factor.
        prior (:obj:`gpytorch.priors.Prior`):
            Prior for :math:`B` matrix.
        var_constraint (Constraint, optional):
            Constraint for added diagonal component. Default: `Positive`.

    Attributes:
        raw_var:
            The element-wise log of the :math:`\mathbf v` vector.
    """

    def __init__(
        self,
        num_tasks: int,
        rank: Optional[int] = 1,
        prior: Optional[Prior] = None,
        var_constraint: Optional[Interval] = None,
        **kwargs,
    ):

        if rank > num_tasks:
            raise RuntimeError("Cannot create a task covariance matrix larger than the number of tasks")
        super().__init__(**kwargs)

        if var_constraint is None:
            var_constraint = Positive()


        self.register_parameter(name="raw_var_cnst", parameter=torch.nn.Parameter(torch.randn(*self.batch_shape,1)))# random numbers
        
        if prior is not None:
            if not isinstance(prior, Prior):
                raise TypeError("Expected gpytorch.priors.Prior but got " + type(prior).__name__)
            self.register_prior("IndexKernelPrior", prior, lambda m: m._eval_covar_matrix())
        
        self.Num_tasks = num_tasks
        self.Rank = rank

        self.register_constraint("raw_var_cnst", var_constraint)

    @property
    def var_cnst(self):
        return self.raw_var_cnst_constraint.transform(self.raw_var_cnst)

    @var_cnst.setter
    def var_cnst(self, value):
        self._set_var_cnst(value)

    def _set_var_cnst(self, value):
        self.initialize(raw_var_cnst=self.raw_var_cnst_constraint.inverse_transform(value))
          
    @property
    def var(self):
        return self.var_cnst * torch.ones(*self.batch_shape, self.Num_tasks)

    def _eval_covar_matrix(self):
        return torch.diag_embed(self.var)
    
    @property
    def covar_matrix(self):
        var = self.var
        res = DiagLazyTensor(var)
        return res

    def forward(self, i1, i2, **params):

        i1, i2 = i1.long(), i2.long()
        covar_matrix = self._eval_covar_matrix()
        batch_shape = torch.broadcast_shapes(i1.shape[:-2], i2.shape[:-2], self.batch_shape)

        res = InterpolatedLinearOperator(
            base_linear_op=covar_matrix,
            left_interp_indices=i1.expand(batch_shape + i1.shape[-2:]),
            right_interp_indices=i2.expand(batch_shape + i2.shape[-2:]),
        )
        return res



####################################################################
#    HadamardMultitaskGPModel_2Levels_OneLengthscale_FixedLevels1and2
####################################################################
# in this new set of notations, the lower level is the higher level, level 1 being to one on top.
# this will allow to easily add Levels to the model

class HadamardMultitaskGPModel_2Levels_OneLengthscale_FixedLevels1and2(gpytorch.models.ExactGP):
    def __init__(self, train_x, train_y, likelihood, mean = gpytorch.means.ZeroMean(), lengthscale_prior=None, lengthscale_MinConstraint = ['min', 'mean', 'median', 'max', None]):
        super(HadamardMultitaskGPModel_2Levels_OneLengthscale_FixedLevels1and2, self).__init__(train_x, train_y, likelihood)
       
        assert len(train_x)>=3, "missing some training inputs"
        #import pdb; pdb.set_trace()
        self.mean_module = mean

        '''
        self.numTasks_level1 = int(max(train_x[1])+1) # the top level
        self.numTasks_level2 = int(max(train_x[2])+1) # the bottom level
        '''
        self.numTasks_level1 = len(set(train_x[1].unique()))# the top level
        self.numTasks_level2 = len(set(train_x[2].unique())) # the bottom level
        
        # We learn an IndexKernel for 2 tasks
        # (so we'll actually learn 2x2=4 tasks with correlations)
        self.task_covar_module_level1 = DiagonalIndexKernel_Fixed(num_tasks = self.numTasks_level1, rank=1)
        self.task_covar_module_level2 = DiagonalIndexKernel_Fixed(num_tasks = self.numTasks_level2, rank=1)
      
        if not lengthscale_MinConstraint is None:

            train_x_values = train_x[0].unique()
            DistVec = train_x_values[1:len(train_x_values)]-train_x_values[0:len(train_x_values)-1] # compute the distance between each pair of consecutive temperatures
            
            match lengthscale_MinConstraint:
                case 'min':
                    Constt = min(DistVec) # we use the minium distance between temperatures as a lower limit for the lengthscale
                case 'mean':
                    Constt = torch.mean(DistVec) # we use the mean distance between temperatures as a lower limit for the lengthscale
                case 'median':
                    Constt = torch.median(DistVec) # we use the medin distance between temperatures as a lower limit for the lengthscale
                case 'max':
                    Constt = max(DistVec) # we use the max distance between temperature as a lower limit for the lengthscale
            
            lengthscale_constraint = gpytorch.constraints.GreaterThan( Constt )

        else: 

            lengthscale_constraint = None # there is no lower limit for the lengthscale
        
        
        self.covar_module = gpytorch.kernels.RBFKernel(lengthscale_prior = lengthscale_prior, lengthscale_constraint = lengthscale_constraint )

        
    def indexKernel_matrix(self,i_level1, i_level2):
        #import pdb; pdb.set_trace()

        # Get task-task covariance
        covar_i_level1 = self.task_covar_module_level1(i_level1)
        covar_i_level2 = self.task_covar_module_level2(i_level2)
        
        return covar_i_level1 + covar_i_level2  

    def covar_matrix(self,x,i_level1, i_level2):

        # Get input-input covariance
        covar_x = self.covar_module(x)
        # Get task-task covariance
        Index_kernel = self.indexKernel_matrix(i_level1, i_level2)
        # Multiply the two together to get the covariance we want
        covar = covar_x.mul(Index_kernel)

        return covar


    def forward(self,x,i_level1, i_level2):
        #import pdb; pdb.set_trace()
        mean_x = self.mean_module(x)

        # Get input-input covariance
        covar_x = self.covar_module(x)
        # Get task-task covariance
        Index_kernel = self.indexKernel_matrix(i_level1, i_level2)
        # Multiply the two together to get the covariance we want
        covar = covar_x.mul(Index_kernel)

        return gpytorch.distributions.MultivariateNormal(mean_x, covar)

####################################################################
#    HadamardMultitaskGPModel_2Levels_OneLengthscale_FixedLevel1_FreeLevel2
####################################################################
# This class shouldn't be used to compare conditions, but only to fit the real data

class HadamardMultitaskGPModel_2Levels_OneLengthscale_FixedLevel1_FreeLevel2(gpytorch.models.ExactGP):
    def __init__(self, train_x, train_y, likelihood, mean = gpytorch.means.ZeroMean(), lengthscale_prior=None, lengthscale_MinConstraint = ['min', 'mean', 'median', 'max', None]):
        super(HadamardMultitaskGPModel_2Levels_OneLengthscale_FixedLevel1_FreeLevel2, self).__init__(train_x, train_y, likelihood)
       
        assert len(train_x)>=3, "missing some training inputs"
        #import pdb; pdb.set_trace()
        self.mean_module = mean

        '''
        self.numTasks_level1 = int(max(train_x[1])+1) # the top level
        self.numTasks_level2 = int(max(train_x[2])+1) # the bottom level
        '''
        self.numTasks_level1 = len(set(train_x[1].unique()))# the top level
        self.numTasks_level2 = len(set(train_x[2].unique())) # the bottom level
        
        # We learn an IndexKernel for 2 tasks
        # (so we'll actually learn 2x2=4 tasks with correlations)
        self.task_covar_module_level1 = DiagonalIndexKernel_Fixed(num_tasks = self.numTasks_level1, rank=1)
        self.task_covar_module_level2 = DiagonalIndexKernel_Free(num_tasks = self.numTasks_level2, rank=1)
      
        if not lengthscale_MinConstraint is None:

            train_x_values = train_x[0].unique()
            DistVec = train_x_values[1:len(train_x_values)]-train_x_values[0:len(train_x_values)-1] # compute the distance between each pair of consecutive temperatures
            
            match lengthscale_MinConstraint:
                case 'min':
                    Constt = min(DistVec) # we use the minium distance between temperatures as a lower limit for the lengthscale
                case 'mean':
                    Constt = torch.mean(DistVec) # we use the mean distance between temperatures as a lower limit for the lengthscale
                case 'median':
                    Constt = torch.median(DistVec) # we use the medin distance between temperatures as a lower limit for the lengthscale
                case 'max':
                    Constt = max(DistVec) # we use the max distance between temperature as a lower limit for the lengthscale
            
            lengthscale_constraint = gpytorch.constraints.GreaterThan( Constt )

        else: 

            lengthscale_constraint = None # there is no lower limit for the lengthscale
        
        
        self.covar_module = gpytorch.kernels.RBFKernel(lengthscale_prior = lengthscale_prior, lengthscale_constraint = lengthscale_constraint )

        
    def indexKernel_matrix(self,i_level1, i_level2):
        #import pdb; pdb.set_trace()

        # Get task-task covariance
        covar_i_level1 = self.task_covar_module_level1(i_level1)
        covar_i_level2 = self.task_covar_module_level2(i_level2)
        
        return covar_i_level1 + covar_i_level2  

    def covar_matrix(self,x,i_level1, i_level2):

        # Get input-input covariance
        covar_x = self.covar_module(x)
        # Get task-task covariance
        Index_kernel = self.indexKernel_matrix(i_level1, i_level2)
        # Multiply the two together to get the covariance we want
        covar = covar_x.mul(Index_kernel)

        return covar


    def forward(self,x,i_level1, i_level2):
        #import pdb; pdb.set_trace()
        mean_x = self.mean_module(x)

        # Get input-input covariance
        covar_x = self.covar_module(x)
        # Get task-task covariance
        Index_kernel = self.indexKernel_matrix(i_level1, i_level2)
        # Multiply the two together to get the covariance we want
        covar = covar_x.mul(Index_kernel)

        return gpytorch.distributions.MultivariateNormal(mean_x, covar)


####################################################################
#    HadamardMultitaskGPModel_3Levels_OneLengthscale_FixedLevels1and2_FreeLevel3
####################################################################
# this class is a child of HadamardMultitaskGPModel_2Levels_OneLengthscale_FixedLevels1and2, to which we add one level in the hierarchy
class HadamardMultitaskGPModel_3Levels_OneLengthscale_FixedLevels1and2_FreeLevels3(HadamardMultitaskGPModel_2Levels_OneLengthscale_FixedLevels1and2):
    def __init__(self, train_x, train_y, likelihood, mean = gpytorch.means.ZeroMean(), lengthscale_prior=None, lengthscale_MinConstraint = ['min', 'mean', 'median', 'max', None]):
        
        #super(HadamardMultitaskGPModel_3Levels_OneLengthscale_FixedLevels1and2_FreeLevel3, self).__init__(train_x, train_y, likelihood)
        HadamardMultitaskGPModel_2Levels_OneLengthscale_FixedLevels1and2.__init__(self, train_x, train_y, likelihood, lengthscale_prior=lengthscale_prior, lengthscale_MinConstraint=lengthscale_MinConstraint)

        assert len(train_x)>=4, "missing some training inputs"
        #import pdb; pdb.set_trace()
        self.mean_module = mean

        self.numTasks_level3  = len(set(train_x[3].unique()))
        
        # We learn an IndexKernel for 2 tasks
        # (so we'll actually learn 2x2=4 tasks with correlations)
        self.task_covar_module_level3 = DiagonalIndexKernel_Free(num_tasks = self.numTasks_level3, rank=1)
      
        if not lengthscale_MinConstraint is None:

            train_x_values = train_x[0].unique()
            DistVec = train_x_values[1:len(train_x_values)]-train_x_values[0:len(train_x_values)-1] # compute the distance between each pair of consecutive temperatures
            
            match lengthscale_MinConstraint:
                case 'min':
                    Constt = min(DistVec) # we use the minium distance between temperatures as a lower limit for the lengthscale
                case 'mean':
                    Constt = torch.mean(DistVec) # we use the mean distance between temperatures as a lower limit for the lengthscale
                case 'median':
                    Constt = torch.median(DistVec) # we use the medin distance between temperatures as a lower limit for the lengthscale
                case 'max':
                    Constt = max(DistVec) # we use the max distance between temperature as a lower limit for the lengthscale
            
            lengthscale_constraint = gpytorch.constraints.GreaterThan( Constt )

        else: 

            lengthscale_constraint = None # there is no lower limit for the lengthscale
           

        self.covar_module = gpytorch.kernels.RBFKernel(lengthscale_prior = lengthscale_prior, lengthscale_constraint = lengthscale_constraint )

        
    def indexKernel_matrix(self,i_level1, i_level2, i_level3):
        #import pdb; pdb.set_trace()

        # Get task-task covariance
        covar_i_level1and2 = super().indexKernel_matrix(i_level1, i_level2)
        covar_i_level3 = self.task_covar_module_level3(i_level3)
        
        return covar_i_level1and2 + covar_i_level3 

    def covar_matrix(self,x,i_level1, i_level2, i_level3):

        # Get input-input covariance
        covar_x = self.covar_module(x)
        # Get task-task covariance
        Index_kernel = self.indexKernel_matrix(i_level1, i_level2, i_level3)
        # Multiply the two together to get the covariance we want
        covar = covar_x.mul(Index_kernel)

        return covar


    def forward(self,x,i_level1, i_level2, i_level3):
        #import pdb; pdb.set_trace()
        mean_x = self.mean_module(x)

        # Get input-input covariance
        covar_x = self.covar_module(x)
        # Get task-task covariance
        Index_kernel = self.indexKernel_matrix(i_level1, i_level2, i_level3)
        # Multiply the two together to get the covariance we want
        covar = covar_x.mul(Index_kernel)

        return gpytorch.distributions.MultivariateNormal(mean_x, covar)


####################################################################
#    HadamardMultitaskGPModel_3Levels_OneLengthscale_FixedLevels1and2and3
####################################################################
# this class is a child of HadamardMultitaskGPModel_2Levels_OneLengthscale_FixedLevels1and2, to which we add one level in the hierarchy
class HadamardMultitaskGPModel_3Levels_OneLengthscale_FixedLevels1and2and3(HadamardMultitaskGPModel_2Levels_OneLengthscale_FixedLevels1and2):
    def __init__(self, train_x, train_y, likelihood, mean = gpytorch.means.ZeroMean(), lengthscale_prior=None, lengthscale_MinConstraint = ['min', 'mean', 'median', 'max', None]):
        
        #super(HadamardMultitaskGPModel_3Levels_OneLengthscale_FixedLevels1and2_FreeLevel3, self).__init__(train_x, train_y, likelihood)
        HadamardMultitaskGPModel_2Levels_OneLengthscale_FixedLevels1and2.__init__(self, train_x, train_y, likelihood, lengthscale_prior=lengthscale_prior, lengthscale_MinConstraint=lengthscale_MinConstraint)

        assert len(train_x)>=4, "missing some training inputs"
        #import pdb; pdb.set_trace()
        self.mean_module = mean

        self.numTasks_level3  = len(set(train_x[3].unique()))
        
        # We learn an IndexKernel for 2 tasks
        # (so we'll actually learn 2x2=4 tasks with correlations)
        self.task_covar_module_level3 = DiagonalIndexKernel_Fixed(num_tasks = self.numTasks_level3, rank=1)
      
        if not lengthscale_MinConstraint is None:

            train_x_values = train_x[0].unique()
            DistVec = train_x_values[1:len(train_x_values)]-train_x_values[0:len(train_x_values)-1] # compute the distance between each pair of consecutive temperatures
            
            match lengthscale_MinConstraint:
                case 'min':
                    Constt = min(DistVec) # we use the minium distance between temperatures as a lower limit for the lengthscale
                case 'mean':
                    Constt = torch.mean(DistVec) # we use the mean distance between temperatures as a lower limit for the lengthscale
                case 'median':
                    Constt = torch.median(DistVec) # we use the medin distance between temperatures as a lower limit for the lengthscale
                case 'max':
                    Constt = max(DistVec) # we use the max distance between temperature as a lower limit for the lengthscale
            
            lengthscale_constraint = gpytorch.constraints.GreaterThan( Constt )

        else: 

            lengthscale_constraint = None # there is no lower limit for the lengthscale
         

        self.covar_module = gpytorch.kernels.RBFKernel(lengthscale_prior = lengthscale_prior, lengthscale_constraint = lengthscale_constraint )


        
    def indexKernel_matrix(self,i_level1, i_level2, i_level3):
        #import pdb; pdb.set_trace()

        # Get task-task covariance
        covar_i_level1and2 = super().indexKernel_matrix(i_level1, i_level2)
        covar_i_level3 = self.task_covar_module_level3(i_level3)
        
        return covar_i_level1and2 + covar_i_level3 

    def covar_matrix(self,x,i_level1, i_level2, i_level3):

        # Get input-input covariance
        covar_x = self.covar_module(x)
        # Get task-task covariance
        Index_kernel = self.indexKernel_matrix(i_level1, i_level2, i_level3)
        # Multiply the two together to get the covariance we want
        covar = covar_x.mul(Index_kernel)

        return covar


    def forward(self,x,i_level1, i_level2, i_level3):
        #import pdb; pdb.set_trace()
        mean_x = self.mean_module(x)

        # Get input-input covariance
        covar_x = self.covar_module(x)
        # Get task-task covariance
        Index_kernel = self.indexKernel_matrix(i_level1, i_level2, i_level3)
        # Multiply the two together to get the covariance we want
        covar = covar_x.mul(Index_kernel)

        return gpytorch.distributions.MultivariateNormal(mean_x, covar)


####################################################################
#    HadamardMultitaskGPModel_3Levels_TwoLengthscales_FixedLevels1and2_FreeLevel3
####################################################################
# this class is a child of HadamardMultitaskGPModel_2Levels_OneLengthscale_FixedLevels1and2, to which we add one level in the hierarchy with free outputscale and lengthscale

class HadamardMultitaskGPModel_3Levels_TwoLengthscales_FixedLevels1and2_FreeLevel3(HadamardMultitaskGPModel_2Levels_OneLengthscale_FixedLevels1and2):
    def __init__(self, train_x, train_y, likelihood, mean = gpytorch.means.ZeroMean(), lengthscale_prior=None, lengthscale_MinConstraint = ['min', 'mean', 'median', 'max', None]):
        
        HadamardMultitaskGPModel_2Levels_OneLengthscale_FixedLevels1and2.__init__(self, train_x, train_y, likelihood, lengthscale_prior=lengthscale_prior, lengthscale_MinConstraint=lengthscale_MinConstraint)

        assert len(train_x)>=4, "missing some training inputs"
        #import pdb; pdb.set_trace()
        self.mean_module = mean

        self.numTasks_level3  = len(set(train_x[3].unique()))
        
        # We learn an IndexKernel for 2 tasks
        # (so we'll actually learn 2x2=4 tasks with correlations)
        self.task_covar_module_level3 = DiagonalIndexKernel_Free(num_tasks = self.numTasks_level3, rank=1)
      
        if not lengthscale_MinConstraint is None:

            train_x_values = train_x[0].unique()
            DistVec = train_x_values[1:len(train_x_values)]-train_x_values[0:len(train_x_values)-1] # compute the distance between each pair of consecutive temperatures
            
            match lengthscale_MinConstraint:
                case 'min':
                    Constt = min(DistVec) # we use the minium distance between temperatures as a lower limit for the lengthscale
                case 'mean':
                    Constt = torch.mean(DistVec) # we use the mean distance between temperatures as a lower limit for the lengthscale
                case 'median':
                    Constt = torch.median(DistVec) # we use the medin distance between temperatures as a lower limit for the lengthscale
                case 'max':
                    Constt = max(DistVec) # we use the max distance between temperature as a lower limit for the lengthscale
            
            lengthscale_constraint = gpytorch.constraints.GreaterThan( Constt )

        else: 

            lengthscale_constraint = None # there is no lower limit for the lengthscale
           

        #self.covar_module_level3 = gpytorch.kernels.RBFKernel(lengthscale_prior = lengthscale_prior, lengthscale_constraint = lengthscale_constraint )
        self.covar_module_LastLevel = gpytorch.kernels.RBFKernel(lengthscale_prior = None, lengthscale_constraint = None )

    '''
    def indexKernel_matrix(self,i_level1, i_level2, i_level3):
        #import pdb; pdb.set_trace()

        # Get task-task covariance
        covar_i_level1and2 = super().indexKernel_matrix(i_level1, i_level2)
        covar_i_level3 = self.task_covar_module_level3(i_level3)
        
        return covar_i_level1and2 + covar_i_level3 
    '''
    

    def covar_matrix(self,x,i_level1, i_level2, i_level3):

        #mport pdb; pdb.set_trace()

        # Levels 1 and 2 
        covar_x_Level1and2 = self.covar_module(x)
        covar_i_Level1and2 = super().indexKernel_matrix(i_level1, i_level2)
        covar_Level1and2 = covar_x_Level1and2.mul(covar_i_Level1and2)

        # level 3
        # Get input-input covariance
        covar_x_Level3 = self.covar_module_LastLevel(x)
        # Get task-task covariance
        covar_i_Level3 = self.task_covar_module_level3(i_level3)
        # Multiply the two together to get the covariance we want
        covar_Level3 = covar_x_Level3.mul(covar_i_Level3)

        return covar_Level1and2 + covar_Level3


    def forward(self,x,i_level1, i_level2, i_level3):
        #import pdb; pdb.set_trace()
        mean_x = self.mean_module(x)
        covar = self.covar_matrix(x, i_level1, i_level2, i_level3)

        return gpytorch.distributions.MultivariateNormal(mean_x, covar)


####################################################################
#    HadamardMultitaskGPModel_3Levels_TwoLengthscales_FixedLevel1_FreeLevels2and3
####################################################################
# this class is a child of HadamardMultitaskGPModel_2Levels_OneLengthscale_FixedLevel1_FreeLevel2, to which we add one level in the hierarchy with free outputscale and lengthscale

class HadamardMultitaskGPModel_3Levels_TwoLengthscales_FixedLevel1_FreeLevels2and3(HadamardMultitaskGPModel_2Levels_OneLengthscale_FixedLevel1_FreeLevel2):
    def __init__(self, train_x, train_y, likelihood, mean = gpytorch.means.ZeroMean(), lengthscale_prior=None, lengthscale_MinConstraint = ['min', 'mean', 'median', 'max', None]):
        
        HadamardMultitaskGPModel_2Levels_OneLengthscale_FixedLevel1_FreeLevel2.__init__(self, train_x, train_y, likelihood, lengthscale_prior=lengthscale_prior, lengthscale_MinConstraint=lengthscale_MinConstraint)

        assert len(train_x)>=4, "missing some training inputs"
        #import pdb; pdb.set_trace()
        self.mean_module = mean

        self.numTasks_level3  = len(set(train_x[3].unique()))
        
        # We learn an IndexKernel for 2 tasks
        # (so we'll actually learn 2x2=4 tasks with correlations)
        self.task_covar_module_level3 = DiagonalIndexKernel_Free(num_tasks = self.numTasks_level3, rank=1)
      
        if not lengthscale_MinConstraint is None:

            train_x_values = train_x[0].unique()
            DistVec = train_x_values[1:len(train_x_values)]-train_x_values[0:len(train_x_values)-1] # compute the distance between each pair of consecutive temperatures
            
            match lengthscale_MinConstraint:
                case 'min':
                    Constt = min(DistVec) # we use the minium distance between temperatures as a lower limit for the lengthscale
                case 'mean':
                    Constt = torch.mean(DistVec) # we use the mean distance between temperatures as a lower limit for the lengthscale
                case 'median':
                    Constt = torch.median(DistVec) # we use the medin distance between temperatures as a lower limit for the lengthscale
                case 'max':
                    Constt = max(DistVec) # we use the max distance between temperature as a lower limit for the lengthscale
            
            lengthscale_constraint = gpytorch.constraints.GreaterThan( Constt )

        else: 

            lengthscale_constraint = None # there is no lower limit for the lengthscale
           

        #self.covar_module_level3 = gpytorch.kernels.RBFKernel(lengthscale_prior = lengthscale_prior, lengthscale_constraint = lengthscale_constraint )
        self.covar_module_LastLevel = gpytorch.kernels.RBFKernel(lengthscale_prior = None, lengthscale_constraint = None )

    
    def covar_matrix(self,x,i_level1, i_level2, i_level3):

        #mport pdb; pdb.set_trace()

        # Levels 1 and 2 
        covar_x_Level1and2 = self.covar_module(x)
        covar_i_Level1and2 = super().indexKernel_matrix(i_level1, i_level2)
        covar_Level1and2 = covar_x_Level1and2.mul(covar_i_Level1and2)

        # level 3
        # Get input-input covariance
        covar_x_Level3 = self.covar_module_LastLevel(x)
        # Get task-task covariance
        covar_i_Level3 = self.task_covar_module_level3(i_level3)
        # Multiply the two together to get the covariance we want
        covar_Level3 = covar_x_Level3.mul(covar_i_Level3)

        return covar_Level1and2 + covar_Level3


    def forward(self,x,i_level1, i_level2, i_level3):
        #import pdb; pdb.set_trace()
        mean_x = self.mean_module(x)
        covar = self.covar_matrix(x, i_level1, i_level2, i_level3)

        return gpytorch.distributions.MultivariateNormal(mean_x, covar)


####################################################################
#    HadamardMultitaskGPModel_3Levels_OneLengthscale_FixedLevel1_FreeLevels2and3
####################################################################
# this class is a child of HadamardMultitaskGPModel_2Levels_OneLengthscale_FixedLevel1_FreeLevel2, to which we add one level in the hierarchy with free outputscale and lengthscale

class HadamardMultitaskGPModel_3Levels_OneLengthscale_FixedLevel1_FreeLevels2and3(HadamardMultitaskGPModel_2Levels_OneLengthscale_FixedLevel1_FreeLevel2):
    def __init__(self, train_x, train_y, likelihood, mean = gpytorch.means.ZeroMean(), lengthscale_prior=None, lengthscale_MinConstraint = ['min', 'mean', 'median', 'max', None]):
        
        HadamardMultitaskGPModel_2Levels_OneLengthscale_FixedLevel1_FreeLevel2.__init__(self, train_x, train_y, likelihood, lengthscale_prior=lengthscale_prior, lengthscale_MinConstraint=lengthscale_MinConstraint)

        assert len(train_x)>=4, "missing some training inputs"
        #import pdb; pdb.set_trace()
        self.mean_module = mean

        self.numTasks_level3  = len(set(train_x[3].unique()))
        
        # We learn an IndexKernel for 2 tasks
        # (so we'll actually learn 2x2=4 tasks with correlations)
        self.task_covar_module_level3 = DiagonalIndexKernel_Free(num_tasks = self.numTasks_level3, rank=1)
      
        if not lengthscale_MinConstraint is None:

            train_x_values = train_x[0].unique()
            DistVec = train_x_values[1:len(train_x_values)]-train_x_values[0:len(train_x_values)-1] # compute the distance between each pair of consecutive temperatures
            
            match lengthscale_MinConstraint:
                case 'min':
                    Constt = min(DistVec) # we use the minium distance between temperatures as a lower limit for the lengthscale
                case 'mean':
                    Constt = torch.mean(DistVec) # we use the mean distance between temperatures as a lower limit for the lengthscale
                case 'median':
                    Constt = torch.median(DistVec) # we use the medin distance between temperatures as a lower limit for the lengthscale
                case 'max':
                    Constt = max(DistVec) # we use the max distance between temperature as a lower limit for the lengthscale
            
            lengthscale_constraint = gpytorch.constraints.GreaterThan( Constt )

        else: 

            lengthscale_constraint = None # there is no lower limit for the lengthscale
           

        self.covar_module = gpytorch.kernels.RBFKernel(lengthscale_prior = lengthscale_prior, lengthscale_constraint = lengthscale_constraint )

    
    def indexKernel_matrix(self,i_level1, i_level2, i_level3):
        #import pdb; pdb.set_trace()

        # Get task-task covariance
        covar_i_level1and2 = super().indexKernel_matrix(i_level1, i_level2)
        covar_i_level3 = self.task_covar_module_level3(i_level3)
        
        return covar_i_level1and2 + covar_i_level3 

    def covar_matrix(self,x,i_level1, i_level2, i_level3):

            # Get input-input covariance
            covar_x = self.covar_module(x)
            # Get task-task covariance
            Index_kernel = self.indexKernel_matrix(i_level1, i_level2, i_level3)
            # Multiply the two together to get the covariance we want
            covar = covar_x.mul(Index_kernel)

            return covar

    def forward(self,x,i_level1, i_level2, i_level3):
        #import pdb; pdb.set_trace()
        mean_x = self.mean_module(x)

        # Get input-input covariance
        covar_x = self.covar_module(x)
        # Get task-task covariance
        Index_kernel = self.indexKernel_matrix(i_level1, i_level2, i_level3)
        # Multiply the two together to get the covariance we want
        covar = covar_x.mul(Index_kernel)

        return gpytorch.distributions.MultivariateNormal(mean_x, covar)

####################################################################
#    HadamardMultitaskGPModel_4Levels_OneLengthscale_FixedLevels1and2_FreeLevels3and4
####################################################################

class HadamardMultitaskGPModel_4Levels_OneLengthscale_FixedLevels1and2_FreeLevels3and4(HadamardMultitaskGPModel_3Levels_OneLengthscale_FixedLevels1and2_FreeLevels3):
    def __init__(self, train_x, train_y, likelihood, mean = gpytorch.means.ZeroMean(), lengthscale_prior=None, lengthscale_MinConstraint = ['min', 'mean', 'median', 'max', None]):
        
        #super(HadamardMultitaskGPModel_4Levels_OneLengthscale_FixedLevels1and2_FreeLevel3and4, self).__init__(train_x, train_y, likelihood)
        HadamardMultitaskGPModel_3Levels_OneLengthscale_FixedLevels1and2_FreeLevels3.__init__(self, train_x, train_y, likelihood, lengthscale_prior=lengthscale_prior, lengthscale_MinConstraint=lengthscale_MinConstraint)

        assert len(train_x)>=5, "missing some training inputs"
        #import pdb; pdb.set_trace()
        self.mean_module = mean

        self.numTasks_level4  = len(set(train_x[4].unique()))
        
        # We learn an IndexKernel for 2 tasks
        # (so we'll actually learn 2x2=4 tasks with correlations)
        self.task_covar_module_level4 = DiagonalIndexKernel_Free(num_tasks = self.numTasks_level4, rank=1)
        
        if not lengthscale_MinConstraint is None:

            train_x_values = train_x[0].unique()
            DistVec = train_x_values[1:len(train_x_values)]-train_x_values[0:len(train_x_values)-1] # compute the distance between each pair of consecutive temperatures
            
            match lengthscale_MinConstraint:
                case 'min':
                    Constt = min(DistVec) # we use the minium distance between temperatures as a lower limit for the lengthscale
                case 'mean':
                    Constt = torch.mean(DistVec) # we use the mean distance between temperatures as a lower limit for the lengthscale
                case 'median':
                    Constt = torch.median(DistVec) # we use the medin distance between temperatures as a lower limit for the lengthscale
                case 'max':
                    Constt = max(DistVec) # we use the max distance between temperature as a lower limit for the lengthscale
            
            lengthscale_constraint = gpytorch.constraints.GreaterThan( Constt )

        else: 

            lengthscale_constraint = None # there is no lower limit for the lengthscale
        

        self.covar_module = gpytorch.kernels.RBFKernel(lengthscale_prior = lengthscale_prior, lengthscale_constraint = lengthscale_constraint )

        
    def indexKernel_matrix(self,i_level1, i_level2, i_level3, i_level4):
        #import pdb; pdb.set_trace()
        covar_i_level1and2and3 = super().indexKernel_matrix(i_level1, i_level2, i_level3)
        covar_i_level4 = self.task_covar_module_level4(i_level4)
        
        return covar_i_level1and2and3 + covar_i_level4

    def covar_matrix(self,x,i_level1, i_level2, i_level3, i_level4):

        # Get input-input covariance
        covar_x = self.covar_module(x)
        # Get task-task covariance
        Index_kernel = self.indexKernel_matrix(i_level1, i_level2, i_level3, i_level4)
        # Multiply the two together to get the covariance we want
        covar = covar_x.mul(Index_kernel)

        return covar


    def forward(self,x,i_level1, i_level2, i_level3, i_level4):
        #import pdb; pdb.set_trace()
        mean_x = self.mean_module(x)

        # Get input-input covariance
        covar_x = self.covar_module(x)
        # Get task-task covariance
        Index_kernel = self.indexKernel_matrix(i_level1, i_level2, i_level3, i_level4)
        # Multiply the two together to get the covariance we want
        covar = covar_x.mul(Index_kernel)

        return gpytorch.distributions.MultivariateNormal(mean_x, covar)

    
####################################################################
#    HadamardMultitaskGPModel_4Levels_OneLengthscale_FixedLevels1and2_FreeLevels3FixedLevels4
####################################################################

class HadamardMultitaskGPModel_4Levels_OneLengthscale_FixedLevels1and2_FreeLevels3FixedLevels4(HadamardMultitaskGPModel_3Levels_OneLengthscale_FixedLevels1and2_FreeLevels3):
    def __init__(self, train_x, train_y, likelihood, mean = gpytorch.means.ZeroMean(), lengthscale_prior=None, lengthscale_MinConstraint = ['min', 'mean', 'median', 'max', None]):
        
        HadamardMultitaskGPModel_3Levels_OneLengthscale_FixedLevels1and2_FreeLevels3.__init__(self, train_x, train_y, likelihood, lengthscale_prior=lengthscale_prior, lengthscale_MinConstraint=lengthscale_MinConstraint)

        assert len(train_x)>=5, "missing some training inputs"
        #import pdb; pdb.set_trace()
        self.mean_module = mean

        self.numTasks_level4  =  len(set(train_x[4].unique()))
        
        # We learn an IndexKernel for 2 tasks
        self.task_covar_module_level4 = DiagonalIndexKernel_Fixed(num_tasks = self.numTasks_level4, rank=1)
        
        if not lengthscale_MinConstraint is None:

            train_x_values = train_x[0].unique()
            DistVec = train_x_values[1:len(train_x_values)]-train_x_values[0:len(train_x_values)-1] # compute the distance between each pair of consecutive temperatures
            
            match lengthscale_MinConstraint:
                case 'min':
                    Constt = min(DistVec) # we use the minium distance between temperatures as a lower limit for the lengthscale
                case 'mean':
                    Constt = torch.mean(DistVec) # we use the mean distance between temperatures as a lower limit for the lengthscale
                case 'median':
                    Constt = torch.median(DistVec) # we use the medin distance between temperatures as a lower limit for the lengthscale
                case 'max':
                    Constt = max(DistVec) # we use the max distance between temperature as a lower limit for the lengthscale
            
            lengthscale_constraint = gpytorch.constraints.GreaterThan( Constt )

        else: 

            lengthscale_constraint = None # there is no lower limit for the lengthscale
         

        self.covar_module = gpytorch.kernels.RBFKernel(lengthscale_prior = lengthscale_prior, lengthscale_constraint = lengthscale_constraint )


        
    def indexKernel_matrix(self,i_level1, i_level2, i_level3, i_level4):
        #import pdb; pdb.set_trace()

        # Get task-task covariance
        covar_i_level1and2and3 = super().indexKernel_matrix(i_level1, i_level2, i_level3)
        covar_i_level4 = self.task_covar_module_level4(i_level4)
        
        return covar_i_level1and2and3 + covar_i_level4

    def covar_matrix(self,x,i_level1, i_level2, i_level3, i_level4):

        # Get input-input covariance
        covar_x = self.covar_module(x)
        # Get task-task covariance
        Index_kernel = self.indexKernel_matrix(i_level1, i_level2, i_level3, i_level4)
        # Multiply the two together to get the covariance we want
        covar = covar_x.mul(Index_kernel)

        return covar


    def forward(self,x,i_level1, i_level2, i_level3, i_level4):
        #import pdb; pdb.set_trace()
        mean_x = self.mean_module(x)

        # Get input-input covariance
        covar_x = self.covar_module(x)
        # Get task-task covariance
        Index_kernel = self.indexKernel_matrix(i_level1, i_level2, i_level3, i_level4)
        # Multiply the two together to get the covariance we want
        covar = covar_x.mul(Index_kernel)

        return gpytorch.distributions.MultivariateNormal(mean_x, covar)


####################################################################
#    HadamardMultitaskGPModel_4Levels_OneLengthscale_FixedLevels1and2and3_FreeLevels4
####################################################################

class HadamardMultitaskGPModel_4Levels_OneLengthscale_FixedLevels1and2and3_FreeLevels4(HadamardMultitaskGPModel_3Levels_OneLengthscale_FixedLevels1and2and3):
    def __init__(self, train_x, train_y, likelihood, mean = gpytorch.means.ZeroMean(), lengthscale_prior=None,lengthscale_MinConstraint = ['min', 'mean', 'median', 'max', None]):
        
        HadamardMultitaskGPModel_3Levels_OneLengthscale_FixedLevels1and2and3.__init__(self, train_x, train_y, likelihood, lengthscale_prior=lengthscale_prior, lengthscale_MinConstraint=lengthscale_MinConstraint)

        assert len(train_x)>=5, "missing some training inputs"
        #import pdb; pdb.set_trace()
        self.mean_module = mean

        self.numTasks_level4  =  len(set(train_x[4].unique()))
        
        # We learn an IndexKernel for 2 tasks
        self.task_covar_module_level4 = DiagonalIndexKernel_Free(num_tasks = self.numTasks_level4, rank=1)
        
        if not lengthscale_MinConstraint is None:

            #train_x_values = train_x[0].unique()
            #DistVec = train_x_values[1:len(train_x_values)]-train_x_values[0:len(train_x_values)-1] # compute the distance between each pair of consecutive temperatures

            Distance_df = pd.DataFrame(data = {'x': train_x[0], 'group': train_x[len(train_x)-1].squeeze()})
            Distance_df['Diff'] = Distance_df.groupby('group').x.transform('diff') # compute the distance between each consecutive temperatures inside a group (the group representing the lowest level in the model)

            
            match lengthscale_MinConstraint:
                case 'min':
                    #Constt = min(DistVec) # we use the minium distance between temperatures as a lower limit for the lengthscale
                    Distance_df['MinDiff'] = Distance_df.groupby('group').Diff.transform('min')
                    Constt = min(Distance_df['MinDiff']) # we use the minium distance between temperatures as a lower limit for the lengthscale
                case 'mean':
                    Distance_df['MeanDiff'] = Distance_df.groupby('group').Diff.transform('mean')
                    Constt = torch.mean(Distance_df['MeanDiff']) # we use the mean distance between temperatures as a lower limit for the lengthscale
                case 'median':
                    Distance_df['MedianDiff'] = Distance_df.groupby('group').Diff.transform('median')
                    Constt = torch.median(Distance_df['MedianDiff']) # we use the medin distance between temperatures as a lower limit for the lengthscale
                case 'max':
                    Distance_df['MaxDiff'] = Distance_df.groupby('group').Diff.transform('max')
                    Constt = max(Distance_df['MaxDiff']) # we use the max distance between temperature as a lower limit for the lengthscale
            
            lengthscale_constraint = gpytorch.constraints.GreaterThan( Constt )

        else: 

            lengthscale_constraint = None # there is no lower limit for the lengthscale
        
        self.covar_module = gpytorch.kernels.RBFKernel(lengthscale_prior = lengthscale_prior, lengthscale_constraint = lengthscale_constraint )


        
    def indexKernel_matrix(self,i_level1, i_level2, i_level3, i_level4):
        #import pdb; pdb.set_trace()

        # Get task-task covariance
        covar_i_level1and2and3 = super().indexKernel_matrix(i_level1, i_level2, i_level3)
        covar_i_level4 = self.task_covar_module_level4(i_level4)
        
        return covar_i_level1and2and3 + covar_i_level4

    def covar_matrix(self,x,i_level1, i_level2, i_level3, i_level4):

        # Get input-input covariance
        covar_x = self.covar_module(x)
        # Get task-task covariance
        Index_kernel = self.indexKernel_matrix(i_level1, i_level2, i_level3, i_level4)
        # Multiply the two together to get the covariance we want
        covar = covar_x.mul(Index_kernel)

        return covar


    def forward(self,x,i_level1, i_level2, i_level3, i_level4):
        #import pdb; pdb.set_trace()
        mean_x = self.mean_module(x)

        # Get input-input covariance
        covar_x = self.covar_module(x)
        # Get task-task covariance
        Index_kernel = self.indexKernel_matrix(i_level1, i_level2, i_level3, i_level4)
        # Multiply the two together to get the covariance we want
        covar = covar_x.mul(Index_kernel)

        return gpytorch.distributions.MultivariateNormal(mean_x, covar)


####################################################################
#    HadamardMultitaskGPModel_4Levels_OneLengthscale_FixedLevels1and2and3and4
####################################################################

class HadamardMultitaskGPModel_4Levels_OneLengthscale_FixedLevels1and2and3and4(HadamardMultitaskGPModel_3Levels_OneLengthscale_FixedLevels1and2and3):
    def __init__(self, train_x, train_y, likelihood, mean = gpytorch.means.ZeroMean(), lengthscale_prior=None, lengthscale_MinConstraint = ['min', 'mean', 'median', 'max', None]):
        
        HadamardMultitaskGPModel_3Levels_OneLengthscale_FixedLevels1and2and3.__init__(self, train_x, train_y, likelihood, lengthscale_prior=lengthscale_prior, lengthscale_MinConstraint=lengthscale_MinConstraint)

        assert len(train_x)>=5, "missing some training inputs"

        self.mean_module = mean

        self.numTasks_level4  =  len(set(train_x[4].unique()))
        
        # We learn an IndexKernel for 2 tasks
        self.task_covar_module_level4 = DiagonalIndexKernel_Fixed(num_tasks = self.numTasks_level4, rank=1)
        
        if not lengthscale_MinConstraint is None:

            train_x_values = train_x[0].unique()
            DistVec = train_x_values[1:len(train_x_values)]-train_x_values[0:len(train_x_values)-1] # compute the distance between each pair of consecutive temperatures
            
            match lengthscale_MinConstraint:
                case 'min':
                    Constt = min(DistVec) # we use the minium distance between temperatures as a lower limit for the lengthscale
                case 'mean':
                    Constt = torch.mean(DistVec) # we use the mean distance between temperatures as a lower limit for the lengthscale
                case 'median':
                    Constt = torch.median(DistVec) # we use the medin distance between temperatures as a lower limit for the lengthscale
                case 'max':
                    Constt = max(DistVec) # we use the max distance between temperature as a lower limit for the lengthscale
            
            lengthscale_constraint = gpytorch.constraints.GreaterThan( Constt )

        else: 

            lengthscale_constraint = None # there is no lower limit for the lengthscale
        
        self.covar_module = gpytorch.kernels.RBFKernel(lengthscale_prior = lengthscale_prior, lengthscale_constraint = lengthscale_constraint )

        
    def indexKernel_matrix(self,i_level1, i_level2, i_level3, i_level4):
        #import pdb; pdb.set_trace()

        # Get task-task covariance
        covar_i_level1and2and3 = super().indexKernel_matrix(i_level1, i_level2, i_level3)
        covar_i_level4 = self.task_covar_module_level4(i_level4)
        
        return covar_i_level1and2and3 + covar_i_level4

    def covar_matrix(self,x,i_level1, i_level2, i_level3, i_level4):

        # Get input-input covariance
        covar_x = self.covar_module(x)
        # Get task-task covariance
        Index_kernel = self.indexKernel_matrix(i_level1, i_level2, i_level3, i_level4)
        # Multiply the two together to get the covariance we want
        covar = covar_x.mul(Index_kernel)

        return covar


    def forward(self,x,i_level1, i_level2, i_level3, i_level4):
        #import pdb; pdb.set_trace()
        mean_x = self.mean_module(x)

        # Get input-input covariance
        covar_x = self.covar_module(x)
        # Get task-task covariance
        Index_kernel = self.indexKernel_matrix(i_level1, i_level2, i_level3, i_level4)
        # Multiply the two together to get the covariance we want
        covar = covar_x.mul(Index_kernel)

        return gpytorch.distributions.MultivariateNormal(mean_x, covar)

####################################################################
#    HadamardMultitaskGPModel_4Levels_TwoLengthscales_FixedLevels1and2and3_FreeLevel4
####################################################################

class HadamardMultitaskGPModel_4Levels_TwoLengthscales_FixedLevels1and2and3_FreeLevel4(HadamardMultitaskGPModel_3Levels_OneLengthscale_FixedLevels1and2and3):
    def __init__(self, train_x, train_y, likelihood, mean = gpytorch.means.ZeroMean(), lengthscale_prior=None,lengthscale_MinConstraint = ['min', 'mean', 'median', 'max', None]):
        
        HadamardMultitaskGPModel_3Levels_OneLengthscale_FixedLevels1and2and3.__init__(self, train_x, train_y, likelihood, lengthscale_prior=lengthscale_prior, lengthscale_MinConstraint=lengthscale_MinConstraint)

        assert len(train_x)>=5, "missing some training inputs"
        #import pdb; pdb.set_trace()
        self.mean_module = mean

        self.numTasks_level4  =  len(set(train_x[4].unique()))
        
        # We learn an IndexKernel for 2 tasks
        self.task_covar_module_level4 = DiagonalIndexKernel_Free(num_tasks = self.numTasks_level4, rank=1)
        
        if not lengthscale_MinConstraint is None:

            train_x_values = train_x[0].unique()
            DistVec = train_x_values[1:len(train_x_values)]-train_x_values[0:len(train_x_values)-1] # compute the distance between each pair of consecutive temperatures
            
            match lengthscale_MinConstraint:
                case 'min':
                    Constt = min(DistVec) # we use the minium distance between temperatures as a lower limit for the lengthscale
                case 'mean':
                    Constt = torch.mean(DistVec) # we use the mean distance between temperatures as a lower limit for the lengthscale
                case 'median':
                    Constt = torch.median(DistVec) # we use the medin distance between temperatures as a lower limit for the lengthscale
                case 'max':
                    Constt = max(DistVec) # we use the max distance between temperature as a lower limit for the lengthscale
            
            lengthscale_constraint = gpytorch.constraints.GreaterThan( Constt )

        else: 

            lengthscale_constraint = None # there is no lower limit for the lengthscale
        
        #self.covar_module = gpytorch.kernels.RBFKernel(lengthscale_prior = lengthscale_prior, lengthscale_constraint = lengthscale_constraint )
        self.covar_module_LastLevel = gpytorch.kernels.RBFKernel(lengthscale_prior = None, lengthscale_constraint = None )


    def covar_matrix(self,x,i_level1, i_level2, i_level3, i_level4):

        # Levels 1 and 2 and 3
        covar_x_Level1and2and3 = self.covar_module(x)
        covar_i_Level1and2and3 = super().indexKernel_matrix(i_level1, i_level2,i_level3)
        covar_Level1and2and3 = covar_x_Level1and2and3.mul(covar_i_Level1and2and3)

        # level 4
        # Get input-input covariance
        covar_x_Level4 = self.covar_module_LastLevel(x)
        # Get task-task covariance
        covar_i_Level4 = self.task_covar_module_level4(i_level4)
        # Multiply the two together to get the covariance we want
        covar_Level4 = covar_x_Level4.mul(covar_i_Level4)

        return covar_Level1and2and3 + covar_Level4


    def forward(self,x,i_level1, i_level2, i_level3, i_level4):
        #import pdb; pdb.set_trace()
        mean_x = self.mean_module(x)

        covar = self.covar_matrix(x, i_level1, i_level2, i_level3,i_level4)


        return gpytorch.distributions.MultivariateNormal(mean_x, covar)


####################################################################
#    HadamardMultitaskGPModel_4Levels_TwoLengthscales_FixedLevels1and2_FreeLevels3and4
####################################################################

class HadamardMultitaskGPModel_4Levels_TwoLengthscales_FixedLevels1and2_FreeLevels3and4(HadamardMultitaskGPModel_3Levels_OneLengthscale_FixedLevels1and2_FreeLevels3):
    def __init__(self, train_x, train_y, likelihood, mean = gpytorch.means.ZeroMean(), lengthscale_prior=None,lengthscale_MinConstraint = ['min', 'mean', 'median', 'max', None]):
        
        HadamardMultitaskGPModel_3Levels_OneLengthscale_FixedLevels1and2_FreeLevels3.__init__(self, train_x, train_y, likelihood, lengthscale_prior=lengthscale_prior, lengthscale_MinConstraint=lengthscale_MinConstraint)

        assert len(train_x)>=5, "missing some training inputs"
        #import pdb; pdb.set_trace()
        self.mean_module = mean

        self.numTasks_level4  =  len(set(train_x[4].unique()))
        
        # We learn an IndexKernel for 2 tasks
        self.task_covar_module_level4 = DiagonalIndexKernel_Free(num_tasks = self.numTasks_level4, rank=1)
        
        if not lengthscale_MinConstraint is None:

            train_x_values = train_x[0].unique()
            DistVec = train_x_values[1:len(train_x_values)]-train_x_values[0:len(train_x_values)-1] # compute the distance between each pair of consecutive temperatures
            
            match lengthscale_MinConstraint:
                case 'min':
                    Constt = min(DistVec) # we use the minium distance between temperatures as a lower limit for the lengthscale
                case 'mean':
                    Constt = torch.mean(DistVec) # we use the mean distance between temperatures as a lower limit for the lengthscale
                case 'median':
                    Constt = torch.median(DistVec) # we use the medin distance between temperatures as a lower limit for the lengthscale
                case 'max':
                    Constt = max(DistVec) # we use the max distance between temperature as a lower limit for the lengthscale
            
            lengthscale_constraint = gpytorch.constraints.GreaterThan( Constt )

        else: 

            lengthscale_constraint = None # there is no lower limit for the lengthscale
        
        #self.covar_module = gpytorch.kernels.RBFKernel(lengthscale_prior = lengthscale_prior, lengthscale_constraint = lengthscale_constraint )
        self.covar_module_LastLevel = gpytorch.kernels.RBFKernel(lengthscale_prior = None, lengthscale_constraint = None )


    def covar_matrix(self,x,i_level1, i_level2, i_level3, i_level4):

        # Levels 1 and 2 and 3
        covar_x_Level1and2and3 = self.covar_module(x)
        covar_i_Level1and2and3 = super().indexKernel_matrix(i_level1, i_level2,i_level3)
        covar_Level1and2and3 = covar_x_Level1and2and3.mul(covar_i_Level1and2and3)

        # level 4
        # Get input-input covariance
        covar_x_Level4 = self.covar_module_LastLevel(x)
        # Get task-task covariance
        covar_i_Level4 = self.task_covar_module_level4(i_level4)
        # Multiply the two together to get the covariance we want
        covar_Level4 = covar_x_Level4.mul(covar_i_Level4)

        return covar_Level1and2and3 + covar_Level4


    def forward(self,x,i_level1, i_level2, i_level3, i_level4):
        #import pdb; pdb.set_trace()
        mean_x = self.mean_module(x)

        covar = self.covar_matrix(x, i_level1, i_level2, i_level3,i_level4)


        return gpytorch.distributions.MultivariateNormal(mean_x, covar)
