#! /usr/bin/env python3

from pathlib import Path
from matplotlib import pyplot as plt
import os
import numpy as np
from collections import Counter
import pandas as pd
from matplotlib.lines import Line2D

from utils.Check_modelTypeValidity import Check_modelTypeValidity
from utils.colorblind_palette import colorblind_palette_forConditions
from utils.shape_palette import shape_palette_forReplicates
from plotting_functions.mscatter import mscatter
from plotting_functions.adjust_text_size import add_text_page
from plotting_functions.optimal_subplots import optimal_subplot_disposition

from matplotlib.backends.backend_pdf import PdfPages

####################################################################
#    rename_conditions
####################################################################
def rename_conditions(df, control_condition, treatment_condition):
    conditions = [
        df['condition'] == 'control_condition',
        df['condition'] == 'treatment_condition',
        df['condition'] == 'joint_condition'
    ]
    choices = [
        control_condition,  
        treatment_condition, 
        'joint_condition'
    ]
    default = np.nan

    return np.select(conditions, choices, default=default)


####################################################################
#    Plot_Model_predictions_and_comparison
####################################################################
def Plot_Model_predictions_and_comparison(
    data, 
    parameters,
    all_predictions_full,
    all_predictions_joint,
    ItemToPlot=None, 
    MaxPlotPerRow=8,
    PlotSave =False, 
    SavingPath=None, 
    debug=True):

    r'''
        Args:
            - ItemToPlot : potentially the set of IDs to plot. If not specified, all IDs are plotted. in any case, all comparisons of the selected IDs are plotted
    '''

    # only enter this function if the user wants these plots
    if PlotSave:

        type_pred = parameters['prediction_type']

        if not type_pred in  ['predicted_functions', 'predicted_observations']:
            raise ValueError(f"the prediction type (either posterior predictive distribution using predicted_observations, or model posterior distribution using predicted_functions) should be correctly specified.")

        if debug:
            print("saving the prediction plots")
                    
        ######## subset the IDs to plot #########
        if ItemToPlot is None :

            # we consider all IDs which have been predicted
            ItemToPlot = np.unique(all_predictions_full['Level_1']['Level_1'])

        if debug:
            print(ItemToPlot)

        if not all_predictions_full.keys() == all_predictions_joint.keys():
            raise ValueError(f"the dictionaries of  predictions for the full and joint models do not contain the same predicted levels: please do check the function generating the predictions.")
        
        prediction_keys = all_predictions_full.keys()
        
        ######## check that these IDs have been predicted #########
        for key in prediction_keys:
            if debug:
                print(set(ItemToPlot))
                print(set(np.unique(all_predictions_full[key]['Level_1'])))
                print(set(np.unique(all_predictions_joint[key]['Level_1'])))
            
            if not set(ItemToPlot).issubset(set(np.unique(all_predictions_full[key]['Level_1']))):
                raise ValueError(f"Some of the IDs to plot do not appear in the predictions data frame corresponding to " + key + " for the full model.")

            if not set(ItemToPlot).issubset(set(np.unique(all_predictions_joint[key]['Level_1']))):
                raise ValueError(f"Some of the IDs to plot do not appear in the predictions data frame corresponding to " + key + " for the joint model.")
        
        #################### General variables ####################
        ### Define the number of levels in the hierarchical model and check the presence of the appropriate columns ###
        NLevels, dfColnames_Levels = Check_modelTypeValidity(data, parameters['modelType'])

        #################### Iterates over the Ids ####################
        for id in ItemToPlot:

            # Create a PDF file in which all plots will be saved
            with PdfPages(type_pred + '_' + str(id)+'.pdf') as pdf, PdfPages(type_pred + '_comparison_' + str(id)+'.pdf') as pdf_comp:

                #### start the pdf with full HGPM plots with an explanation ###
                if type_pred == 'predicted_functions':

                    introduction_text = "The following plots represent the model posterior distribution $p(f* | x*, X, y)$ at some test points $x*$, given training data $(x,y)$. This posterior is the distribution over the function $f$ modeling the melting curves, and thus quantifies the model uncertainty. (citing from gpytorch documentation)."

                elif type_pred == 'predicted_observations':
                    
                    introduction_text = "The following plots represent the posterior predictive distribution $p(y* | x*, X, y)$ at some test points $x*$, given training data $(x,y)$. This corresponds to the probability distribution over the predicted output value $y*$. (citing from gpytorch documentation)."

                introduction_text_pred = introduction_text + "\n\n\nEach row represents a level of the HGP model. The bottom plots are the replicate-wise plots, the second level plots are the condition-wise plots and the top plot is the ID-wise plot. Possible additional levels might have additional interpretation. Replicates are represented by different markers. For each comparison, the first plot is obtained from the full HGPM fit, and the second shows in black how the predictions from the joint HGP model compares to the full HGP model."
                
                add_text_page(pdf, introduction_text_pred)

                ### start the pdf with the comparison plots with an explanation ###
                
                introduction_text_comp = introduction_text + ""

                add_text_page(pdf_comp, introduction_text_comp)


                #################### General variables ####################
                #reduce each prediction df to the id of interest
                pred_item = {'full' : {}, 'joint' : {} }

                for key in prediction_keys:
                    pred_item['full'][key] = all_predictions_full[key].loc[all_predictions_full[key]['Level_1']==id]
                    pred_item['joint'][key] = all_predictions_joint[key].loc[all_predictions_joint[key]['Level_1']==id]

                ## define a shape for each replicate of a control and a treatment condition
                shape_df = shape_palette_forReplicates(pred_item['full'], NLevels)
                
                ## define the color 
                N_cond = pred_item['full']['Levels_1to2']['Level_2'].nunique()
                color_df = colorblind_palette_forConditions(N_cond)
                
                #################### Control conditions IDs ####################
                # as many plots as Control conditions for this ID
                # the idea is to group on one plot all the comparisons corresponding to a given control condition
                # 'all_predictions_joint['Levels_1to2']['control_condition']' contains the all the control conditions considered for this ID
                # it might be only one, or it might be all conditions (if the user did not specify a specific control condition).
                control_conditions_forThisUniqueID = pred_item['joint']['Levels_1to2']['control_condition'].unique()

                #################### Iterates over the comparisons ####################
                for ctl_ID in control_conditions_forThisUniqueID:
                    #import pdb; pdb.set_trace()
                    
                    #################### Data subset ####################
                    # subset the joint model predictions to the control condition of interest
                    # for these plots we only need the second level of the hierarchy to be plotted (condition-wise predictions)
                    pred_item_ctl_ID = {'full' : {}, 'joint' : {} }
                    pred_item_ctl_ID['joint']['Levels_1to2'] = pred_item['joint']['Levels_1to2'].loc[pred_item['joint']['Levels_1to2']['control_condition'].isin([ctl_ID])]   

                    # get the names of all treatment conditions which have been compared to this control condition
                    ttt_Ids  = list(pred_item_ctl_ID['joint']['Level_2'].unique())

                    # subset the full model predictions to the treatment conditions of interest
                    # for these plots we only need the second level of the hierarchy to be plotted (condition-wise predictions)
                    pred_item_ctl_ID['full']['Levels_1to2'] = pred_item['full']['Levels_1to2'].loc[pred_item['full']['Levels_1to2']['Level_2'].isin([ctl_ID] + ttt_Ids)]   

                    #################### Define the color and shapes ####################
                    ### associate the right colors to each comparison
                    # we want the control condition to always be depicted by color "#009E73" & the joint model by "black"
                    # the other conditions are associated to up to 8 different colorblind-friendly colors.
                    #Create a mapping and inverse mapping from unique values of treatment condition to 'additional_condition_i'
                    # Setup both mappings at the same time
                    mapping_ttt_cd = {}
                    inverse_mapping_ttt_cd = {}
                    for i, value in enumerate(ttt_Ids, 1):
                        mapping_ttt_cd[value] = f'additional_condition_{i}'
                        inverse_mapping_ttt_cd[f'additional_condition_{i}'] = value
                    #mapping_ttt_cd = {val: f'additional_condition_{i+1}' for i, val in enumerate(ttt_Ids)}

                    # First deal with the joint model predictions
                    if 'condition' in pred_item_ctl_ID['joint']['Levels_1to2'].columns:
                        pred_item_ctl_ID['joint']['Levels_1to2'] = pred_item_ctl_ID['joint']['Levels_1to2'].drop(columns=['condition'])
                   
                    # Define the new column using np.where
                    pred_item_ctl_ID['joint']['Levels_1to2']['condition'] = pred_item_ctl_ID['joint']['Levels_1to2']['Level_2'].apply(lambda x: 'joint_condition' if x == 'joint_condition' else mapping_ttt_cd[x])
                    pred_item_ctl_ID['joint']['Levels_1to2'] = pd.merge(pred_item_ctl_ID['joint']['Levels_1to2'], color_df, how = 'left', on=['condition'])

                    # Then deal with the full model predictions
                    if 'condition' in pred_item_ctl_ID['full']['Levels_1to2'].columns:
                        pred_item_ctl_ID['full']['Levels_1to2'] = pred_item_ctl_ID['full']['Levels_1to2'].drop(columns=['condition'])
                   
                    # Define the new column using np.where
                    pred_item_ctl_ID['full']['Levels_1to2']['condition'] = pred_item_ctl_ID['full']['Levels_1to2']['Level_2'].apply(lambda x: 'control_condition' if x == ctl_ID else mapping_ttt_cd[x])
                    pred_item_ctl_ID['full']['Levels_1to2'] = pd.merge(pred_item_ctl_ID['full']['Levels_1to2'], color_df, how = 'left', on=['condition'])

                    ### associate the right shape to the replicates of the observations
                    data_ctl_ID = data.loc[(data['Level_1']==id) & (data['Level_2'].isin([ctl_ID] + ttt_Ids))]
                    if 'condition' in data_ctl_ID.columns:
                        data_ctl_ID = data_ctl_ID.drop(columns=['condition'])

                    data_ctl_ID['condition'] = np.where(data_ctl_ID['Level_2'] == ctl_ID , 'control_condition',  'treatment_condition')
                    data_ctl_ID = pd.merge(data_ctl_ID, shape_df, how = 'left', on=['condition', 'rep' ])

                    #################### Define the legends of the coming plot  ####################
                    
                    #import pdb; pdb.set_trace()
                    labels = []
                    for i in range(shape_df.shape[0]):
                        labels.append('rep ' + str(shape_df['rep'][i]) + ' - ' + shape_df['condition'][i] )
                    
                    # Create a scatter plot with markers and labels
                    plt.figure()
                    handles = []

                    for i, row in shape_df.iterrows():
                        handle = plt.scatter(i, i, marker=shape_df['markers'][i], c = 'k')
                        handles.append(handle)   

                    plt.close()

                    # add the color of the fits to the legend
                    custom_lines_handles = []
                    custom_lines_labels = []

                    print("legend definition")
                    for cd in ['control_condition', 'joint_condition'] + [f'additional_condition_{i+1}' for i,_ in enumerate(ttt_Ids)] :
                        print(cd)
                        custom_lines_handles.append(Line2D([0], [0], color = color_df.loc[color_df['condition'] == cd]['color'].item() , lw = 4))
                        custom_lines_labels.append( 'joint_condition' if cd == 'joint_condition' else pred_item_ctl_ID['full']['Levels_1to2'].loc[pred_item_ctl_ID['full']['Levels_1to2']['condition']==cd]['Level_2'].item() )
                    
                    # labels starting with "_" are ignored => in case this should not be the case (e.g with the phosphoTPP dataset), then we prepend backslash to the labels
                    custom_lines_labels = ['\\' + label if label.startswith('_') else label for label in custom_lines_labels]

                    #################### Define the plot  ####################

                    ### compute the optimal arrangement of plots for these number of comparisons.
                    # the first subplot will contain the legend, common to all other subplots, so we need len(ttt_Ids)+1 subplots
                    rows, cols, fig_width, fig_height = optimal_subplot_disposition(len(ttt_Ids)+1)
                    
                    fig_comp = plt.figure(figsize=(fig_width, fig_height), constrained_layout=True)
                    
                    if type_pred == 'predicted_functions':
                        NamePlot_comp =  'Considering ' + str(ctl_ID) + ' as control condition'  + '\n Comparisons of model posterior distributions for the full vs joint HGP models.'
                    elif type_pred == 'predicted_observations':
                        NamePlot_comp =  'Considering ' + str(ctl_ID) + ' as control condition'  +  '\n Comparisons of posterior predictive distributions for the full vs joint HGP models.'
                    
                    fig_comp.suptitle(NamePlot_comp, weight='semibold', size='xx-large')

                    ## Add subplots
                    for i in range(0, len(ttt_Ids) + 1):
                        
                        #create the subplot
                        ax_comp = fig_comp.add_subplot(rows, cols, i+1)
                        ax_comp.grid(linestyle = '--')

                        if i==0:
                            # the first subplot will contain the legend, common to all other subplots
                            ax_comp.legend(handles = handles + custom_lines_handles ,labels = labels + custom_lines_labels, loc='upper left')

                        else:

                            treatment_condition_tmp =  inverse_mapping_ttt_cd[f'additional_condition_{i}']

                            #ax_comp = fig_comp.add_subplot(rows, cols, i)
                            #ax_comp.grid(linestyle = '--')

                            ## treatment condition ##
                            # Plot predictive mean for the treatment conditions
                            ax_comp.plot(
                                pred_item_ctl_ID['full']['Levels_1to2'].loc[pred_item_ctl_ID['full']['Levels_1to2']['Level_2']== treatment_condition_tmp]['x'], 
                                pred_item_ctl_ID['full']['Levels_1to2'].loc[pred_item_ctl_ID['full']['Levels_1to2']['Level_2']== treatment_condition_tmp]['y'], 
                                color=pred_item_ctl_ID['full']['Levels_1to2'].loc[pred_item_ctl_ID['full']['Levels_1to2']['Level_2']== treatment_condition_tmp]['color'].unique().item())
                            # Shade between the lower and upper confidence bounds for the treatment conditions
                            ax_comp.fill_between(
                                pred_item_ctl_ID['full']['Levels_1to2'].loc[pred_item_ctl_ID['full']['Levels_1to2']['Level_2']== treatment_condition_tmp]['x'], 
                                pred_item_ctl_ID['full']['Levels_1to2'].loc[pred_item_ctl_ID['full']['Levels_1to2']['Level_2']== treatment_condition_tmp]['conf_lower'],
                                pred_item_ctl_ID['full']['Levels_1to2'].loc[pred_item_ctl_ID['full']['Levels_1to2']['Level_2']== treatment_condition_tmp]['conf_upper'], 
                                alpha=0.5, 
                                color=pred_item_ctl_ID['full']['Levels_1to2'].loc[pred_item_ctl_ID['full']['Levels_1to2']['Level_2']== treatment_condition_tmp]['color'])
                            
                            ## control condition ##
                            # Plot predictive mean for the control conditions
                            ax_comp.plot(
                                pred_item_ctl_ID['full']['Levels_1to2'].loc[pred_item_ctl_ID['full']['Levels_1to2']['Level_2']== ctl_ID]['x'], 
                                pred_item_ctl_ID['full']['Levels_1to2'].loc[pred_item_ctl_ID['full']['Levels_1to2']['Level_2']== ctl_ID]['y'], 
                                color=pred_item_ctl_ID['full']['Levels_1to2'].loc[pred_item_ctl_ID['full']['Levels_1to2']['Level_2']== ctl_ID]['color'].unique().item())
                            # Shade between the lower and upper confidence bounds for the control conditions
                            ax_comp.fill_between(
                                pred_item_ctl_ID['full']['Levels_1to2'].loc[pred_item_ctl_ID['full']['Levels_1to2']['Level_2']== ctl_ID]['x'], 
                                pred_item_ctl_ID['full']['Levels_1to2'].loc[pred_item_ctl_ID['full']['Levels_1to2']['Level_2']== ctl_ID]['conf_lower'],
                                pred_item_ctl_ID['full']['Levels_1to2'].loc[pred_item_ctl_ID['full']['Levels_1to2']['Level_2']== ctl_ID]['conf_upper'], 
                                alpha=0.5, 
                                color=pred_item_ctl_ID['full']['Levels_1to2'].loc[pred_item_ctl_ID['full']['Levels_1to2']['Level_2']== ctl_ID]['color'])

                            ## joint condition ##
                            # Plot predictive mean for the joint conditions
                            ax_comp.plot(
                                pred_item_ctl_ID['joint']['Levels_1to2'].loc[pred_item_ctl_ID['joint']['Levels_1to2']['treatment_condition']== treatment_condition_tmp]['x'], 
                                pred_item_ctl_ID['joint']['Levels_1to2'].loc[pred_item_ctl_ID['joint']['Levels_1to2']['treatment_condition']== treatment_condition_tmp]['y'], 
                                color=pred_item_ctl_ID['joint']['Levels_1to2'].loc[pred_item_ctl_ID['joint']['Levels_1to2']['treatment_condition']== treatment_condition_tmp]['color'].unique().item())
                            # Shade between the lower and upper confidence bounds for the joint conditions
                            ax_comp.fill_between(
                                pred_item_ctl_ID['joint']['Levels_1to2'].loc[pred_item_ctl_ID['joint']['Levels_1to2']['treatment_condition']== treatment_condition_tmp]['x'], 
                                pred_item_ctl_ID['joint']['Levels_1to2'].loc[pred_item_ctl_ID['joint']['Levels_1to2']['treatment_condition']== treatment_condition_tmp]['conf_lower'],
                                pred_item_ctl_ID['joint']['Levels_1to2'].loc[pred_item_ctl_ID['joint']['Levels_1to2']['treatment_condition']== treatment_condition_tmp]['conf_upper'], 
                                alpha=0.5, 
                                color=pred_item_ctl_ID['joint']['Levels_1to2'].loc[pred_item_ctl_ID['joint']['Levels_1to2']['treatment_condition'] == treatment_condition_tmp]['color'])
                            

                            # add the real data point
                            mscatter(
                                data_ctl_ID.loc[data_ctl_ID['Level_2'].isin([ctl_ID,treatment_condition_tmp ])]['x'],
                                data_ctl_ID.loc[data_ctl_ID['Level_2'].isin([ctl_ID,treatment_condition_tmp ])]['y'], 
                                c='k',  
                                m=data_ctl_ID.loc[data_ctl_ID['Level_2'].isin([ctl_ID,treatment_condition_tmp ])]['markers'], 
                                ax=ax_comp)
                    
                            #ax_comp.set_ylim([ymin-0.2, ymax+0.2])
                            ax_comp.set_xlabel('Temperature [°C]')
                            ax_comp.set_ylabel('Scaled intensity')
                            ax_comp.label_outer()
                    
                    pdf_comp.savefig(fig_comp)
                    plt.close(fig_comp)




                    


