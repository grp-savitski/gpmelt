#! /usr/bin/env python3

from matplotlib import pyplot as plt
import textwrap

###########################################
########   add_dynamic_text_page   ########
###########################################

####### fully written by ChatGPT4 #######
# used to add text in the mulitpages pdf while ensuring the text is not cut.
# this function aims to automatically adjust text properties like font size based on the amount of text. This requires dynamically calculating the space needed and adjusting font sizes or margins.

import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import textwrap

def add_text_page(pdf, text, fig_width=8.27, fig_height=11.69, margin=0.2, initial_font_size=12):
    """
    Adds a page of text to the PDF, ensuring the text fits within the page.
    Text is centered vertically and horizontally, with adjustable margins and initial font size.

    Args:
    - pdf: PdfPages object, the PDF file being written to.
    - text: str, the long string of text to add.
    - fig_width: float, the width of the figure (page) in inches. Default is A4 width.
    - fig_height: float, the height of the figure (page) in inches. Default is A4 height.
    - margin: float, margin size in inches.
    - initial_font_size: int, starting font size for text.
    """
    # Create a figure with specified dimensions
    fig, ax = plt.subplots(figsize=(fig_width, fig_height))
    ax.axis('off')  # Hide axes
    fig.patch.set_visible(False)  # Hide the figure patch

    # Text properties
    font_size = initial_font_size
    wrap_width = int((fig_width - 2 * margin) / (font_size * 0.013))  # Approximate wrap width based on font size

    # Wrap text to fit the width calculated from font size
    wrapped_text = textwrap.fill(text, width=wrap_width)

    # Try to fit text into the available vertical space
    text_height = len(wrapped_text.split('\n')) * font_size * 0.013  # Estimate text block height
    while text_height > (fig_height - 2 * margin) and font_size > 8:  # Minimum font size to maintain readability
        font_size -= 1
        wrap_width = int((fig_width - 2 * margin) / (font_size * 0.013))
        wrapped_text = textwrap.fill(text, width=wrap_width)
        text_height = len(wrapped_text.split('\n')) * font_size * 0.013

    # Add text to the figure, centered
    ax.text(0.5, 0.5, wrapped_text, transform=ax.transAxes, ha='center', va='center', fontsize=font_size)

    # Save the page
    pdf.savefig(fig)
    plt.close(fig)

''' 
# Example usage:
with PdfPages('multiple_figures.pdf') as pdf:
    long_text = "This is a very long text that needs to be wrapped appropriately to fit into a single page of a PDF document. The text includes multiple lines and descriptions which need careful handling to avoid being cut off. Make sure the text is not too long to fit on one page, otherwise consider using multiple pages or a smaller font."
    add_text_page(pdf, long_text)
'''


## exmaple:
#with PdfPages('multiple_figures.pdf') as pdf:
#    add_dynamic_text_page(pdf, long_text)
