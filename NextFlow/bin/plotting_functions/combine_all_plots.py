#! /usr/bin/env python3

from matplotlib.backends.backend_pdf import PdfPages
from matplotlib import pyplot as plt

import logging

from plotting_functions.adjust_text_size import add_text_page
from plotting_functions.plot_fitting_convergence import PlotFitConvergence

def combine_all_plots(pdf_results,pdf_name,  debug=True):

    if debug:
        logging.basicConfig(filename='debug.log', level=logging.DEBUG, format='%(message)s')
        logging.debug("##############################")
        logging.debug("Entering combine_all_plots")

    with PdfPages(pdf_name) as pdf:
        
        ###################################################
        # print the convergence plot
        ###################################################
        add_text_page(pdf, "Convergence plot for the full HGP model fitting")
        LossValues = pdf_results['full_hgpm_fit_loss_values_df']['LossValue'].tolist()
        
        _ , fig, _ = PlotFitConvergence(LossValues, pdf_results['training_iterations'], pdf_results['LearningRate'], pdf_results['Amsgrad'])
        pdf.savefig(fig)

        ###################################################
        # print the full_hgpm_covariance_matrix
        ###################################################
        add_text_page(pdf, pdf_results['full_hgpm_covariance_matrix_introduction_text'])

        for sub_fig in pdf_results['full_hgpm_covariance_matrix_pdf_content']:
            pdf.savefig(sub_fig)
            plt.close(sub_fig)
        
        ###################################################
        # print the joint_hgpm_covariance_matrix
        ###################################################
        add_text_page(pdf, pdf_results['joint_hgpm_covariance_matrix_introduction_text'])

        for sub_fig in pdf_results['joint_hgpm_covariance_matrix_pdf_content']:
            pdf.savefig(sub_fig)
            plt.close(sub_fig)

        if debug:
            logging.debug(f"length of full_hgpm_individual_predictions_introduction_text: {len(pdf_results['full_hgpm_individual_predictions_introduction_text'])}")
            logging.debug(f"Content of full_hgpm_individual_predictions_introduction_text: {pdf_results['full_hgpm_individual_predictions_introduction_text']}")
        
        ''' 
        # print the individual prediction of the full hgpm
        if len(pdf_results['full_hgpm_individual_predictions_introduction_text']) == 1:
            add_text_page(pdf, pdf_results['full_hgpm_individual_predictions_introduction_text'][0])
        else:
            raise NotImplementedError(f"full_hgpm_individual_predictions_introduction_text has more than one value!")

        for sub_fig in pdf_results['full_hgpm_individual_predictions_pdf_content']:
            pdf.savefig(sub_fig)
            plt.close(sub_fig)
        '''

        ###################################################
        # print the comparison of the full and joint predictions
        ###################################################
        if debug:
            logging.debug(f"length of full_and_joint_hgpm_predictions_comparison_introduction_text: {len(pdf_results['full_and_joint_hgpm_predictions_comparison_introduction_text'])}")
            logging.debug(f"Content of full_and_joint_hgpm_predictions_comparison_introduction_text: {pdf_results['full_and_joint_hgpm_predictions_comparison_introduction_text']}")

        if len(pdf_results['full_and_joint_hgpm_predictions_comparison_introduction_text']) == 1:
            add_text_page(pdf, pdf_results['full_and_joint_hgpm_predictions_comparison_introduction_text'][0])
        else:
            raise NotImplementedError(f"full_and_joint_hgpm_predictions_comparison_introduction_text has more than one value!")

        for sub_fig in pdf_results['full_and_joint_hgpm_predictions_comparison_pdf_content']:
            pdf.savefig(sub_fig)
            plt.close(sub_fig)

