#! /usr/bin/env python3

import matplotlib.markers as mmarkers
from matplotlib import pyplot as plt

###########################################
########## mscatter                ########
###########################################
# taken from https://github.com/matplotlib/matplotlib/issues/11155

def mscatter(x,y,ax=None, m=None, **kw):
    
    if not ax: ax=plt.gca()
    sc = ax.scatter(x,y,**kw)
    if (m is not None) and (len(m)==len(x)):
        paths = []
        for marker in m:
            if isinstance(marker, mmarkers.MarkerStyle):
                marker_obj = marker
            else:
                marker_obj = mmarkers.MarkerStyle(marker)
            path = marker_obj.get_path().transformed(
                        marker_obj.get_transform())
            paths.append(path)
        sc.set_paths(paths)
    return sc