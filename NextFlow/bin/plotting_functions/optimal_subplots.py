#! /usr/bin/env python3

import math
import matplotlib.pyplot as plt

####### fully written by ChatGPT4 #######
# these functions aim to
# 1) minimizing the difference between the number of rows and columns, 
# 2) while considering the aspect ratio and readability of the resulting subplots.  Ideally, keep the aspect ratio close to that of common screen or paper formats (e.g., 4:3, 16:9). This helps in maintaining clarity when viewed on different devices or printed.
# 3) Minimum Size: Each subplot should be at least 6 inches wide and 4 inches tall.
# 4) Figure Size: Adjust the overall figure size based on the number of rows and columns to ensure each subplot meets the minimum size requirement.


###########################################
########## optimal_subplot_dims    ########
###########################################
def optimal_subplot_dims(N):
    # Find the most square-like factorization of N
    # Square-like Layout: calculates columns as the rounded square root of NN and determines rows based on this value.
    # This approach tries to make the subplot layout more square-like, which often works better for readability and visual appeal, especially when dealing with arbitrary numbers of plots.
    cols = round(math.sqrt(N))
    rows = math.ceil(N / cols)
    return (rows, cols)

###########################################
########## create_subplot_figure    ########
###########################################
def create_subplot_figure(N, subplot_width=6, subplot_height=4):
    # Calculate optimal rows and columns
    rows, cols = optimal_subplot_dims(N)

    # Define the figure size based on the number of subplots and the minimum subplot size
    fig_width = cols * subplot_width
    fig_height = rows * subplot_height
    fig = plt.figure(figsize=(fig_width, fig_height))

    # Add subplots
    for i in range(1, N + 1):
        ax = fig.add_subplot(rows, cols, i)
        ax.plot([1, 2, 3], [i, i**2, i**3])  # Example plot
        ax.set_title(f'Subplot {i}')

    plt.tight_layout()
    plt.show()

# Example usage for N subplots
#create_subplot_figure(5)


# created from ChatGPT4 functions
###########################################
########## optimal_subplot_disposition  ########
###########################################
def optimal_subplot_disposition(N, subplot_width=6, subplot_height=4):
    # Find the most square-like factorization of N
    # Square-like Layout: calculates columns as the rounded square root of NN and determines rows based on this value.
    # This approach tries to make the subplot layout more square-like, which often works better for readability and visual appeal, especially when dealing with arbitrary numbers of plots.
    cols = round(math.sqrt(N))
    rows = math.ceil(N / cols)

    # Define the figure size based on the number of subplots and the minimum subplot size
    fig_width = cols * subplot_width
    fig_height = rows * subplot_height

    return rows, cols, fig_width, fig_height