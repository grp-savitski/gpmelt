#! /usr/bin/env python3

from pathlib import Path
from matplotlib import pyplot as plt
import re
import torch
import os
from matplotlib.colors import LogNorm

###########################################
########## Covariance matrix plots ########
###########################################

#plt.imshow(cov);plt.colorbar();plt.show()
def CovarianceMatrixPlot(submodel,plot_title, figure_title,modelType,  SavingPath,  SavePlot, ShowPlot, Min_CovarianceMatrix=None, Max_CovarianceMatrix=None, Min_IndexKernel=None, Max_IndexKernel=None):

    plt.ioff()  # Turn interactive mode off ucht hat the figure is not plotted unless specified

    #################### Create the path to the folder to save the matrices plots ####################
    if SavePlot:
        Path(SavingPath).mkdir(parents=True, exist_ok=True) 

    #################### save the training inputs als dictionnary ####################
    NLevels = len(submodel.train_inputs) -1

    TrainInput_dict = {}

    TrainInput_dict['TrainInput'] = submodel.train_inputs[0]
    for l in range(1,NLevels+1):
        TrainInput_dict['Level_{0}'.format(l)] = submodel.train_inputs[l]

    #################### define the figure ####################
    fig = plt.figure(figsize=(25, 15), constrained_layout=True)
    fig.suptitle("Covariance matrix decomposition for the HGPM on " + str(plot_title), weight='semibold', size='xx-large')

    #import pdb; pdb.set_trace()
    subfigs = fig.subfigures(2, 1)

    #### first row is the full covariance matrix and the RBF kernel(s) ####
    if re.search("TwoLengthscales", modelType):
        ax0 = subfigs[0].subplots(nrows=1, ncols=3)
    else:
        ax0 = subfigs[0].subplots(nrows=1, ncols=2)

    ## define the full covariance matrix ##
    #import pdb; pdb.set_trace()
    if NLevels == 3:
        FullCovMat = submodel.covar_matrix(x = TrainInput_dict['TrainInput'], i_level1 = TrainInput_dict['Level_1'], i_level2 = TrainInput_dict['Level_2'],i_level3 = TrainInput_dict['Level_3']).evaluate().detach()
    elif NLevels == 4:
        FullCovMat = submodel.covar_matrix(x = TrainInput_dict['TrainInput'], i_level1 = TrainInput_dict['Level_1'], i_level2 = TrainInput_dict['Level_2'],i_level3 = TrainInput_dict['Level_3'],i_level4 = TrainInput_dict['Level_4']).evaluate().detach()

    MinList = [torch.min(FullCovMat)]
    MaxList = [torch.max(FullCovMat)]

    ## RBF kernel of the first levels (plotted in any cases) ##
    RBFKernel_dict = {}
    RBFKernel_dict['RBFKernel'] = submodel.covar_module(TrainInput_dict['TrainInput'].unique()).evaluate().detach()

    MinList.append(torch.min(RBFKernel_dict['RBFKernel']))
    MaxList.append(torch.max(RBFKernel_dict['RBFKernel']))

    ## RBF kernel of the last level if 2 lengthscales ##
    if re.search("TwoLengthscales", modelType):
        RBFKernel_dict['RBFKernel_LastLevel'] = submodel.covar_module_LastLevel(TrainInput_dict['TrainInput'].unique()).evaluate().detach()
        MinList.append(torch.min(RBFKernel_dict['RBFKernel_LastLevel']))
        MaxList.append(torch.max(RBFKernel_dict['RBFKernel_LastLevel']))

    if Min_CovarianceMatrix is None:
        Min_CovarianceMatrix = min(MinList)
    if Max_CovarianceMatrix is None:
        Max_CovarianceMatrix = max(MaxList)

    im1 = ax0[0].imshow(FullCovMat, aspect='auto', cmap='viridis',vmin=Min_CovarianceMatrix, vmax=Max_CovarianceMatrix)
    ax0[0].set_title("Full covariance matrix")
   
    im2 = ax0[1].imshow(RBFKernel_dict['RBFKernel'],  aspect='auto', cmap='viridis',vmin=Min_CovarianceMatrix, vmax=Max_CovarianceMatrix)

    if re.search("TwoLengthscales", modelType):
        im3 = ax0[2].imshow(RBFKernel_dict['RBFKernel_LastLevel'],  aspect='auto', cmap='viridis',vmin=Min_CovarianceMatrix, vmax=Max_CovarianceMatrix)
        ax0[2].set_title("RBF Kernel Level " + str(NLevels))
        ax0[1].set_title("RBF Kernel Levels 1 to " + str(NLevels-1))

        for t in subfigs[0].colorbar(im3).ax.get_yticklabels():
            t.set_fontsize(20)
    else:
        ax0[1].set_title("RBF Kernel Levels 1 to " + str(NLevels))

        for t in subfigs[0].colorbar(im2).ax.get_yticklabels():
            t.set_fontsize(20)


    #### second row present each index kernel matrix ####
    
    #import pdb; pdb.set_trace()
    # set of all index kernels
    IndexKernel_dict = {}
    MinList_IndexKernel = []
    MaxList_IndexKernel = []

    for l in range(1,NLevels+1):
        # get the index kernel
        tmp = getattr(submodel, 'task_covar_module_level'+ str(l))
        #tmp = tmp.covar_matrix.evaluate().detach()
        tmp = tmp(TrainInput_dict['Level_{0}'.format(l)]).evaluate().detach()
        # replace the 0 by -1 to better see the pattern of the matrix
        tmp[tmp == 0 ] = -1 

        # compute the min and max
        if not torch.all(tmp == -1):
            MinList_IndexKernel.append(torch.min(tmp[tmp>0]))
            MaxList_IndexKernel.append(torch.max(tmp[tmp>0]))
        # store the index kernel
        IndexKernel_dict['Level_{0}'.format(l)] = tmp

    # if the user did not a-priori specify the color range for the kernel, recompute it
    if Min_IndexKernel is None: 
        Min_IndexKernel = min(MinList_IndexKernel)
    if Max_IndexKernel is None: 
        Max_IndexKernel =  max(MaxList_IndexKernel)

    # plot the index kernels
    ax1 = subfigs[1].subplots(nrows=1, ncols=NLevels)
    subfigs[1].suptitle('Index kernels', weight='bold')

    im_indexKernel = []
    for l in range(1,NLevels+1):
        im_indexKernel.append( ax1[l-1].imshow(IndexKernel_dict['Level_{0}'.format(l)], extent=(-5,5,-5,5), aspect='auto', cmap='viridis',norm=LogNorm(vmin=Min_IndexKernel, vmax=Max_IndexKernel)))
        ax1[l-1].set_title("Level " + str(l))
        ax1[l-1].set_xticks([])
        ax1[l-1].set_yticks([])

        if l == NLevels:
            for t in subfigs[1].colorbar(im_indexKernel[-1]).ax.get_yticklabels():
                t.set_fontsize(20)

    if SavePlot:
        fig.savefig(os.path.join(SavingPath, str(figure_title) + '.pdf') )

    if ShowPlot:          
        plt.show()
    
    return fig, Min_CovarianceMatrix, Max_CovarianceMatrix, Min_IndexKernel, Max_IndexKernel


