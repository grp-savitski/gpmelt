#! /usr/bin/env python3

from pathlib import Path
from matplotlib import pyplot as plt
import os
import pandas as pd


####################################################################
#    PlotFitConvergence
####################################################################
def PlotFitConvergence(LossValues, training_iterations, LearningRate, Amsgrad):

    f, ax = plt.subplots(1, 1, figsize=(25, 10))
    ax.plot(range(training_iterations), LossValues, 'k*')
    f.suptitle("Loss using Adams + lr = " + str(LearningRate) + " + amsgrad = " + str(Amsgrad), weight='semibold', size='xx-large')

    title = "LR" + str(LearningRate) + "_amsgrad" + str(Amsgrad) + '.pdf'

    df_LossValues = pd.DataFrame({'LossValue' : LossValues}) 
    

    return df_LossValues, f, title

    