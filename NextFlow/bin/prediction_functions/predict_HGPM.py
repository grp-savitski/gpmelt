#! /usr/bin/env python3
import os
import pandas as pd
import torch
import gpytorch
import numpy as np
import re
import logging
import copy
from pathlib import Path
#import json

from copy import deepcopy

from utils.retrieveNlevels_FromModelParam import RetrieveNLevels_fromModelParam
from fitting_functions.define_fullHGPM import Define_fullHGPM
from utils.Check_modelTypeValidity import Check_modelTypeValidity
from plotting_functions.plot_covarianceMatrix import CovarianceMatrixPlot
from utils.unnest_tuple import unnest_tuple
from plotting_functions.adjust_text_size import add_text_page

from matplotlib.backends.backend_pdf import PdfPages
from matplotlib import pyplot as plt

pd.options.mode.copy_on_write = True


def return_considered_levels(NLevels):

    if NLevels > 1 :
        return "Levels_1to{0}".format(NLevels)
    elif NLevels == 1 :
        return "Level_1"
    else:
        raise ValueError(f"The number of levels in the hierarchical model should be at least 1")

####################################################################
#    predict_full_HGPM
####################################################################

def predict_full_HGPM(
    data, 
    parameters, 
    full_hgpm_state_dict_list,
    test_x, 
    debug=True):

    if debug:
        logging.basicConfig(filename='debug.log', level=logging.DEBUG, format='%(message)s')
        logging.debug("##############################")
        logging.debug("Entering predict_full_HGPM")

    type_pred = parameters['prediction_type']

    if not type_pred in  ['predicted_functions', 'predicted_observations']:
        raise ValueError(f"the prediction type (either posterior predictive distribution using predicted_observations, or model posterior distribution using predicted_functions) should be correctly specified.")

    test_x = test_x.unique()

    ###############################################################################################################
    ### Define the number of levels in the hierarchical model and check the presence of the appropriate columns ###
    ###############################################################################################################
    NLevels, dfColnames_Levels = Check_modelTypeValidity(data, parameters['modelType'])

    ##############################################################################################
    ### Define the full HGPM, i.e the one in which each condition is modeled independently  ###
    ##############################################################################################
    model_list, likelihood_list, Index_df = Define_fullHGPM(data, parameters['modelType'],  parameters['mean'], parameters['lengthscale_prior'], parameters['lengthscale_MinConstraint'])

    #################################################
    ### Load the parameters of the trained model  ###
    #################################################
    for submodels, sub_state_dict in zip(model_list.models, full_hgpm_state_dict_list):
        submodels.load_state_dict(sub_state_dict)
    ################################################################
    ### Check that the correct number of models have been defined ##
    ################################################################
    ID2test = data['Level_1'].unique()
    if not len(ID2test) == len(model_list.models):
        raise ValueError(f"Problem with the number of models defined.")

    #######################################################
    # variables to save the predictions
    #######################################################
    pred_full_model = {}
    pred_full_model["Level_1"] = {}
    for l in range(2, NLevels+1):
        pred_full_model["Levels_1to{0}".format(l)] = {}
    
    if debug:
        logging.debug("Checking keys of pred_full_model")
        for key in pred_full_model:
            logging.debug(key)

    # this level is the same for all IDs and models
    test_i_Level_1 = torch.full((test_x.shape[0],1), dtype=torch.long, fill_value=0) 

    # iterate over the models
    for level1, submodel, sublik in zip(ID2test, model_list.models, likelihood_list.likelihoods):

        # Create data for the  PDF file in which all plots will be saved
        pdf_filename = 'covariance_matrices_for_' + type_pred + '_full_HGPM_' + str(level1) +'.pdf'
        pdf_introduction_text  = 'The following plots represent the covariance matrix structure used to obtained the prediction from the GP fit of the full HGP model. \nBecause we predict each level at a time, the same number of pages as levels in the HGP models should be obtained. \nWe encourage you to check these matrices to ensure that the expected model is being predicted.'
        pdf_content = []
        ### start ###
        Index_df_allLevels = Index_df.copy(deep=True)
        Index_df_allLevels = Index_df_allLevels.loc[Index_df_allLevels['Level_1']==level1]
        Index_df_tmp = Index_df_allLevels.copy(deep=True)
        Index_df_tmp['Index_Level_1'] = 0
    
        # indicate how many levels does the "currently being predicted model" has
        NLevels_tmp = NLevels
        
        #######################################################
        # Prepare required data for succesive predictions
        #######################################################
        full_model_parameters_tuple= [(raw_param_name, len(param)) for  raw_param_name, param in submodel.named_parameters()]
    
        #######################################################
        # loop for predictions
        #######################################################
        # To predict the different levels of the full model, we will successively set to 0 the corresponding outputscale and re-predict
        while NLevels_tmp > 0:
            
            # indicate how many levels does the "currently being predicted model" has
            considered_levels = return_considered_levels(NLevels_tmp)
            
            ## empty list to store the index-levels
            #Index_df_tmp_to_merge = {}
            # empty dict to store the prediction of these levels
            # pred_full_model[considered_levels] = {}

            submodel.eval()
            sublik.eval()

            # save the coavarince matrix of the model that will be predicted. 
            # The name of the plot will be "{level1}_fullModel_Levels_1to{0}".format(NLevels_tmp).pdf.
            # SavingPath is set to '', to be saved directly in the right directory using nextflow.
            # to keep the plots consistant across levels, I use the color arange of the HGPM with all levels
            title_covariance_matrix_plot = level1 + "; full model \n HGPM with " + considered_levels
            figname_covariance_matrix_plot = level1 + "_fullModel_" + considered_levels
                    
            if NLevels_tmp == NLevels: 
                fig, Min_CovarianceMatrix_fullHGPM, Max_CovarianceMatrix_fullHGPM, Min_IndexKernel_fullHGPM, Max_IndexKernel_fullHGPM = CovarianceMatrixPlot(submodel, plot_title = title_covariance_matrix_plot, figure_title = figname_covariance_matrix_plot, modelType = parameters['modelType'],  SavingPath='',  SavePlot=False, ShowPlot=False)
            else:
                fig, *_ = CovarianceMatrixPlot(submodel, plot_title = title_covariance_matrix_plot, figure_title = figname_covariance_matrix_plot, modelType = parameters['modelType'],  SavingPath='',  SavePlot=False, ShowPlot=False, Min_IndexKernel=Min_IndexKernel_fullHGPM, Min_CovarianceMatrix=Min_CovarianceMatrix_fullHGPM, Max_CovarianceMatrix=Max_CovarianceMatrix_fullHGPM, Max_IndexKernel=Max_IndexKernel_fullHGPM)
            
            pdf_content.append(fig)
            #pdf.savefig(fig)
            plt.close(fig)

            #######################################################
            # prepare the list where the prediction will be saved
            #######################################################
            predictions_mean = []
            predictions_conf_lower = []
            predictions_conf_upper = []
            Npred = 0

            with torch.no_grad(), gpytorch.settings.fast_pred_var():

                # iterate over the modeled tasks, i.e. all existing combinations of index levels.
                for i in range(Index_df_tmp.shape[0]):

                    #######################################################
                    # Define the combinations of index levels to be predicted
                    #######################################################
                    test_i = {}
                    test_i['Level_1'] = test_i_Level_1
                    for l in range(2, NLevels_tmp+1):
                        test_i["Level_{0}".format(l)] = torch.full((test_x.shape[0],1), dtype=torch.long, fill_value=Index_df_tmp.iloc[i]["Index_Level_{0}".format(l)])
                    # complete with index 0 for the HGPM with less levels 
                    # as we set the outputscales for the lower level to 0, the indices for these levels should not matter
                    if NLevels_tmp < NLevels:
                        for l in range(NLevels_tmp+1, NLevels+1):
                            test_i["Level_{0}".format(l)] = torch.full((test_x.shape[0],1), dtype=torch.long, fill_value=0) 
                        
                    #######################################################
                    # Predict
                    ## Important note: from https://docs.gpytorch.ai/en/stable/examples/01_Exact_GPs/Simple_GP_Regression.html ##
                    # submodel(*test_tuple): the model posterior distribution p(f* | x*, X, y), for training data X, y. This posterior is the distribution over the function we are trying to model, and thus quantifies our model uncertainty.
                    # sublik(submodel(*test_tuple)): the posterior predictive distribution p(y* | x*, X, y) which is the probability distribution over the predicted output value => here the prediction is over the observed value of the test point.
                    # Similarly as one would plot the sigmoid fit alone, we also want to fit the "GP fit" alone, i.e the distribution over the functions $f$ such that $Y = f + \epsilon$.
                    #######################################################
                    # transform the index_level dictionary into a tuple of torch.tensor
                    test_i_tuple = tuple(test_i.values())
                    test_tuple = unnest_tuple((torch.unsqueeze(test_x, 1), test_i_tuple))
                                            
                    if type_pred == 'predicted_functions':
                        
                        f_pred = submodel(*test_tuple)

                        predictions_mean.append(f_pred.mean.detach().numpy() ) 
                        predictions_conf_lower.append(f_pred.confidence_region()[0].detach().numpy())
                        predictions_conf_upper.append(f_pred.confidence_region()[1].detach().numpy()) 

                    elif type_pred == 'predicted_observations':
                    
                        observed_pred = sublik(submodel(*test_tuple))

                        predictions_mean.append(observed_pred.mean.detach().numpy() ) 
                        predictions_conf_lower.append(observed_pred.confidence_region()[0].detach().numpy())
                        predictions_conf_upper.append(observed_pred.confidence_region()[1].detach().numpy()) 

                    Npred += 1

                ############################################################################################
                # Combine the predictions of the different tasks of this level & add the index_levels & save
                ############################################################################################

                # contains the index_level variables
                Index_df_tmp_to_merge = Index_df_tmp.loc[np.repeat(Index_df_tmp.index, test_x.shape[0])].reset_index(drop=True)

                df = pd.DataFrame({'model' : np.repeat('full', Index_df_tmp_to_merge.shape[0] ), 
                    'predicted_levels' : np.repeat(considered_levels, Index_df_tmp_to_merge.shape[0] ), 
                    'x' : np.tile(test_x, Npred),
                    'y' :  np.array(predictions_mean).flatten(),
                    'conf_lower': np.array(predictions_conf_lower).flatten(),
                    'conf_upper': np.array(predictions_conf_upper).flatten()})
                
                df = pd.concat([df,Index_df_tmp_to_merge ], axis =1)

                ColumnsOrder = ['model', 'predicted_levels'] + ["Level_{0}".format(l) for l in range(1, NLevels_tmp+1)] + ["Index_Level_{0}".format(l) for l in range(1, NLevels_tmp+1)] +  ['x', 'y', 'conf_lower', 'conf_upper']
                ColumnsOrder = [x for x in ColumnsOrder if x]
                df = df[ColumnsOrder]
                
                # save the predictions for this ID/level1, and this level of the hierarchy 
                pred_full_model[considered_levels][level1] = df


            ############################################################################################
            #  remove the bottom level of the "currently being predicted model"
            ############################################################################################
            # Filter columns based on the pattern
            columns_to_keep = [col for col in Index_df_tmp.columns if r"Level_{0}".format(NLevels_tmp) not in col]

            # filter the corresponding columns
            Index_df_tmp = Index_df_tmp[columns_to_keep].drop_duplicates()
            
            if debug:
                logging.debug("checking Index_df_tmp")
                logging.debug(Index_df_tmp)

            # set to 0 the corresponding outputscale parameter(s)
            # outputscale_bottom_level[0][1] : indicates the number of values in the outputscale parameter; 
            # re.sub(r'raw_', '',outputscale_bottom_level[0][0]) remove the "raw" component of the parameter name
            outputscale_bottom_level = [ t for t in full_model_parameters_tuple if re.search(r"task_covar_module_level{0}".format(NLevels_tmp), t[0])]
            if len(outputscale_bottom_level) > 1 :
                raise ValueError(f"Only one parameter is expected to match the pattern here!")
                
            submodel.initialize(**{re.sub(r'raw_', '',outputscale_bottom_level[0][0]): torch.zeros(outputscale_bottom_level[0][1])}) 

            # the "next model to be predicted" has one less level
            NLevels_tmp = NLevels_tmp -1

            if debug:
                logging.debug("checking the number of levels")
                logging.debug(NLevels_tmp)
                logging.debug('likelihood.noise_covar.noise ')
                logging.debug(submodel.likelihood.noise_covar.noise)
                logging.debug('\n task_covar_module_level1.var_cnst')
                logging.debug(submodel.task_covar_module_level1.var_cnst)
                logging.debug('\n task_covar_module_level2.var_cnst')
                logging.debug(submodel.task_covar_module_level2.var_cnst)
                logging.debug('\n covar_module.lengthscale')
                logging.debug(submodel.covar_module.lengthscale)
                logging.debug('\n task_covar_module_level3.var')
                logging.debug(submodel.task_covar_module_level3.var)

    pred_full_model_df = {}

    for key in pred_full_model:
        pred_full_model_df[key] = pd.concat(pred_full_model[key], ignore_index=True)
        
        # only save teh indivudal predictions if explicitely required
        if parameters['save_individual_results']:
            pred_full_model_df[key].to_csv(index = True, path_or_buf = os.path.join( type_pred + "_full_model_" + key + ".csv" ))
    
    results_dict = {
        'prediction_full_hgpm_dict' : pred_full_model_df, 
        'full_hgpm_covariance_matrix_pdf_filename' : pdf_filename,
        'full_hgpm_covariance_matrix_introduction_text' : pdf_introduction_text,
        'full_hgpm_covariance_matrix_pdf_content' : pdf_content
    }   

    if debug:
        logging.debug("##############################")
    
    return results_dict 
            

    
'''
for  raw_param_name, param in submodel.named_parameters(): print(raw_param_name);print(param)

print(submodel.likelihood.noise_covar.noise);
print(submodel.task_covar_module_level1.var_cnst);
print(submodel.task_covar_module_level2.var_cnst);
print(submodel.covar_module.lengthscale);
print(submodel.task_covar_module_level3.var)

'''


####################################################################
#    predict_joint_HGPM
####################################################################

def predict_joint_HGPM(
    data, 
    parameters, 
    full_hgpm_state_dict_list,
    test_x,
    sampling,
    nb_samples_df,
    debug=True):

    r'''
    will return a data frame per prediction, such that
        * Level_1 = the "true" ID
        * Level_1_ForNullHyp = Level_1 + "-" + treatment_condition + "-" + control_condition 
        * comparison_id = treatment_condition + "-" + control_condition 
    
    '''
    if debug:
        logging.basicConfig(filename='debug.log', level=logging.DEBUG, format='%(message)s')
        logging.debug("##############################")
        logging.debug("Entering predict_joint_HGPM")

    type_pred = parameters['prediction_type']

    if not type_pred in  ['predicted_functions', 'predicted_observations']:
        raise ValueError(f"the prediction type (either posterior predictive distribution using predicted_observations, or model posterior distribution using predicted_functions) should be correctly specified.")

    test_x = test_x.unique()

    ###############################################################################################################
    ### Define the number of levels in the hierarchical model and check the presence of the appropriate columns ###
    ###############################################################################################################
    NLevels, dfColnames_Levels = Check_modelTypeValidity(data, parameters['modelType'])

    ##############################################################################################
    ### Define the full HGPM, i.e the one in which each condition is modeled independently  ###
    ##############################################################################################
    model_list, likelihood_list, Index_df = Define_fullHGPM(data, parameters['modelType'],  parameters['mean'], parameters['lengthscale_prior'], parameters['lengthscale_MinConstraint'])

    #################################################
    ### Load the parameters of the trained model  ###
    #################################################
    for submodels, sub_state_dict in zip(model_list.models, full_hgpm_state_dict_list):
        submodels.load_state_dict(sub_state_dict)

    ##################################################################################################
    ########################## Create the joint models from the full models ##########################  
    # The idea is to modify the training inputs of the full model to match the ones of a joint model
    ##################################################################################################

    ID2test = data['Level_1'].unique()
    if not len(ID2test) == len(model_list.models):
        raise ValueError(f"Problem with the number of models defined.")

    # this level is the same for all IDs and models
    test_i_Level_1 = torch.full((test_x.shape[0],1), dtype=torch.long, fill_value=0) 

    #### variables to save the outputs ###
    # to save the prediction
    pred_joint_model = {}
    pred_joint_model["Level_1"] = {}
    for l in range(2, NLevels+1):
        pred_joint_model["Levels_1to{0}".format(l)] = {}

    # to save the indices
    Index_df_joint_model = {}
    Index_df_joint_model["Level_1"] = {}

    # to save the mll
    level_1_mll = []
    comparison_id_mll = []
    control_condition_mll = []
    treatment_condition_mll = []
    value_mll = []

    # iterate over the full models
    for level1, submodel, sublik in zip(ID2test, model_list.models, likelihood_list.likelihoods):

        if sampling:
            # filter data to item of interest
            data_level1 = data[(data['Level_1'] == level1)].drop_duplicates()
            all_samples = []
            ## create the directories
            #dir_name_sampling_folder = parameters['directory_name_for_null_dataset_sampling']
            #Path(dir_name_sampling_folder).mkdir(parents=True, exist_ok=True) 
        
        # Create data for the  PDF file in which all plots will be saved
        pdf_filename = 'covariance_matrices_for_' + type_pred + '_joint_HGPM_' + str(level1) +'.pdf'
        pdf_introduction_text  = 'The following plots represent the covariance matrix structure used to obtained the prediction from the GP fit of the joint HGP model. \nBecause we predict each level at a time, the same number of pages as levels in the HGP models should be obtained. \nWe encourage you to check these matrices to ensure that the expected model is being predicted.'
        pdf_content = []
        
        ########## save the original parameters of the model ##########
        original_state_dict = deepcopy(submodel.state_dict()) # deep copy of the dictionary containing the parameters for our model
        
        if debug:
            logging.debug("original stat dict")
            logging.debug(original_state_dict)

        index_level_to_modify = 2 # in the curent GPMelt framework, we only change the index of the condition-level (i.e index of level_2) between the full and joint model
        original_train_inputs = deepcopy(submodel.train_inputs)

        # restrict the indices to the level1 of interest
        Index_df_allLevels = Index_df.copy(deep=True)
        Index_df_allLevels = Index_df_allLevels.loc[Index_df_allLevels['Level_1']==level1]
        Index_df_allLevels = Index_df_allLevels.rename(columns={"Level_2": "Level_2_ForAltHyp", "Index_Level_2":"Index_Level_2_ForAltHyp"})

        ## empty list to store the prediction of these levels for this ID across all comparisons
        for key in pred_joint_model:
            pred_joint_model[key][level1] = {}

        if not parameters["control_condition"] is None:
            control_condition = parameters["control_condition"]

            if control_condition not in Index_df_allLevels['Level_2_ForAltHyp'].values:
                raise ValueError(f"Control condition misspecified, got {control_condition}, but needs one of {Index_df_allLevels['Level_2_ForAltHyp'].unique()}.")
            
            Index_Level_2_control = Index_df_allLevels.loc[Index_df_allLevels['Level_2_ForAltHyp'] == control_condition]['Index_Level_2_ForAltHyp'].unique().item()
            Index_Level_2_treatment = [x for x in Index_df_allLevels['Index_Level_2_ForAltHyp'].unique() if x != Index_Level_2_control]

            for Comparison_idx in Index_Level_2_treatment:

                ### first we modify the Index_df such that the treatment condition corresponding to the index "Comparison_idx" is combined with the control condition
                Index_df_forthiscomparison = Index_df_allLevels.copy(deep=True)
                treatment_condition = Index_df_forthiscomparison.loc[Index_df_forthiscomparison['Index_Level_2_ForAltHyp']==Comparison_idx, "Level_2_ForAltHyp"].unique().item()
                Comparison_id = treatment_condition + "-" + control_condition
                # change the "Level_2" for the treatement condition and control condition of this comparison to "joint_condition", as they are now forming the "joint" condition.
                Index_df_forthiscomparison['Level_2_ForNullHyp'] = np.where( Index_df_forthiscomparison['Level_2_ForAltHyp'].isin([treatment_condition, control_condition]),'joint_condition' , Index_df_forthiscomparison['Level_2_ForAltHyp']) 
                #the index associated to this "joint_condition" should be unique, so we set the index of the treatement condition to the index of the control condition.
                Index_df_forthiscomparison['Index_Level_2_ForNullHyp'] = np.where(Index_df_forthiscomparison['Index_Level_2_ForAltHyp'] == Comparison_idx, Index_Level_2_control, Index_df_forthiscomparison['Index_Level_2_ForAltHyp']) 
                Index_df_forthiscomparison['Level_1_ForNullHyp'] = level1 + "-" + Comparison_id  
                Index_df_forthiscomparison['Index_Level_1'] = 0

                Index_df_joint_model["Level_1"][Comparison_id] = Index_df_forthiscomparison

                ###### Add a blank page in the pdf with the name of the comparison ####
                fig, ax = plt.subplots()
                ax.axis('off')  # Turn off the axis
                fig.patch.set_visible(False)  # Turn off the figure patch
                ax.text(0.5, 0.5,
                'Considering comparison:\n' + Comparison_id.split("-", 1)[0] + " vs " + Comparison_id.split("-", 1)[1], 
                transform=ax.transAxes, 
                        ha='center', va='center', fontsize=12)  # Add text in the middle of the page
                pdf_content.append(fig)
                plt.close(fig)

                
                ########## reload the original parameter of the model ##########
                submodel.load_state_dict(original_state_dict)
                
                if debug:
                    logging.debug("stat dict for comparison " + Comparison_id)
                    logging.debug(submodel.state_dict()) 
                    logging.debug("Cheking Index_df_forthiscomparison")
                    logging.debug(Index_df_forthiscomparison)
                    logging.debug("Index_Level_2_control")
                    logging.debug(Index_Level_2_control)
                    logging.debug("Index_Level_2_ttt")
                    logging.debug(Comparison_idx)
                    #logging.debug(Index_df_forthiscomparison["Index_Level_2_ForNullHyp"])
                    index_level_to_modify = 2 
                    modified_train_inputs = submodel.train_inputs[index_level_to_modify]
                    modified_train_inputs = torch.where(modified_train_inputs == Comparison_idx, torch.tensor(Index_Level_2_control), modified_train_inputs)
                    #logging.debug(modified_train_inputs)
                    modified_tuple = submodel.train_inputs[:index_level_to_modify] + (modified_train_inputs,) + submodel.train_inputs[index_level_to_modify+1:]
                    logging.debug("new submodel.train_inputs")
                    logging.debug(modified_tuple)
                

                ### update the input data to the model
                modified_train_inputs = deepcopy(original_train_inputs)
                modified_train_inputs = modified_train_inputs[index_level_to_modify]
                #modified_train_inputs = submodel.train_inputs[index_level_to_modify]
                modified_train_inputs = torch.where(modified_train_inputs == Comparison_idx, torch.tensor(Index_Level_2_control), modified_train_inputs)
                modified_tuple = original_train_inputs[:index_level_to_modify] + (modified_train_inputs,) + original_train_inputs[index_level_to_modify+1:]
                submodel.train_inputs = modified_tuple

                    
                if debug:
                    logging.debug("modified_train_inputs")
                    logging.debug(modified_train_inputs)
                    logging.debug(modified_tuple)

                #################### Compute the MLL for the obtained joint model ####################
                # now that we redefine the joint HGPM with all levels, we can compute the MLL
                mll = gpytorch.mlls.ExactMarginalLogLikelihood(sublik, submodel)
                output = submodel(*submodel.train_inputs)
                loss = -mll(output, submodel.train_targets)

                if debug:
                    logging.debug("Check loss variables for this comparison")
                    logging.debug(level1)
                    logging.debug(Comparison_id)
                    logging.debug(treatment_condition)
                    logging.debug(control_condition)
                    logging.debug("test1")
                    logging.debug(loss.detach().numpy())
                    logging.debug("test2")
                    logging.debug(loss.item())
                    

                level_1_mll.append(level1)
                comparison_id_mll.append(Comparison_id)
                treatment_condition_mll.append(treatment_condition)
                control_condition_mll.append(control_condition)
                value_mll.append(loss.item())
                
                #################### Sample from the joint model if required (sampling of true negative) ####################
                if sampling:
                    
                    if debug:
                        logging.debug("Enter sampling for comparison id: " +str(Comparison_id)) 
                        logging.debug("Cheking Index_df_forthiscomparison")
                        logging.debug(Index_df_forthiscomparison.columns)
                        logging.debug("Cheking data_level1")
                        logging.debug(data_level1.columns)
                        logging.debug("sampling from joint model for " + str(Comparison_id))

                    #merge the index_df of this comparison with the real data
                    dfColnames_Levels_2 = ['Level_2_ForAltHyp' if col == 'Level_2' else col for col in dfColnames_Levels]
                    Index_df_forthiscomparison_with_data = pd.merge(data_level1, Index_df_forthiscomparison.rename(columns={"Level_2_ForAltHyp":"Level_2", "Index_Level_2_ForAltHyp":"Index_Level_2"}), on = dfColnames_Levels)
                    ColumnsToKeep = ["Level_{0}".format(x) for x in range(1,(NLevels+1))] + [ 'condition' if set(['condition']).issubset(Index_df_forthiscomparison_with_data.columns) else ''] + [ 'Level_2_ForNullHyp' if set(['Level_2_ForNullHyp']).issubset(Index_df_forthiscomparison_with_data.columns) else '']+ [ 'Index_Level_2_ForNullHyp' if set(['Index_Level_2_ForNullHyp']).issubset(Index_df_forthiscomparison_with_data.columns) else ''] + ['x', 'rep']
                    ColumnsToKeep = [x for x in ColumnsToKeep if x]

                    samples_df = Index_df_forthiscomparison_with_data[ColumnsToKeep]
                    if debug:
                        logging.debug("Cheking samples_df")
                        logging.debug(samples_df.columns)
                    

                    # the samples are obtained at the same points than the observations
                    test_x_joint_model = submodel.train_inputs

                    with gpytorch.settings.prior_mode(True):
                        
                        submodel.eval()
                        sublik.eval()

                        prior_samples_generator_joint_model = sublik(submodel(*test_x_joint_model))

                        #import pdb; pdb.set_trace()    
                        nb_samples = nb_samples_df['NSamples'].unique()
                        if len(nb_samples) != 1: 
                            raise ValueError(f"Problem with the definition of the number of samples.")
                        
                        # Convert the single unique value to an integer
                        nb_samples_value = int(nb_samples[0])

                        for n_samples in range(0, nb_samples_value):
                            
                            samples_df_tmp = copy.deepcopy(samples_df)
                            samples_df_tmp.loc[:,('y')] = prior_samples_generator_joint_model.sample()
                            samples_df_tmp.loc[:,('Dataset')] = n_samples+1
                            samples_df_tmp.loc[:,('control_condition')] = treatment_condition
                            samples_df_tmp.loc[:,('Model')] = "joint"
                            samples_df_tmp.loc[:,('SamplingType')] = "prior_allLevels"
                            all_samples.append(samples_df_tmp)
                            
                #################### joint model prediction ####################
                ### here we enter a similar loop as in predict_full_HGPM, where we iterate over the level of this HGP model to predict it.
                
                # indicate how many levels does the "currently being predicted model" has
                NLevels_tmp = NLevels

                #######################################################
                # Prepare required data for succesive predictions
                #######################################################
                joint_model_parameters_tuple= [(raw_param_name, len(param)) for  raw_param_name, param in submodel.named_parameters()]

                Index_df_tmp = Index_df_forthiscomparison.copy(deep=True)
                Index_df_tmp['Index_Level_1'] = 0
                if NLevels_tmp > 1 :
                    Index_df_tmp = Index_df_tmp.drop(columns=['Level_2_ForAltHyp','Index_Level_2_ForAltHyp'])
                    Index_df_tmp = Index_df_tmp.rename(columns={"Level_2_ForNullHyp": "Level_2",  "Index_Level_2_ForNullHyp":"Index_Level_2"})
                    Index_df_tmp = Index_df_tmp.drop_duplicates()

                else:
                    # to maintain the coherance between all prediction data frames.
                    Index_df_tmp['Level_1_ForNullHyp'] = level1 + "-" + Comparison_id
                    Index_df_tmp = Index_df_tmp.drop_duplicates()

                #######################################################
                # loop for predictions
                #######################################################
                # To predict the different levels of the joint model, we will successively set to 0 the corresponding outputscale and re-predict
                while NLevels_tmp > 0:
                    
                    # indicate how many levels does the "currently being predicted model" has
                    considered_levels = return_considered_levels(NLevels_tmp)
                    

                    submodel.eval()
                    sublik.eval()

                    # save the covarince matrix of the model that will be predicted. 
                    # The name of the plot will be "{level1}_fullModel_Levels_1to{0}".format(NLevels_tmp).pdf.
                    # SavingPath is set to '', to be saved directly in the right directory using nextflow.
                    # to keep the plots consistant across levels, I use the color arange of the HGPM with all levels 
                    title_covariance_matrix_plot = level1 + "; joint model for comparison " + Comparison_id.split("-", 1)[0] + " vs " + Comparison_id.split("-", 1)[1] + "\n HGPM with " + considered_levels
                    figname_covariance_matrix_plot = level1 + "_jointModel_" + Comparison_id + considered_levels
                    
                    if NLevels_tmp == NLevels: 
                        if debug:
                            logging.debug("inside predict_joint_HGPM")
                            logging.debug(level1)
                            logging.debug(Comparison_id)
                            logging.debug(considered_levels)
                    
                        fig, Min_CovarianceMatrix_jointHGPM, Max_CovarianceMatrix_jointHGPM, Min_IndexKernel_jointHGPM, Max_IndexKernel_jointHGPM = CovarianceMatrixPlot(submodel, plot_title = title_covariance_matrix_plot, figure_title=figname_covariance_matrix_plot,  modelType = parameters['modelType'],  SavingPath='',  SavePlot=False, ShowPlot=False)
                    else:
                        fig, *_  =CovarianceMatrixPlot(submodel, plot_title = title_covariance_matrix_plot, figure_title=figname_covariance_matrix_plot,  modelType = parameters['modelType'],  SavingPath='',  SavePlot=False, ShowPlot=False, Min_IndexKernel = Min_IndexKernel_jointHGPM, Min_CovarianceMatrix = Min_CovarianceMatrix_jointHGPM, Max_CovarianceMatrix = Max_CovarianceMatrix_jointHGPM, Max_IndexKernel = Max_IndexKernel_jointHGPM)
                    
                    pdf_content.append(fig)
                    plt.close(fig)

                    #######################################################
                    # prepare the list where the prediction will be saved
                    #######################################################
                    predictions_mean = []
                    predictions_conf_lower = []
                    predictions_conf_upper = []
                    Npred = 0

                    with torch.no_grad(), gpytorch.settings.fast_pred_var():

                        # iterate over the modeled tasks, i.e. all existing combinations of index levels.
                        for i in range(Index_df_tmp.shape[0]):

                            #######################################################
                            # Define the combinations of index levels to be predicted
                            #######################################################
                            test_i = {}
                            test_i['Level_1'] = test_i_Level_1

                            ''' 
                            # in the curent GPMelt framework, we only change the index of the condition-level (i.e index of level_2) between the full and joint model
                            if NLevels_tmp > 1 :
                                test_i["Level_2"] = torch.full((test_x.shape[0],1), dtype=torch.long, fill_value=Index_df_tmp.iloc[i]["Index_Level_2_ForNullHyp"])'''

                            for l in range(2, NLevels_tmp+1):
                                # in the curent GPMelt framework, we only change the index of the condition-level (i.e index of level_2) between the full and joint model
                                test_i["Level_{0}".format(l)] = torch.full((test_x.shape[0],1), dtype=torch.long, fill_value=Index_df_tmp.iloc[i]["Index_Level_{0}".format(l)])

                            # complete with index 0 for the HGPM with less levels 
                            # as we set the outputscales for the lower level to 0, the indices for these levels should not matter
                            if NLevels_tmp < NLevels:
                                for l in range(NLevels_tmp+1, NLevels+1):
                                    test_i["Level_{0}".format(l)] = torch.full((test_x.shape[0],1), dtype=torch.long, fill_value=0) 
                                

                            #######################################################
                            # Predict
                            ## Important note: from https://docs.gpytorch.ai/en/stable/examples/01_Exact_GPs/Simple_GP_Regression.html ##
                            # submodel(*test_tuple): the model posterior distribution p(f* | x*, X, y), for training data X, y. This posterior is the distribution over the function we are trying to model, and thus quantifies our model uncertainty.
                            # sublike(submodel(*test_tuple)): the posterior predictive distribution p(y* | x*, X, y) which is the probability distribution over the predicted output value => here the prediction is over the observed value of the test point.
                            # Similarly as one would plot the sigmoid fit alone, we also want to fit the "GP fit" alone, i.e the distribution over the functions $f$ such that $Y = f + \epsilon$.
                            #######################################################
                        
                            # transform the index_level dictionary into a tuple of torch.tensor
                            test_i_tuple = tuple(test_i.values())
                            test_tuple = unnest_tuple((torch.unsqueeze(test_x, 1), test_i_tuple))
                            
                            if debug:
                                logging.debug(test_tuple)
                                logging.debug(submodel)
                            


                            if type_pred == 'predicted_functions':
                        
                                f_pred = submodel(*test_tuple)

                                predictions_mean.append(f_pred.mean.detach().numpy() ) 
                                predictions_conf_lower.append(f_pred.confidence_region()[0].detach().numpy())
                                predictions_conf_upper.append(f_pred.confidence_region()[1].detach().numpy()) 

                            elif type_pred == 'predicted_observations':
                            
                                observed_pred = submodel(*test_tuple)

                                predictions_mean.append(observed_pred.mean.detach().numpy() ) 
                                predictions_conf_lower.append(observed_pred.confidence_region()[0].detach().numpy())
                                predictions_conf_upper.append(observed_pred.confidence_region()[1].detach().numpy()) 


                            Npred += 1

                        ############################################################################################
                        # Combine the predictions of the different tasks of this level & add the index_levels & save
                        ############################################################################################

                        # contains the index_level variables
                        Index_df_tmp_to_merge = Index_df_tmp.loc[np.repeat(Index_df_tmp.index, test_x.shape[0])].reset_index(drop=True)
                        
                        df = pd.DataFrame({'model' : np.repeat('joint', Index_df_tmp_to_merge.shape[0] ), 
                            'comparison_id' : np.repeat(Comparison_id, Index_df_tmp_to_merge.shape[0] ), 
                            'treatment_condition' : np.repeat(treatment_condition, Index_df_tmp_to_merge.shape[0] ), 
                            'control_condition' : np.repeat(control_condition, Index_df_tmp_to_merge.shape[0] ), 
                            'predicted_levels' : np.repeat(considered_levels, Index_df_tmp_to_merge.shape[0] ), 
                            'x' : np.tile(test_x, Npred),
                            'y' :  np.array(predictions_mean).flatten(),
                            'conf_lower': np.array(predictions_conf_lower).flatten(),
                            'conf_upper': np.array(predictions_conf_upper).flatten()})
                        
                        df = pd.concat([df,Index_df_tmp_to_merge ], axis =1)
                        
                        ColumnsOrder = ['model', 'comparison_id', 'treatment_condition', 'control_condition','predicted_levels','Level_1', 'Level_1_ForNullHyp'] + ["Level_{0}".format(l) for l in range(2, NLevels_tmp+1)] + ["Index_Level_{0}".format(l) for l in range(1, NLevels_tmp+1)] +  ['x', 'y', 'conf_lower', 'conf_upper']
                        ColumnsOrder = [x for x in ColumnsOrder if x]
                        df = df[ColumnsOrder]
                        
                        if debug:
                            logging.debug("checking predictions df column names")
                            logging.debug(df.columns.tolist())
                        
                        # save the predictions for this ID/level1, and this level of the hierarchy 
                        pred_joint_model[considered_levels][level1][Comparison_id] = df

                    ############################################################################################
                    #  remove the bottom level of the "currently being predicted model"
                    ############################################################################################
                    # Filter columns based on the pattern
                    columns_to_keep = [col for col in Index_df_tmp.columns if r"Level_{0}".format(NLevels_tmp) not in col]

                    # filter the corresponding columns
                    Index_df_tmp = Index_df_tmp[columns_to_keep].drop_duplicates()

                    if debug:
                        logging.debug("checking Index_df_tmp")
                        logging.debug(Index_df_tmp)

                    # set to 0 the corresponding outputscale parameter(s)
                    # outputscale_bottom_level[0][1] : indicates the number of values in the outputscale parameter; 
                    # re.sub(r'raw_', '',outputscale_bottom_level[0][0]) remove the "raw" component of the parameter name
                    outputscale_bottom_level = [ t for t in joint_model_parameters_tuple if re.search(r"task_covar_module_level{0}".format(NLevels_tmp), t[0])]
                    if len(outputscale_bottom_level) > 1 :
                        raise ValueError(f"Only one parameter is expected to match the pattern here!")
                        
                    submodel.initialize(**{re.sub(r'raw_', '',outputscale_bottom_level[0][0]): torch.zeros(outputscale_bottom_level[0][1])}) 

                    # the "next model to be predicted" has one less level
                    NLevels_tmp = NLevels_tmp -1

                    if debug:
                        logging.debug("checking the number of levels")
                        logging.debug(NLevels_tmp)
            
            # here we finished to predict the joint models containing "Nlevels" to "1 Level" for this Id/Level_1
            for key_1 in pred_joint_model: # key_1 -> considered_levels
                for key_2 in pred_joint_model[key_1]: # key_2 -> level_1

                    if debug:
                        logging.debug("checking pred_joint_model before")
                        logging.debug(key_1)
                        logging.debug(key_2)
                        logging.debug(len(pred_joint_model[key_1][key_2]))
                    
                    pred_joint_model[key_1][key_2] = pd.concat(pred_joint_model[key_1][key_2], ignore_index=True) # concatenate the prediction for all comparisons of this "level_1" for these "considered_levels"
                    
                    if debug:
                        logging.debug("checking pred_joint_model after")
                        logging.debug(len(pred_joint_model[key_1][key_2]))
        else:
            raise NotImplementedError(f'The case where the control condition is not specified is not yet implemented!')
            # will need to defined that ['Level_1_ForNullHyp'] = level1 + "-" + treatment_conditiion + "-" + control_condition to separate the comparison of the same treatment conditions with different control conditions.

    ##################### Save the predictions #####################
    # here we finished to predict for all IDs / Level_1    
    pred_joint_model_df = {}

    for key in pred_joint_model: # key -> considered_levels

        if debug:
            logging.debug("Saving pred_joint_model")
            logging.debug(key)

        pred_joint_model_df[key] = pd.concat(pred_joint_model[key], ignore_index=True) # pred_joint_model[key] -> contains all the IDs
       
        # only save the individual predictions if explicitly required
        if parameters['save_individual_results']:
            pred_joint_model_df[key].to_csv(index = True, path_or_buf = os.path.join( type_pred + "_joint_model_" + key + ".csv" ))

    ##################### save the index_df corresponding to the joint models #####################
    #  (i.e one joint model for each comparison of each ID)
    Index_df_joint_model_ToSave = {}
    for key in Index_df_joint_model: # key -> Level_1
        Index_df_joint_model_ToSave[key] = pd.concat(Index_df_joint_model[key], ignore_index=True) # Index_df_joint_model[key] -> contains all the index for all comparisons of this ID
    
    Index_df_joint_model_ToSave_df = pd.concat(Index_df_joint_model_ToSave, ignore_index=True) # Index_df_joint_model_ToSave_df -> contains all the index for all IDs
    ColumnsOrder = ['Level_1', 'Level_1_ForNullHyp', 'Level_2_ForAltHyp', 'Level_2_ForNullHyp'] + ["Level_{0}".format(l) for l in range(3, NLevels+1)] + ['Index_Level_1', 'Index_Level_2_ForAltHyp', 'Index_Level_2_ForNullHyp'] + ["Index_Level_{0}".format(l) for l in range(3, NLevels+1)]
    ColumnsOrder = [x for x in ColumnsOrder if x]
    Index_df_joint_model_ToSave_df = Index_df_joint_model_ToSave_df[ColumnsOrder]
    
    # only save the individual predictions if explicitly required
    if parameters['save_individual_results']:
        Index_df_joint_model_ToSave_df.to_csv(index = True, path_or_buf = os.path.join( "index_joint_HGPM.csv" ))
    
    # note that the index_full_HGPM.csv could be obtained from index_joint_HGPM.csv by selecting only ['Level_1','Level_2_ForAltHyp'] + ["Level_{0}".format(l) for l in range(3, NLevels+1)] + ['Index_Level_1', 'Index_Level_2_ForAltHyp'] + ["Index_Level_{0}".format(l) for l in range(3, NLevels+1)]
    # and renaming .rename(columns={ "Level_2_ForAltHyp" : "Level_2", "Index_Level_2_ForAltHyp": "Index_Level_2"})

    ##################### save the sampling if done #####################
    if sampling:
        all_samples_df = pd.concat(all_samples)
        
        # only save the individual predictions if explicitly required
        if parameters['save_individual_results']:
            all_samples_df.to_csv(index=True, path_or_buf= os.path.join("prior_samples_all_levels.csv"))

    else:
        all_samples_df = None


    ##################### Save the mll #####################
    mll_df = pd.DataFrame({'Level_1' : level_1_mll,
    'comparison_id' : comparison_id_mll,
    'treatment_condition': treatment_condition_mll,
    'control_condition' : control_condition_mll,
    'loss_joint_model' : value_mll})

    results_dict = {
        'prediction_joint_hgpm_dict' : pred_joint_model_df,
        'joint_hgpm_index_df': Index_df_joint_model_ToSave_df,
        'samples_df': all_samples_df,
        'joint_hgpm_mll_values' : mll_df,
        'joint_hgpm_covariance_matrix_pdf_filename' : pdf_filename,
        'joint_hgpm_covariance_matrix_introduction_text' : pdf_introduction_text,
        'joint_hgpm_covariance_matrix_pdf_content' : pdf_content
    }   
    
    
    if debug:
        logging.debug("##############################")
    
    return results_dict

##############################
# evaluate_joint_HGPM
##############################
def evaluate_joint_HGPM(
    data, 
    parameters, 
    full_hgpm_state_dict_list,
    debug=True):

    if debug:
        logging.basicConfig(filename='debug.log', level=logging.DEBUG, format='%(message)s')
        logging.debug("##############################")
        logging.debug("Entering evaluate_joint_HGPM")


    ##############################################################################################
    ### Define the full HGPM, i.e the one in which each condition is modeled independently  ###
    ##############################################################################################
    model_list, likelihood_list, Index_df = Define_fullHGPM(data, parameters['modelType'],  parameters['mean'], parameters['lengthscale_prior'], parameters['lengthscale_MinConstraint'])

    #################################################
    ### Load the parameters of the trained model  ###
    #################################################
    ''' 
    state_dict = torch.load('hgpm_fit_parameters.pth')
    for submodels, sub_state_dict in zip(model_list.models, state_dict):
        submodels.load_state_dict(sub_state_dict)
    '''
    for submodels, sub_state_dict in zip(model_list.models, full_hgpm_state_dict_list):
        submodels.load_state_dict(sub_state_dict)

    ##################################################################################################
    ########################## Create the joint models from the full models ##########################  
    # The idea is to modify the training inputs of the full model to match the ones of a joint model
    ##################################################################################################

    ID2test = data['Level_1'].unique()
    if not len(ID2test) == len(model_list.models):
        raise ValueError(f"Problem with the number of models defined.")

    # to save the mll
    level_1_mll = []
    comparison_id_mll = []
    control_condition_mll = []
    treatment_condition_mll = []
    value_mll = []

    # iterate over the full models
    for level1, submodel, sublik in zip(ID2test, model_list.models, likelihood_list.likelihoods):

        ########## save the original parameters of the model ##########
        original_state_dict = deepcopy(submodel.state_dict()) # deep copy of the dictionary containing the parameters for our model
        
        if debug:
            logging.debug("original stat dict")
            logging.debug(original_state_dict)

        index_level_to_modify = 2 # in the curent GPMelt framework, we only change the index of the condition-level (i.e index of level_2) between the full and joint model
        original_train_inputs = deepcopy(submodel.train_inputs)

        # restrict the indices to the level1 of interest
        Index_df_allLevels = Index_df.copy(deep=True)
        Index_df_allLevels = Index_df_allLevels.loc[Index_df_allLevels['Level_1']==level1]
        Index_df_allLevels = Index_df_allLevels.rename(columns={"Level_2": "Level_2_ForAltHyp", "Index_Level_2":"Index_Level_2_ForAltHyp"})

        if not parameters["control_condition"] is None:
            control_condition = parameters["control_condition"]
            Index_Level_2_control = Index_df_allLevels.loc[Index_df_allLevels['Level_2_ForAltHyp'] == control_condition]['Index_Level_2_ForAltHyp'].unique().item()
            Index_Level_2_treatment = [x for x in Index_df_allLevels['Index_Level_2_ForAltHyp'].unique() if x != Index_Level_2_control]

            for Comparison_idx in Index_Level_2_treatment:
                
                ### first we modify the Index_df such that the treatment condition corresponding to the index "Comparison_idx" is combined with the control condition
                Index_df_forthiscomparison = Index_df_allLevels.copy(deep=True)
                treatment_condition = Index_df_forthiscomparison.loc[Index_df_forthiscomparison['Index_Level_2_ForAltHyp']==Comparison_idx, "Level_2_ForAltHyp"].unique().item()
                Comparison_id = treatment_condition + "-" + control_condition

                ########## reload the original parameter of the model ##########
                submodel.load_state_dict(original_state_dict)

                if debug:
                    logging.debug("stat dict for comparison " + Comparison_id)
                    logging.debug(submodel.state_dict())

                ### update the input data to the model
                modified_train_inputs = deepcopy(original_train_inputs)
                modified_train_inputs = modified_train_inputs[index_level_to_modify]
                #modified_train_inputs = submodel.train_inputs[index_level_to_modify]
                modified_train_inputs = torch.where(modified_train_inputs == Comparison_idx, torch.tensor(Index_Level_2_control), modified_train_inputs)
                modified_tuple = original_train_inputs[:index_level_to_modify] + (modified_train_inputs,) + original_train_inputs[index_level_to_modify+1:]
                submodel.train_inputs = modified_tuple

                if debug:
                    logging.debug("modified_train_inputs")
                    logging.debug(modified_train_inputs)
                    logging.debug(modified_tuple)

                #################### Compute the MLL for the obtained joint model ####################
                # now that we redefine the joint HGPM with all levels, we can compute the MLL
                mll = gpytorch.mlls.ExactMarginalLogLikelihood(sublik, submodel)
                output = submodel(*submodel.train_inputs)
                loss = -mll(output, submodel.train_targets)

                if debug:
                    logging.debug("Check loss variables for this comparison")
                    logging.debug(level1)
                    logging.debug(Comparison_id)
                    logging.debug(treatment_condition)
                    logging.debug(control_condition)
                    logging.debug("test1")
                    logging.debug(loss.detach().numpy())
                    logging.debug("test2")
                    logging.debug(loss.item())
                    

                level_1_mll.append(level1)
                comparison_id_mll.append(Comparison_id)
                treatment_condition_mll.append(treatment_condition)
                control_condition_mll.append(control_condition)
                value_mll.append(loss.item())
                
    ##################### Save the mll #####################
    mll_df = pd.DataFrame({'Level_1' : level_1_mll,
    'comparison_id' : comparison_id_mll,
    'treatment_condition': treatment_condition_mll,
    'control_condition' : control_condition_mll,
    'loss_joint_model' : value_mll})

    if debug:
        logging.debug("##############################")
    
    return mll_df

                        
                        
                        
            




            