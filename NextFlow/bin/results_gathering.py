#! /usr/bin/env python3

import sys
import dask.dataframe as dd
import pickle
import pandas as pd
import torch
import glob
import logging

##################
 # This script adds the items of the pkl file resulting from each job in combined_results
 # then each key of combined_results is concatenated and saved, either to a csv file (if the key contains a list of data frame)
 # or to a pth file if the key contains a list of dictionaries.
##################
def main(results_pkl_file_list,  debug=True):

    if debug:
        logging.basicConfig(filename='debug.log', level=logging.DEBUG, format='%(message)s')
        logging.debug('################################################################')
        logging.debug("Entering results gathering")
        logging.debug("The list of pkl files is: \n")
        logging.debug(results_pkl_file_list)

    # Open combined results as a new dictionary
    combined_results = {}

    for results_pkl_file_path in results_pkl_file_list :
        
        if debug:
            logging.debug("Opening file: " + str(results_pkl_file_path))

        # Load the new results
        with open(results_pkl_file_path, 'rb') as f:
            new_results = pickle.load(f)

        # Update the combined results
        for key, value in new_results.items():
            if isinstance(value, pd.DataFrame):
                # Convert to Dask DataFrame for efficient concatenation
                value = dd.from_pandas(value, npartitions=1)
                if key in combined_results:
                    # check that the type of data to add matches the previous type of data in the list
                    if not isinstance(combined_results[key], list) or not isinstance(combined_results[key][0], dd.DataFrame):
                        raise ValueError(f"Type mismatch for key '{key}': expected list of dask DataFrames, found {type(combined_results[key])}")
                    combined_results[key].append(value)
                else:
                    combined_results[key] = [value]
            elif isinstance(value, list) and isinstance(value[0], dict):
                
                if debug:
                    logging.debug(f"{key} is a list of dictionary in the results_job_id.pkl file, containing {len(value)} items.")

                # Flatten the list of dictionaries into combined_results
                for sub_dict in value: 
                    # subdict is a dictionary
                    for sub_key, sub_value in sub_dict.items():
                        if key in combined_results:
                            # Check that the type of data to add matches the previous type of data in the dictionary
                            if not isinstance(combined_results[key], dict):
                                raise ValueError(f"Type mismatch for key '{key}': expected dictionary, found {type(combined_results[key])}")
                            combined_results[key][sub_key] = sub_value
                        else:
                            combined_results[key] = {sub_key: sub_value}
                
                if debug:
                    logging.debug(f"In combined_results, {key} is a {type(combined_results[key])} containing {len(combined_results[key])} items.")
                    logging.debug(combined_results[key])

            else:
                raise ValueError(f"Unsupported data type for key '{key}': {type(value)}")
    
    number_of_pkl_gathered = len(results_pkl_file_list)
    N_id = None  # Initialize N_id to None

    for key, value in combined_results.items():

        #if isinstance(value, list) and isinstance(value[0], dict):
        if isinstance(value, dict):
            if N_id is None:
                N_id = len(value)  # Set N_id based on the first dictionary list encountered

            if debug:
                logging.debug(f"length of the {key} (dictionary): " + str(len(value)))
                logging.debug("expected length: " + str(N_id))

            if len(value) != N_id:
                raise ValueError(f"All dictionary entries of the combined results pkl file do not have the same number of items!")

        elif isinstance(value, list) and isinstance(value[0], dd.DataFrame):
            
            if debug:
                logging.debug(f"length of the {key} (dask dataframe): " + str(len(value)))
                logging.debug("expected length: " + str(number_of_pkl_gathered))

            if len(value) != number_of_pkl_gathered:
                raise ValueError(f"All keys of the combined results containing dataframes do not have the same number of items! The aggregation of pkl files went wrong somehow!")

        # if the key has the appropriate length, then concatenate the content
        if isinstance(value, list) and isinstance(value[0], dd.DataFrame):

            # Concatenate list of DataFrames
            df = dd.concat(value, axis=0).compute()

            # special treatment for some data frames:
            if key == 'samples_df' or key == 'likelihood_ratios_sampled_dataset_df':
                #rename some variables:
                df = df.rename(columns={'Level_1': 'Level_1_sample','Level_1_short': 'Level_1'})
                # Reorder columns
                new_order = ['job_id', 'Level_1', 'Level_1_sample'] + [col for col in df.columns if col not in ['job_id', 'Level_1', 'Level_1_sample']]
                df = df[new_order]
            elif key == 'likelihood_ratios_df':
                # Reorder columns
                new_order = ['job_id'] + [col for col in df.columns if col not in ['job_id']]
                df = df[new_order]

            df.to_csv(f"{key}.csv", index=False)

            if debug:
                logging.basicConfig(filename='debug.log', level=logging.DEBUG)
                logging.debug(f"{key}.csv has been saved!")

        #elif isinstance(value, list) and isinstance(value[0], dict):
        elif isinstance(value, dict):

            torch.save(value, f= f"{key}.pth")

            if debug:
                logging.debug(f"{key}.pth has been saved!")
            
        else:
            raise ValueError(f"Unsupported data type for key '{key}': {type(value)}")

    # Save the updated combined results
    with open('combined_results.pkl', 'wb') as f:
        pickle.dump(combined_results, f)
        if debug:
            logging.debug(f"combined_results.pkl has been saved!")
    
    if debug:
        logging.debug('################################################################')


if __name__ == '__main__':
    # removing the * before the sys allows to capture all arguments passed to the main as first argument, here results_pkl_file_list
    main(sys.argv[1:])
    