#! /usr/bin/env python3

from matplotlib import pyplot as plt
import pandas as pd
import numpy as np
from pathlib import Path
import pickle
import multiprocessing as mp
import pickle
import torch

import os
import sys
import logging

from fitting_functions.fit_evaluate_sample import fit_evaluate_predict_and_sample_HGPM, fit_evaluate_HGPM_on_one_sample
from utils.Check_modelTypeValidity import Check_modelTypeValidity

########################################
# in this python script, we
# 1) read the pkl file associated to the ID of interest
# 
# ########################################s

def main(pkl_file_path, NUM_CPU, debug=True):

    if debug:
        logging.basicConfig(filename='debug.log', level=logging.DEBUG, format='%(message)s')
        logging.debug('################################################################')
        logging.debug('Entering GPMelt for job id: \n')
        logging.debug(pkl_file_path)
        logging.debug("Checking os.environ['OMP_NUM_THREADS'] = " + str(os.environ['OMP_NUM_THREADS']))
    
    if os.environ['OMP_NUM_THREADS'] != '1' :
        raise ValueError(f"os.environ['OMP_NUM_THREADS'] should be set to str(1) to limit the use of thread for GPytorch. At the moment,os.environ['OMP_NUM_THREADS'] = {os.environ['OMP_NUM_THREADS'] }.")

    # Global_dict contains mulitple dictionary, each of these dictionaries corresponding ot one ID
    with open(pkl_file_path, 'rb') as f:
        Global_dict = pickle.load(f)
    
    ##################################################
    ## iterate over the Ids
    ##################################################

    ID_array = list(Global_dict.keys())

    if debug:
        logging.debug(f"Running GPMelt on a total of {len(ID_array)} ids.")

    combined_results_fit_evaluate_and_sample_HGPM = {}

    for id in ID_array:

        if debug:
            logging.debug(f"Running GPMelt for id {id}")

        data = Global_dict[id]['data'] 
        numberSamples_df = Global_dict[id]['numberSamples_perID'] 
        parameters = Global_dict[id]['parameters'] 
        Level_1 = data['Level_1'].unique().item()

        ## Check that the dataset has the required variable given the model type ##
        NLevels, dfColnames_Levels = Check_modelTypeValidity(data, parameters['modelType'])

        ##################################################
        ## define which results should be saved
        ##################################################
        FreeSpace = parameters['Minimise_required_memory_space']
        if not type(FreeSpace) == type(True):
            raise ValueError(f'variable Minimise_required_memory_space should be a boolean')

        #To reduce the amount of memory, the user can select to save less results
        if FreeSpace : 
            required_outputs = ["full_hgpm_state_dict_list","full_hgpm_index_df","joint_hgpm_index_df","full_hgpm_fit_parameters_df", "full_hgpm_fit_loss_values_df", "prediction_full_hgpm_dict", "prediction_joint_hgpm_dict", "likelihood_ratios_df", "samples_df", "full_hgpm_fit_parameters_sampled_dataset_df", "likelihood_ratios_sampled_dataset_df"]

        ##################################################
        ## First step is to fit the full HGPM on the real data, evaluate the fit on the full and joint model and possibly sample fromt he joint model.
        ##################################################
        # we first want to make sure that the results won't be saved individually
        parameters['save_individual_results'] = False

        results_fit_evaluate_and_sample_HGPM = fit_evaluate_predict_and_sample_HGPM(
            data, 
            parameters,
            sampling=True,
            numberSamples_df = numberSamples_df,
            debug=True)
        
        ##################################################
        ## Process the output to simplify it
        ##################################################
        # transform the prediction dictionaries into dataframes.
        for prediction_dict in ["prediction_full_hgpm_dict", "prediction_joint_hgpm_dict"]:
            # Extract the prefix from the prediction_dict name
            prefix = prediction_dict.replace("_dict", "")
            new_keys = []
            # Replace the dictionary of data frames with individual data frames
            for key, df in results_fit_evaluate_and_sample_HGPM[prediction_dict].items():
                new_key = f'{prefix}_{key}'
                results_fit_evaluate_and_sample_HGPM[new_key] = df
                new_keys.append(new_key)
            # remove the key from the dictionary
            del results_fit_evaluate_and_sample_HGPM[prediction_dict]
            #add the new keys in the required_outputs
            if FreeSpace :
                # Replace the entry in required_outputs with new_keys
                index = required_outputs.index(prediction_dict)
                required_outputs[index:index+1] = new_keys
                #required_outputs = required_outputs[:index] + new_keys + required_outputs[index+1:]
        
        if debug:
            if FreeSpace :
                logging.debug(f"after adding the prediction_df to the required_outputs:{required_outputs}")

        # add the name of ids for which the parameters, models and likelihood have been obtained by transforming the lists into dictionaries, where the key indicates the id
        for result_list in ['full_hgpm_state_dict_list', 'full_hgpm_model_list', 'full_hgpm_likelihood_list']:
            # Extract the prefix from the prediction_dict name
            name_res = result_list.replace("_list", "_dict")

            match result_list:
                case 'full_hgpm_state_dict_list':
                    value_res = results_fit_evaluate_and_sample_HGPM[result_list]
                case 'full_hgpm_model_list':
                    value_res = results_fit_evaluate_and_sample_HGPM[result_list].models
                case 'full_hgpm_likelihood_list':
                    value_res = results_fit_evaluate_and_sample_HGPM[result_list].likelihoods

            results_fit_evaluate_and_sample_HGPM[name_res] = {id: res_val for id, res_val in zip(results_fit_evaluate_and_sample_HGPM["full_hgpm_ID_list"], value_res)}

            #add the new keys in the required_outputs. Note that only 'full_hgpm_state_dict_list' is in required_outputs
            if FreeSpace and result_list == 'full_hgpm_state_dict_list' :
                # Replace the entry in required_outputs with new_keys
                index = required_outputs.index(result_list)
                required_outputs[index] = name_res

            # remove the key from the dictionary
            del results_fit_evaluate_and_sample_HGPM[result_list]

        del results_fit_evaluate_and_sample_HGPM["full_hgpm_ID_list"]
                
        if debug:
            if FreeSpace :
                logging.debug(f"after adding the stat_dict as dictionnary to the required_outputs: {required_outputs}")

        ##################################################
        ## combine the results obtained on each ID
        #################################################

        # add keys to combined_results_fit_evaluate_and_sample_HGPM if not exisiting yet
        #(done here as we update the required_outputs in the previous chunk)
        if FreeSpace :
            for key in required_outputs:
                if key not in combined_results_fit_evaluate_and_sample_HGPM:
                    combined_results_fit_evaluate_and_sample_HGPM[key] = []

        # iterate over the keys      
        for key, df in results_fit_evaluate_and_sample_HGPM.items():
            if FreeSpace : 
                if key in required_outputs:
                    combined_results_fit_evaluate_and_sample_HGPM[key].append(df)

                    if debug:
                        logging.debug(f"nb of dataframe for key {key}: {len(combined_results_fit_evaluate_and_sample_HGPM[key])}")
            else:
                if key not in combined_results_fit_evaluate_and_sample_HGPM:
                    combined_results_fit_evaluate_and_sample_HGPM[key] = []
                combined_results_fit_evaluate_and_sample_HGPM[key].append(df)

                if debug:
                    logging.debug(f"nb of dataframe for key {key}: {len(combined_results_fit_evaluate_and_sample_HGPM[key])}")

        if debug:
            logging.debug("keys in combined_results_fit_evaluate_and_sample_HGPM")
            for key, _ in combined_results_fit_evaluate_and_sample_HGPM.items():
                logging.debug(key)

        #then work on the sampled dataset
        sampled_dataset = results_fit_evaluate_and_sample_HGPM['samples_df']

        # clear the memory as not longer needed
        del results_fit_evaluate_and_sample_HGPM
        
        if sampled_dataset is not None:
            ##################################################
            ## As a second step we preprocess the dataset obtained by generating samples from the joint model.
            ##################################################
            # preprocess the dataset made of samples under the null 
            sampled_dataset['Level_1_short'] = sampled_dataset['Level_1']
            sampled_dataset['Level_1'] =  sampled_dataset['Level_1_short'] + "_Sample" + sampled_dataset['Dataset'].astype(int).astype(str) + "_CtrlCd" + sampled_dataset['control_condition']
            sampled_dataset = sampled_dataset.drop(columns={'Level_2_ForNullHyp'})

            ##################################################
            ## Fitting of the null dataset ##
            # All samples are fitted in parallel via multiprocessing
            ##################################################
            '''
            Level1_Samples = sampled_dataset.Level_1.unique()

            # Initialize an empty dictionary to collect DataFrames
            combined_results_dict = {}

            for level1 in Level1_Samples:
                
                df = sampled_dataset[sampled_dataset.Level_1 == level1]
                
                df = df.drop(columns=['Level_1_short'])
                df = df.sort_values( dfColnames_Levels + ['rep',  'x'])
            
                if debug:
                    logging.basicConfig(filename='debug.log', level=logging.DEBUG)
                    logging.debug(f"Enter fitting of sample: {level1}")

                result_dict = fit_evaluate_HGPM_on_one_sample(
                    df, 
                    parameters,
                    debug=debug)
                
                # directly combine the results obtained on each level1
                for key, df in result_dict.items():
                    if key not in combined_results_dict:
                        combined_results_dict[key] = []
                    combined_results_dict[key].append(df)
                
                del result_dict
            '''

            pool = mp.Pool(int(NUM_CPU))
            results_objects = [] 

            Level1_Samples = sampled_dataset.Level_1.unique()

            # Initialize an empty dictionary to collect DataFrames
            combined_results_dict = {}

            for level1 in Level1_Samples:
                
                df = sampled_dataset[sampled_dataset.Level_1 == level1]
                
                df = df.drop(columns=['Level_1_short'])
                df = df.sort_values( dfColnames_Levels + ['rep',  'x'])
            
                if debug:
                    logging.debug(f"Enter fitting of sample: {level1}")
                
                results_objects.append(pool.apply_async(fit_evaluate_HGPM_on_one_sample, args=(df, parameters, False)))

            results = [r.get() for r in results_objects] #required, as this will wait all parallels jobs to be ran before ending

            pool.close()
            pool.join() 

            # combine the results obtained on each sample in a list
            for res_dict in results:
                for key, df in res_dict.items():
                    if key not in combined_results_dict:
                        combined_results_dict[key] = []
                    combined_results_dict[key].append(df)

            del results

            # Concatenate the lists for each key
            for key, df_list in combined_results_dict.items():
                combined_df = pd.concat(df_list, ignore_index=True)
                combined_df['Level_1_short'] = Level_1
                if FreeSpace : 
                    if key in required_outputs:
                        combined_results_fit_evaluate_and_sample_HGPM[key].append(combined_df)
                else:
                    if key not in combined_results_fit_evaluate_and_sample_HGPM:
                        combined_results_fit_evaluate_and_sample_HGPM[key] = []
                    combined_results_fit_evaluate_and_sample_HGPM[key].append(combined_df)
            
            del combined_results_dict
            
            ##################################################
    
    # Combine all the results for this Id with the previously obtained ones.
    if debug:
        logging.debug("keys in combined_results_fit_evaluate_and_sample_HGPM - second check")
        for key, _ in combined_results_fit_evaluate_and_sample_HGPM.items():
            logging.debug(key)

    # concatenate the dataframes and dictionnary and save into a pkl file.
    job_id =  os.path.splitext(os.path.basename(pkl_file_path))[0] # extract the job_id
    if debug:
        logging.debug("Checking the content of the final results dict")

    for key, df_list in combined_results_fit_evaluate_and_sample_HGPM.items():

        if debug:
            logging.debug(key)

        # Check if df_list is actually a list of DataFrames
        if isinstance(df_list, list) and all(isinstance(df, pd.DataFrame) for df in df_list):
            # if this is the case, then concatenate the data frames
            combined_df = pd.concat(df_list, ignore_index=True)
            combined_df['job_id'] = job_id
            combined_results_fit_evaluate_and_sample_HGPM[key] = combined_df
        elif not (isinstance(df_list, list) and all(isinstance(df, dict) for df in df_list)):
            # otherwise we expect a list of dictionaries
            raise ValueError(f"Unsupported data type in list for key '{key}': {type(df_list)}")
    

    with open(os.path.join(f'results_{job_id}.pkl'), 'wb') as f:
        pickle.dump(combined_results_fit_evaluate_and_sample_HGPM, f)
    
    if debug:
        logging.debug('################################################################')
    

if __name__ == '__main__':
    main(*sys.argv[1:])