#! /usr/bin/env python3

####################################################################
#    Check_modelTypeValidity
####################################################################
def Check_modelTypeValidity(df, modelType):
    # define the number of layers in your hierarchy:
    NLevels = int(modelType.split("Levels_")[0])
    assert 3 <= NLevels <= 4, "Only HM with 3 or 4 levels have been defined, but you asked for " + str(NLevels) + "!"

    # Check the appropriate columns have been defined
    dfColnames_Levels = ["Level_{:d}".format(x) for x in range(1,(NLevels+1))]
    assert set(dfColnames_Levels).issubset(df.columns), "Columns named Level_i for i from 1 to "+ str(NLevels)+ " are required in the dataset."

    return NLevels, dfColnames_Levels

    