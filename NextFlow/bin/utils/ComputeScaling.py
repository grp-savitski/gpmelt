#! /usr/bin/env python3

import numpy as np
import copy
import pandas as pd

from scipy.stats import gmean

####################################################################
#    yVal_forlowestX
####################################################################

def yVal_forlowestX(df):
    r"""
    Returns the value of y for the lowest x.

    typically usage for TPP dataset: returns the value of the observed intensity (y) for the lowest temperature (x) for fold change computation.

    : param pd.DataFrame df: df needs to be a panda.DataFrame with columns:
        - x : training features (e.g. temperatures)
        - y : the training targets (e.g. scaled intensities). Note: when entering this function, the input y is expected to be already scaled, i.e to not be in the world of the raw intensities.
        
    """

    smallest_x_index = df['x'].idxmin()  # Get the index of the row with the smallest 'x'
    smallest_y_value = df.loc[smallest_x_index, 'y']  # Get the value of 'y' corresponding to the smallest 'x'
    return pd.Series([smallest_y_value] * len(df), index=df.index)  # Create a Series with the same 'y' value for each row


####################################################################
#    compute_ScalingFactorPerReplicate
####################################################################

def compute_ScalingFactorPerReplicate(df, NLevels=None, Min_y = 1e-8):
    
    r"""
    Compute the following scaling factors for each replicate from the input data frame: "mean", "median", "geometric mean", "fold change".
    Also returns the "log" transform data.

    : param pd.DataFrame df: df needs to be a panda.DataFrame with columns:
        - Level_1 : protein-ID, unique ID, gene ID, etc... this is an identifier of the entity to whom the data belong.
        - Level_2 to Level_L : L indicates the number of levels in the hierarchical model, each "Level_l" indicate, for each input (x, y), to which group it belongs to. For example Level_2 could be the condition (control vs treatment), and Level_3 the replicate value.
        - x : training features (e.g. temperatures)
        - y : the training targets (e.g. raw intensities).
        
    """
    #import pdb; pdb.set_trace() 

    if NLevels is None:
        # compute the number of levels from the column names of the data frame.
        Levels = [s for s in df.columns if "Level_" in s]
        NLevels = int(max([item.split("_")[1] for item in Levels]))
    
    # create a deep copy of df, to not modify df.
    df_scaled = df.copy()
    grouped_df = df.copy()

    # We replace 0 values in the observations by Min_y to avoid the geometric mean to be 0 if one observation is 0
    if not df[df['y']==0].empty :
        df_scaled.loc[df_scaled['y']==0, 'y'] = Min_y
        grouped_df.loc[grouped_df['y']==0, 'y'] = Min_y

    grouped_df = grouped_df.groupby(["Level_{0}".format(x) for x in range(1, NLevels + 1)])

    # compute the median per replicate
    df_scaled['median'] = grouped_df['y'].transform('median')
    # compute the mean per replicate
    df_scaled['mean'] = grouped_df['y'].transform('mean')
    # compute the geometric mean per replicate
    df_scaled['geometric_mean'] = grouped_df['y'].transform(lambda x: gmean(x.dropna()))
    # compute the FC per replicate
    df_scaled['FC'] = grouped_df.apply(yVal_forlowestX).reset_index(name='FC')['FC'].values

    # compute the scaled values
    df_scaled['y_mean_Scaling'] = df_scaled['y']/df_scaled['mean']
    df_scaled['y_median_Scaling'] = df_scaled['y']/df_scaled['median']
    df_scaled['y_geometricMean_Scaling'] = df_scaled['y']/df_scaled['geometric_mean']
    df_scaled['y_FC_Scaling'] = df_scaled['y']/df_scaled['FC']
    df_scaled['y_log'] = np.log(df_scaled['y'])

    return df_scaled
    
  