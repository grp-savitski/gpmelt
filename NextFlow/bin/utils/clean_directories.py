#! /usr/bin/env python3

import os
import shutil

##################################
# clean_directory
##################################
# remove all but the files specified in Required_files, and move all files from Required_files to the base_dir
def clean_directory(base_dir, required_files):

    for root, dirs, files in os.walk(base_dir, topdown=False):
        # Process each file in the current directory
        for file in files:
            file_path = os.path.join(root, file)
            if file in required_files:
                shutil.move(file_path, os.path.join(base_dir, file))
            else:
                os.remove(file_path)

        # Process each subdirectory in the current directory
        for dir in dirs:
            dir_path = os.path.join(root, dir)
            if dir_path != base_dir:
                shutil.rmtree(dir_path)