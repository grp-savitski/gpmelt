#! /usr/bin/env python3

import pandas as pd

####################################################################
#    colorblind_palette_forConditions
####################################################################
# here, I combined the "safeColors" palette with a subset of the SteppedSequential5Steps palette (both from the R package colorBlindness)
# For additional conditions, colors are recyclued from the 'base_palette' palette.
def colorblind_palette_forConditions(n):
    r'''
        Args : 
            n : total number of conditions
    '''
    
    # "#000000" -> black
    # "#56B4E9" -> blue for the ID-wise melting curve
    # "#009E73" -> green for control condition
    # "#E69F00" -> orange for  the treatment condition
    # '#0F6B99', '#990F0F', '#260F99', '#99540F',"#CC79A7", "#F0E442", "#0072B2","#E69F00" -> additional color if necessary
    base_palette = ['#0F6B99', '#990F0F', '#260F99', '#99540F',"#CC79A7", "#F0E442", "#0072B2","#E69F00" ]

    df =  pd.DataFrame({
        'condition' : ['ID-wise', 'control_condition', 'treatment_condition', 'joint_condition'] + ['additional_condition_{0}'.format(x) for x in range(1,n+1)],
        'color' : ["#56B4E9","#009E73","#E69F00", "#000000"] + [base_palette[i % len(base_palette)] for i in range(n)]
    })

    return df

    '''
    # Plot the colors
    fig, ax = plt.subplots()
    for i, color in enumerate(df['color']):
        ax.scatter(i % 5, -(i // 5), color=color, s=200)

    ax.set_xlim(-1, 5)
    ax.set_ylim(-5, 1)
    ax.axis('off')

    plt.show() 
    '''



    