#! /usr/bin/env python3

# this function combinesmulitple dictionaries in one, while ensuring that all dictionaries have different keys
def combine_dictionaries(*dicts):
    combined_dict = {}
    for d in dicts:
        for key, value in d.items():
            if key in combined_dict:
                raise ValueError(f"Duplicate key found: {key}")
            combined_dict[key] = value
    return combined_dict