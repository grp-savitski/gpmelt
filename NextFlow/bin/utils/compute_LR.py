#! /usr/bin/env python3

import os
import numpy as np

####################################################################
#    ComputeRatios
####################################################################
def ComputeRatios(mll_full_value, mll_joint_HGPM_df):

    r"""
    Returns the ratio of marginal likelihoods:
        $$ 
        LR  = -2 \log \frac{p(Y | T, \theta_{ \mathcall{M}_1}^{MLE}, \mathcall{M}_0)}{p(Y | T, \theta_{ \mathcall{M}_1}^{MLE}, \mathcall{M}_1)}  \\
            = -2 \cdot [- \log p(Y | T, \theta_{ \mathcall{M}_1}^{MLE}, \mathcall{M}_1) - ( - \log p(Y | T, \theta_{ \mathcall{M}_1}^{MLE}, \mathcall{M}_0))] \\
            = -2 \cdot (loss_{\mathcall{M}_1}  - loss_{\mathcall{M}_0})
        $$ 
        

    with
        * $\theta_{ \mathcall{M}_1}^{MLE}$ the type-II MLE of the parameters and hyperparameters for $\mathcall{M}_1$, 
        * $\mathcall{M}_0$ the joint model, 
        * $\mathcall{M}_1$ the full model, 
        * $ - log p(Y | T, \theta_{ \mathcall{M}_1}^{MLE}, \mathcall{M}_i) = $ loss for model $\mathcall{M}_i$

    """

    mll_joint_HGPM_df['loss_full_model'] = np.repeat(mll_full_value, mll_joint_HGPM_df.shape[0] ) 
    mll_joint_HGPM_df["LikelihoodRatio"] = -2 * ( mll_joint_HGPM_df['loss_full_model'] - mll_joint_HGPM_df['loss_joint_model']) # loss = - mll
    
    #reorder the dataframe with largest ratios first
    mll_joint_HGPM_df = mll_joint_HGPM_df.sort_values(by=['LikelihoodRatio'], ascending=False)
    
    return mll_joint_HGPM_df

