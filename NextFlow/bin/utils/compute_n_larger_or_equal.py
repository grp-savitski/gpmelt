#! /usr/bin/env python3

# This function counts how many values of set_of_ratios are larger or equal than the actual value of a likelihood ratio  
def calculate_sum_extreme(likelihood_ratio, null_distribution_approximation_of_the_statistic):
    return sum(1 for ratio in null_distribution_approximation_of_the_statistic if ratio >= likelihood_ratio) 