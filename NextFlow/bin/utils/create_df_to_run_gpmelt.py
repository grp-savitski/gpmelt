import pandas as pd

#######################################
### function to create a data frame defining where are the data, and which Ids to run for each jobs.
#######################################
def create_id_dataframe(dir_name,  N_IDs, N_subsequent_ids):

    r"""
    Return a data frame such that each line is a job that can be run in parallel from the others.
    The function defines the 'rules' on how to run GPMelt, i.e specifying how many IDs per job should be analysed subsequently. 
    Typically, if the dataset has a lot of 'small' IDs (i.e IDs with small amount of data, few replicates, few conditions) it's more efficient to run mulitple of them together in a job.
    If the IDs are large (large number of replicates and/or conditions) then it's likely better to run more jobs and each job considers fewer IDs.
    The trade-off between number of jobs to be un in parallel vs number of IDs per job can be optimised by the user.

    Args:
        dir_name (string) : path to the directory where the pkl files with the data & parameters are saved
        N_IDs (int) : total number of IDs in the dataset
        N_subsequent_ids (int) : number of IDs to run subsequently in each job

    """
    ids = list(range(0, N_IDs))
    
    # Create the min_id and max_id ranges
    min_ids = ids[::N_subsequent_ids]
    max_ids = ids[N_subsequent_ids-1::N_subsequent_ids]
    
    # Adjust the last max_id to be N_IDs if it doesn't align perfectly
    if len(min_ids) > len(max_ids):
        max_ids.append(N_IDs)
    
    # Generate the pkl_files_name list
    pkl_files_name = [f"job_id_{i}" for i in range(len(min_ids))]
        
    # Create the dataframe
    df = pd.DataFrame({
        'pkl_files_directory': [dir_name] * len(min_ids),
        'pkl_files_name': pkl_files_name,
        'min_id': min_ids,
        'max_id': max_ids
    })
    
    return df