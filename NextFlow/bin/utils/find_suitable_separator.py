#! /usr/bin/env python3

# Function to find a suitable separator
# in the list of potential_separators, search for the forst one which does not appear in any of the unique_values, in order to use it to collapse these unique_values in one string.
def find_suitable_separator(unique_values, potential_separators):
    for separator in potential_separators:
        if not any(separator in value for value in unique_values):
            return separator
    raise ValueError("No suitable separator found in the provided list.")


def combine_unique_values_to_string( unique_values, potential_separators = ["_", ".", '|', '~', '^', ';', ':'] ):
    # Find a suitable separator
    separator = find_suitable_separator(unique_values, potential_separators)

    # Create a string from all the unique values using the separator
    unique_values_string = separator.join(unique_values)

    return unique_values_string
