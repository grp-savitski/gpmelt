#! /usr/bin/env python3

import pickle


###########################
# load_pickle
###########################
def load_pickle(file):

    r"""
    if the pickle file doesn't exist yet, create a dictionary, otherwise read the pickle file.
    """
    try:
        with open(file, 'rb') as f:
            return pickle.load(f)
    except FileNotFoundError:
        return {}