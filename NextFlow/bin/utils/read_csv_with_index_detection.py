#! /usr/bin/env python3

import pandas as pd
import logging

#########################
# because we don't know how the dataset ahs been saved df.to_csv("*.csv", index=False) or df.to_csv("*.csv", index=True), we use this function to detect if the first column is an index column or not.
#########################
def read_csv_with_index_detection(file_path, debug=False):

    if debug:
        logging.basicConfig(filename='debug.log', level=logging.DEBUG, format='%(message)s')
        logging.debug("##############################")
        logging.debug("Entering read_csv_with_index_detection ")

    # Step 1: Read the CSV file without specifying the index column
    df = pd.read_csv(file_path)
    
    # Step 2: Check if the first column looks like an index
    first_column_name = df.columns[0]
    first_column_values = df[first_column_name]
    
    # Heuristic: Check if the first column values are unique and sorted (which is common for index columns)
    if first_column_values.is_unique and first_column_values.is_monotonic_increasing:
        # Step 3: Re-read the CSV file using the first column as the index
        df = pd.read_csv(file_path, index_col=0)
        if debug:
            logging.debug(f"Using '{first_column_name}' as the index column.")
    else:
        if debug:
            logging.debug(f"First column '{first_column_name}' is not used as an index column.")
    
    if debug:
        logging.basicConfig(filename='debug.log', level=logging.DEBUG, format='%(message)s')
        logging.debug("##############################")
    
    return df