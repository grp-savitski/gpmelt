#! /usr/bin/env python3

import re

####################################################################
#    RetrieveNLevels_fromModelParam
####################################################################
def RetrieveNLevels_fromModelParam(model):

    task_covar_module_levels=[]
    #we assume that all models of a model list have the same number of levels
    for param_name, param in model.named_parameters():
        if re.search('task_covar_module_', param_name):
            task_covar_module_levels.append(param_name)
    #compute the highest level found among the param of the model
    NLevels = max([int(item.split(".")[0].split('task_covar_module_level')[1]) for item in task_covar_module_levels])
    
    return NLevels