#! /usr/bin/env python3
import re
import numpy as np
import logging
from utils.retrieveNlevels_FromModelParam import RetrieveNLevels_fromModelParam
####################################################################
#    SaveParameters
####################################################################

def SaveParameters(Index_df, model, ID, debug=False):

    if debug:
        logging.basicConfig(filename='debug.log', level=logging.DEBUG, format='%(message)s')
        logging.debug("##############################")
        logging.debug("Entering SaveParameters")

    NLevels = RetrieveNLevels_fromModelParam(model)

    # Check the appropriate columns have been defined
    dfColnames_Levels = ["Level_{0}".format(x) for x in range(1,(NLevels+1))]
    assert set(dfColnames_Levels).issubset(Index_df.columns), "Columns named Level_i for i from 1 to "+ str(NLevels)+ " are required in the Index_df dataset."
    
    ColumnsToKeep = dfColnames_Levels + [ 'condition' if set(['condition']).issubset(Index_df.columns) else ''] + [  'rep' if set(['rep']).issubset(Index_df.columns) else ''] 
    ColumnsToKeep = [x for x in ColumnsToKeep if x]

    #import pdb; pdb.set_trace()
    Parameters_df = Index_df.filter(items = ColumnsToKeep).copy()
    #Parameters_df = Parameters_df.loc[Index_df['Level_1']==ID]
    
    # check if there is 1 or 2 lengthscales
    for param_name, param in model.named_parameters():
        if debug:
            logging.debug(param_name)
        if re.match('covar_module_LastLevel', param_name):# search the pattern t the start of the string
            Parameters_df['lengthscale_LastLevel'] = np.tile(model.covar_module_LastLevel.lengthscale.detach().item(), Index_df.shape[0])

    Parameters_df['lengthscale'] = np.tile(model.covar_module.lengthscale.detach().item(), Index_df.shape[0])
    Parameters_df['noise'] = np.tile(model.likelihood.noise.detach().item(), Index_df.shape[0])
    Parameters_df["outputscale_Level_1"] =  np.repeat( model.task_covar_module_level1.var_cnst.unique().detach(),Index_df.shape[0])
    #Parameters_df["outputscale_Level_1"] =  np.repeat( model.task_covar_module_level1._eval_covar_matrix().unique().detach(),Index_df.shape[0])
    ##### the following line is wrong
    #Parameters_df["outputscale_Level_1"] =  np.repeat( getattr(model, 'task_covar_module_level1').var.unique().detach(),Index_df.shape[0])

    # variance parameters
    for l in reversed(range(2, NLevels+1)):

        ParamVal = getattr(model, 'task_covar_module_level{0}'.format(l)).var.detach()

        if debug:
            logging.debug(f"level for param save {l}")

        if (l == NLevels) and (len(ParamVal) == len(set(Index_df.loc[Index_df['Level_1']==ID]['Index_Level_{0}'.format(l-1)].unique()))):
            
            if debug:
                logging.debug("case FreeLevel3PerGroup for param save")

            Parameters_df["outputscale_Level_{0}".format(l)] =  np.repeat(ParamVal, Index_df.loc[Index_df['Level_1']==ID]['Index_Level_{0}'.format(l-1)].value_counts(sort=False)) # if one value per group for the last level
        else:
            if debug:
                logging.debug("other cases than FreeLevel3PerGroup for param save")
            Parameters_df["outputscale_Level_{0}".format(l)] =  np.repeat(ParamVal, Index_df.loc[Index_df['Level_1']==ID]['Index_Level_{0}'.format(l)].value_counts(sort=False)) # if one value per task/ one value for all tasks (any level)

    ColumnsOrder = dfColnames_Levels + ["outputscale_Level_{0}".format(x) for x in range(1,(NLevels+1))] + ['lengthscale'] + [ 'lengthscale_LastLevel' if set(['lengthscale_LastLevel']).issubset(Parameters_df.columns) else ''] +  ['noise']
    ColumnsOrder = [x for x in ColumnsOrder if x]
    Parameters_df = Parameters_df[ColumnsOrder]
   
    return Parameters_df