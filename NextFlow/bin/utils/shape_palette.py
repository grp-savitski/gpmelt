#! /usr/bin/env python3

import pandas as pd
import numpy as np


####################################################################
#    shape_palette_forReplicates
####################################################################
# create a palette fo shape for at most 8 replicates per condition
# the idea is to assign different shape to the control condition and the treatment condition.
# if additional conditions are considered, then the shapes are the same than for the treatment condition.
def shape_palette_forReplicates(full_prediction_dict,NLevels):

    # shape -- can plot up to 8 replicates and 2 conditions (i.e 16 shapes)
    marker_shape = ['o', 'v', 's','^', '*','<', 'p', '>', 'D',  'X', 'h','1', '2', '3', '4','d']
    
    # first, we count the max number of replicate per condition
    count_rep = full_prediction_dict['Levels_1to{0}'.format(NLevels)].groupby('Level_{0}'.format(NLevels-1))['Index_Level_{0}'.format(NLevels)].nunique().reset_index(name='n_rep')
    
    # define a mapping corresponding to the largest number of replicates
    largest_count_rep = max(count_rep['n_rep'])

    if 2*largest_count_rep > len(marker_shape):
        raise ValueError(f"Too many replicates to consider, can at most consider 8 replicates per condition.")

    else:
        shape_df =  pd.DataFrame({
            'rep' : np.tile(range(1, largest_count_rep + 1), reps=2),
            'markers' : marker_shape[:(2* largest_count_rep)],
            'condition' : np.repeat(['control_condition', 'treatment_condition'], repeats = largest_count_rep)
        })

        return shape_df
    

''' 
####################################################################
#    shape_palette_forReplicates
####################################################################
# create a palette fo shape for at most 8 replicates per condition
# the idea is to assign different shape to the control condition and the treatment condition.
# if additional conditions are considered, then the shapes are the same than for the treatment condition.
def shape_palette_forReplicates(full_prediction_dict,NLevels, control_condition=None, treatment_condition=None):

    # shape -- can plot up to 8 replicates and 2 conditions (i.e 16 shapes)
    marker_shape = ['o', 'v', 's','^', '*','<', 'p', '>', 'D',  'X', 'h','1', '2', '3', '4','d']
    
    # first, we count the max number of replicate per condition
    count_rep = full_prediction_dict['Levels_1to{0}'.format(NLevels)].groupby('Level_{0}'.format(NLevels-1))['Index_Level_{0}'.format(NLevels)].nunique().reset_index(name='n_rep')
    # we then add a row for each of these replicates
    count_rep_extended = pd.concat([pd.DataFrame({'Level_{0}'.format(NLevels-1): row['Level_{0}'.format(NLevels-1)], 'rep': range(1, row['n_rep'] + 1)}) for _, row in count_rep.iterrows()])

    # Merge count_rep and count_rep_extended to fill missing columns
    shape_df = pd.merge(count_rep, count_rep_extended, on='Level_{0}'.format(NLevels-1))

    # check that the sum of treatment + control condition replicates is not above 16
    if (not control_condition is None) and (not treatment_condition is None):
        count_control = shape_df[shape_df['Level_2'] == control_condition].shape[0]
        count_treatment = shape_df[shape_df['Level_2'] == treatment_condition].shape[0]
        sum_count = count_control + count_treatment 
    else:
        # we take the 2 conditions with the largest number of replicates
        shape_df_reordered = shape_df.sort_values(by=['n_rep'], ascending=[False]) 
        sum_count = sum(shape_df_reordered['n_rep'].iloc[:1])
        # assign the control and treatment conditions values to the 2 conditions with the largest number of replicates (just for the replicates shape definition)
        control_condition = shape_df_reordered['Level_2'].iloc[0]
        treatment_condition = shape_df_reordered['Level_2'].iloc[1]

    if sum_count > len(marker_shape):
        raise ValueError(f"Too many replicates to consider, can at most consider 16 repliates between the two conditions wiht the largest number of replicates.")
    else:
        # Assign markers for X=a
        markers_control = marker_shape[:count_control]
        shape_df.loc[shape_df['Level_2'] == control_condition, 'marker'] = markers_control

        # Assign markers for X=b
        markers_treatment = marker_shape[count_control:count_control + count_treatment]
        shape_df.loc[shape_df['Level_2'] == treatment_condition, 'marker'] = markers_treatment
        
        # Create a mapping of Level_2 and rep to marker for values not 'control_condition' or 'treatment_condition'
        mapping = shape_df[shape_df['Level_2'] == '_RpSVGSDE_'].groupby(['rep']).first()['marker'].to_dict()

        # Update markers for Level_2 not 'control_condition' or 'treatment_condition'
        # if the rep is not found in the dictionnary, return an error
        shape_df.loc[shape_df['Level_2'].apply(lambda x: x not in [control_condition, treatment_condition]), 'marker'] = shape_df.loc[shape_df['Level_2'].apply(lambda x: x not in [control_condition, treatment_condition])].apply(lambda x: mapping.get(x['rep']) if x['rep'] in mapping else ValueError("Error: No marker found for rep {}".format(x['rep'])), axis=1)
    
    return shape_df

    '''