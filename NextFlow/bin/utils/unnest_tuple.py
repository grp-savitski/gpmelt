#! /usr/bin/env python3


#################################################
# unnest_tuple
#################################################   
# unnest a tuple of tuple
# from (tuple1, (tuple2, tuple3)) to (tuple1, tuple2, tuple3)
def unnest_tuple(tup):
    result = ()
    for item in tup:
        if isinstance(item, tuple):
            result += unnest_tuple(item)
        else:
            result += (item,)
    return result