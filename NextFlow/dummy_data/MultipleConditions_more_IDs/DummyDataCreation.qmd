---
title: "Create dummy data to test gpmelt + nextflow"
format: html
---

```{r}
library(dplyr)
```


# load the phosphoTPP data

this will generate data with multiple conditions

```{r}
phosphoTPP_data <- read.csv("/Users/sueur/Desktop/PhD/PaperWriting/code/ISMBsubmission_MyCode/CodeForSubmission/Analysis/PhosphoTPP/RScripts/prerun/SelectedDataForPython_2repPerPeptides_1PhosphoTo3Unbounds_MedianNonPhospho_WithGroupSlicing_20PhosphoAtMost.csv") %>% dplyr::select(-X)
```

```{r}
NumberSamples_phosphoTPP <- read.csv("/Users/sueur/Desktop/PhD/PaperWriting/code/ISMBsubmission_MyCode/CodeForSubmission/Analysis/PhosphoTPP/RScripts/prerun/NumberSamples_perID.csv") %>% dplyr::select(-X)
```

# Random choice of Ids

Let's choose 3 Ids, one with only 1 phospho, one with 3 and one with 5 :

```{r}
set.seed(29348756)
RandomSubset <- NumberSamples_phosphoTPP %>%
  filter(Phospho %in% c(1,3,5)) %>%
  group_by(Phospho) %>%
  slice_sample(n=5)
```

```{r}
DummyData_mulitpleConditions <- phosphoTPP_data %>%
  inner_join(
    RandomSubset %>% ungroup() %>% dplyr::select(Level_1)
  ) %>%
  dplyr::rename(
     "rep" = "replicate",
     "x" = "temperature",
     "y" = "FC",
     "Level_2" = "sequence",
     "Level_3" = "ReplicatePerSequence"
  ) %>%
  dplyr::select(-GroupNumber, -comparisonID) %>%
  dplyr::relocate("gene_name","Level_1", "condition" , "Level_2", "Level_3", "x", "y" )
```

```{r}
DummyNumberSamples <- data.frame(Level_1 = RandomSubset$Level_1,
                                 NSamples =  sample( x = c(3: 10), size=length( RandomSubset$Level_1), replace=TRUE)) 
```

```{r}
write.csv(DummyData_mulitpleConditions,
          "/Users/sueur/Desktop/PhD/PaperWriting/code/ISMBsubmission_MyCode/CodeForSubmission/HGPM_Code/NextFlow/dummy_data/MultipleConditions_more_IDs/dataForGPMelt.csv")

write.csv(DummyNumberSamples,
          "/Users/sueur/Desktop/PhD/PaperWriting/code/ISMBsubmission_MyCode/CodeForSubmission/HGPM_Code/NextFlow/dummy_data/MultipleConditions_more_IDs/NumberSamples_perID.csv")

```

