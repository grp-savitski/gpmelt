
process GATHER_AND_SAVE_RESULTS {

    debug true 

    publishDir path: {"${permanent_results_directory_ch}"} , mode: 'copy', overwrite: true

    input:
        path results_gpmelt_pkl
        val permanent_results_directory_ch
        
    output:
        path 'likelihood_ratios_df.csv', emit: likelihood_ratios_real_dataset
        path 'likelihood_ratios_sampled_dataset_df.csv', emit: likelihood_ratios_sampled_dataset
        path '*.csv', emit: results_csv_file
        path 'combined_results.pkl'
        path '*.pth'


    script:
    """
    echo "Enter gathering and saving of results from parallel jobs"
    num_collected_files=\$(ls ${results_gpmelt_pkl} | wc -l)
    echo "Number of collected results pkl files: \$num_collected_files"
    results_gathering.py ${results_gpmelt_pkl}
    """
}