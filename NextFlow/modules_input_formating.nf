process INPUT_FORMATTING {
    debug true 

    publishDir path: {"${permanent_results_directory_ch}"} , mode: 'copy', overwrite: true, pattern:'data_with_scalingFactors.csv'

    input:
        path dataset
        path nb_sample
        path parameter_file
        val permanent_results_directory_ch
        val number_id_per_batch
        val minimise_required_memory_space_ch
    
    output:
        path 'parameter_dict.pkl', emit: parameter_dict_pkl
        path 'job_id_*.pkl' , emit: input_data_pkl
        path 'GPMelt_rules.csv' , emit: gpmelt_rules
        path 'UniqueIDs.csv'      , emit: uniqueIDs_csv
        path 'data_with_scalingFactors.csv' , optional: true 


    script:
    """
    echo Starting by formatting the input dataset and parameters.
    echo A number of $number_id_per_batch IDs per batch has been chosen to run GPMelt in parallel.
    echo The amount of outputs saved will be reduced: $minimise_required_memory_space_ch
    InputFormatting.py $dataset $nb_sample $parameter_file $number_id_per_batch $minimise_required_memory_space_ch
    """
}

process INPUT_FORMATTING_WITHSUBSET {
    debug true 

    publishDir path: {"${permanent_results_directory_ch}/Test_IDs"} , mode: 'copy', overwrite: true, pattern:'data_with_scalingFactors.csv'

    input:
        path dataset
        path nb_sample
        path parameter_file
        val permanent_results_directory_ch
        val number_id_per_batch
        val minimise_required_memory_space_ch
        path subset
    
    
    output:
        path 'parameter_dict.pkl', emit: parameter_dict_pkl
        path 'job_id_*.pkl' , emit: input_data_pkl
        path 'GPMelt_rules.csv' , emit: gpmelt_rules
        path 'UniqueIDs.csv'      , emit: uniqueIDs_csv
        path 'data_with_scalingFactors.csv' , optional: true  // Note that this will still contain all IDs, and not only the subset used for the testing


    script:
    """
    echo Considering a subset of IDs to test the model specification and CPU, time and memory requirement.
    echo Starting by formatting the input dataset and parameters.
    echo A number of $number_id_per_batch IDs per batch has been chosen to run GPMelt in parallel.
    echo The amount of outputs saved will be reduced: $minimise_required_memory_space_ch
    InputFormatting.py $dataset $nb_sample $parameter_file $number_id_per_batch $minimise_required_memory_space_ch $subset
    """
}
