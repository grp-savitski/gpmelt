process RUN_GPMELT_IN_BATCH{

    debug true 

    publishDir path: {"${permanent_results_directory_ch}/Plots"} , mode: 'copy', overwrite: true, pattern:'*.pdf'

    input:
        path(id_pkl_paths)
        val permanent_results_directory_ch
        
    output:
        path 'results_*.pkl' , emit: results_pkl
        path '*.pdf', optional: true // the plots are optional

    script:
    """
    echo Entering GPMelt workflow
    echo "job id: $id_pkl_paths"
    export OMP_NUM_THREADS=1 # limit the use of thread for GPytorch, as we run several processed in parallel
    echo "Running GPMelt workflow using ${task.cpus} CPUs"
    run_gpmelt_on_mulitple_ids.py $id_pkl_paths ${task.cpus}
    """

}

process COMPUTE_GPMELT_STATISTICS{

    debug true 

    publishDir path: {"${permanent_results_directory_ch}"} , mode: 'copy', overwrite: true, pattern:'*.csv'

    input:
        path likelihood_ratios_df_csv
        path likelihood_ratios_sampled_dataset_df_csv
        path parameter_dict
        val permanent_results_directory_ch
        
    output:
        path '*.csv', emit : p_values

    script:
    """
    echo Computing GPMelt statistics
    compute_likelihood_statistic.py $likelihood_ratios_df_csv $likelihood_ratios_sampled_dataset_df_csv $parameter_dict
    """

}

