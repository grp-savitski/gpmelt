# GPMelt
**A revised version of the GPMelt implementation is here!**

The method is implemented in Python (with some supplementary R scripts) and includes a [Nextflow](https://www.nextflow.io) pipeline. This setup simplifies installation, supports parallel computation, and provides a portable solution for deployment on both local machines and high-performance computing (HPC) clusters.

## Installation & Tutorial
Visit the [GPMelt website](https://gpmelt-website-grp-savitski-be284daa99aadeca97f8e634d7838ce7b92.embl-community.io/contents/home.html) to learn about the GPMelt model (**no advanced math required!**). The site also provides detailed installation and usage instructions, with a comprehensive tutorial.

The current release relies on the Docker image `ceccads/gpmelt-image-with-dask:0.0.1` available on [Docker Hub](https://hub.docker.com/) (see installation instructions on the website for more details).

## Description
GPMelt is a novel statistical framework based on Hierarchical Gaussian Process models designed to fit and compare melting curves of TPP-TR datasets. GPMelt is introduced in [this paper](https://doi.org/10.1371/journal.pcbi.1011632) and can be adapted to analyze any time series data that includes multiple replicates and conditions.

## Abstract
Thermal proteome profiling (TPP) is a proteome-wide technique enabling unbiased detection of protein-drug interactions and changes in protein post-translational states under various biological conditions. Statistical analysis of temperature range TPP (TPP-TR) datasets traditionally compares protein melting curves, which describe the fraction of non-denatured proteins at different temperatures, across conditions (e.g., with and without a drug). However, current models are limited to sigmoidal melting behaviors, even though unconventional melting curves, which can represent up to **50%** of TPP-TR datasets, carry valuable biological information.

We developed GPMelt, a statistical framework based on Hierarchical Gaussian Process models, to make TPP-TR data analysis unbiased with respect to protein melting profiles. GPMelt can handle multiple conditions and is extendable to more complex hierarchies, allowing it to accommodate intricate TPP-TR protocols. This framework expands TPP-TR analysis for both protein- and peptide-level melting curves, enabling access to thousands of previously excluded curves and significantly enhancing TPP’s potential to reveal new biological insights.

## Figures and Analyses from the [GPMelt Paper](https://doi.org/10.1371/journal.pcbi.1011632)

All data used to produce the figures and results are available on Zenodo ([DOI](http://doi.org/10.5281/zenodo.12806563)). Code for reproducing the analyses presented in the paper can be found in the "Analysis" folder, provided as `R` scripts.

*Note:* These `R` scripts rely on `rds` files located in the `data.zip` folder on Zenodo. To run the analyses, please move the folders `data`, `dat` (if available), `GPMelt_result`, and `prerun` from each subfolder (`Simulation_study`, `Staurosporine2021`, `ATP2019`, `PhosphoTPP`, and `all_methods_benchmarking_datasets`) into the corresponding subfolder in the `Analysis` directory.

## Interested in contributing to GPMelt?
Have any thoughts, feedback, or suggestions on GPMelt, the code, or this tutorial? **Encountered any bugs?** Please reach out — we value your contributions to help take GPMelt to the next level!

## License
GNU General Public License v3.0

## Project Status
Still under construction.

## Authors and Acknowledgments
This code has been written by Cecile Le Sueur, with the invaluable help of Federico Marotta. We also thank and the EMBL IT Services and HPC resources, Renato Alves (EMBL Bio-IT), Florian Heyl for the introduction to Nextflow, the AI Health Innovation Cluster, especially Magnus Wahlberg, Marcel Mück and Brian Clarke for the fantastic support and insightful consultations.

